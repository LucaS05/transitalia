FROM php:5.6-apache

COPY . /var/www/html

RUN a2enmod rewrite

RUN apt update && apt install -y --no-install-recommends \
  nano

RUN rm -rf /var/www/html/logs/* \
    && chown -R www-data:www-data /var/www/html

COPY "./php.ini" "$PHP_INI_DIR/conf.d/php.ini"

RUN docker-php-ext-install mysqli pdo pdo_mysql
