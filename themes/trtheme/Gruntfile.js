module.exports = function(grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {});
    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: false,
                    yuicompress: false,
                    optimization: 0
                },
                files: {
                    'assets/css/azienda.css': 'assets/less/azienda.less',
                    'assets/css/servizi.css': 'assets/less/servizi.less',
                    'assets/css/inner.css': 'assets/less/inner.less',
                    'assets/css/main.css': 'assets/less/main.less',
                    'assets/css/home.css': 'assets/less/home.less',
                    'assets/css/login.css': 'assets/less/login.less'
                }
            }
        },
        watch: {
            less: {
                files: ['assets/less/**/*.less'],
                tasks: ['newer:less'],
                options: {
                    spawn: false
                }
            }
        }
    });
    grunt.registerTask('default', ['watch']);
};