$(document).ready(function(){
    var menuBtn = $(".menu-btn");
    var menuClose = $(".menu-close");
    menuBtn.click(function(){
        menuBtn.toggleClass("active");
        $(".pane-mask").toggleClass("animate");
        $(".menu-inner").toggleClass("visible");
    });
    menuClose.click(function(){
        $(".menu-inner").toggleClass("visible");
    })
});