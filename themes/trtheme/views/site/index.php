<?php
    $themePath = Yii::app()->theme->baseUrl;
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($themePath . '/assets/css/home.css');
    $cs->registerScriptFile($themePath . '/assets/js/inner.ui.js', CClientScript::POS_END);
?>
<header>
    <div itemscope itemtype="http://schema.org/Organization" class="logo">
       <a itemprop="url" href="http://www.transitaliaexpress.it" class="home-link">Home</a>
        <?php 
            $imghtml = CHtml::image($themePath . "/assets/images/logo.png", 
                "Transitalia Express s.r.l.", array("itemprop" => "logo"));
            echo CHtml::link($imghtml, array('/', 'lang' => Yii::app()->language));
        ?>
    </div>
    <?php $this->widget('application.components.transita-menu.TransitaMenu', array('isHome' => true)); ?>
</header>
<!-- Link per Reviews: https://www.google.it/search?site&source=hp&ei=Faj_V5SWIYbTU9yXjZgN&q=transitalia%20express&oq=transitalia%20ex&gs_l=mobile-gws-hp.1.0.0j0i22i30k1l3.1111.5777.0.6891.15.15.0.8.8.0.216.2466.0j14j1.15.0....0...1c.1j4.64.mobile-gws-hp..1.13.1035.0..0i131k1.kcFTuZxDYsc#fpstate=lie&lrd=0x133810955c6031cf:0xf039cfcaf64aa79a,3,5 -->
<div id="overlay"></div>
<div class="container-fluid">
    <div class="jumbotron" id="home-payoff">
        <h1>Transitalia Express</h1>
        <p class="lead">Your shipments all over the world</p>
    </div>
</div>