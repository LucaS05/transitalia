<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contatti',
);
?>
<style>
#map_canvas{
	position:absolute;
    width: 100%;
    height: 100%;
    z-index: 0;
    top: 0px;
}

#current{
     padding-top: 25px;
}
#bg{
	padding: 20px 0 40px;
	background-color: rgba(255, 255, 255, .4);
	position: relative;
	z-index: 10;
}
#padding{
	height: 500px;
}

#content{
	margin-bottom: 50px;
}
#m-formcontatti #btn-invia{
	background-color:#7f7e7e;
	color: #fff;
	font-weight: normal;
}
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
<div class="col-xs-12 hidden-xs" id="overlay"></div>
<div id="map_canvas" class="hidden-xs"></div>
<div id="padding" class="hidden-xs"></div>
<div id="bg">
	<div class="container">
		<h1><strong>Contatti</strong></h1>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<?php $this->renderPartial("//components/_frmcontact", array('model'=>$model)); ?>
			</div>
			<div class="col-xs-12 col-md-6" id="maininfo">
				<div class="col-xs-12"><div id="cntlogo">Testo per seo</div></div>
				<div class="col-xs-12">Via Foggia, 2</div>
				<div class="col-xs-12">info@gllex.it</div>
				<div class="col-xs-12">0883 55.47.70</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var emptyDiv = document.createElement('div');
	emptyDiv.className = 'empty';
	emptyDiv.index = 0;

var settingsItemsMap = {
	    zoom: 17,
	    mapTypeControl: false,
	    panControl: false,
	    zoomControl: false,
	    streetViewControl: false,
	    scrollwheel: false,
	    center: new google.maps.LatLng(41.2282103, 16.2996555),
	    styles: [{
	        "featureType": "water",
	        "stylers": [{
	            "color": "#46bcec"
	        }, {
	            "visibility": "on"
	        }]
	    }, {
	        "featureType": "landscape",
	        "stylers": [{
	            "color": "#e2e1e1"
	        }]
	    }, {
	        "featureType": "road",
	        "stylers": [{
	            "saturation": -100
	        }, {
	            "lightness": 45
	        }]
	    }, {
	        "featureType": "road.highway",
	        "stylers": [{
	            "visibility": "simplified"
	        }]
	    }, {
	        "featureType": "road.arterial",
	        "elementType": "labels.icon",
	        "stylers": [{
	            "visibility": "off"
	        }]
	    }, {
	        "featureType": "road.arterial",
	        "elementType": "geometry.fill",
	        "stylers": [{
	            "color": "#f8f8f8"
	        }]
	    }, {
	        "featureType": "road.local",
	        "elementType": "geometry.fill",
	        "stylers": [{
	            "color": "#f8f8f8"
	        }]
	    }, {
	        "featureType": "administrative",
	        "elementType": "labels.text.fill",
	        "stylers": [{
	            "color": "#444444"
	        }]
	    }, {
	        "featureType": "transit",
	        "stylers": [{
	            "visibility": "off"
	        }]
	    }, {
	        "featureType": "poi",
	        "stylers": [{
	            "visibility": "off"
	        }]
	    }],
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById('map_canvas'), settingsItemsMap);

	var myMarker = new google.maps.Marker({
	    position: new google.maps.LatLng(41.2295431, 16.3000097)
	});

	
	myMarker.setMap(map);
</script>
<script type="text/javascript">
	var mph = $("#content").height() + 50;
	$("#map_canvas").height(mph);
</script>