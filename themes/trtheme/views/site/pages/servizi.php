<?php
    $themePath = Yii::app()->theme->baseUrl;
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($themePath . '/assets/css/inner.css');
    $cs->registerCssFile($themePath . '/assets/css/servizi.css');
    $cs->registerScriptFile($themePath . '/assets/js/inner.ui.js', CClientScript::POS_END);
?>
<div class="pane-mask">
    <div itemscope itemtype="http://schema.org/Organization" class="logo">
        <a itemprop="url" href="http://www.transitaliaexpress.it" class="home-link">Home</a>
        <?php 
            $imghtml = CHtml::image($themePath . "/assets/images/logo.png", 
                "Transitalia Express s.r.l.", array("itemprop" => "logo"));
            echo CHtml::link($imghtml, array('/', 'lang' => Yii::app()->language));
        ?>
    </div>
    <?php $this->widget('application.components.transita-menu.TransitaMenu'); ?>
    <div class="left-pane left-pane-servizi">
        <div id="overlay">
            <h1 class="pane-title"><strong><?php echo Yii::t("strings", "servizi.titolo"); ?></strong></h1>
            <h3><?php echo Yii::t("strings", "servizi.sottotitolo"); ?></h3>
        </div>
    </div>
    <div class="right-pane right-pane-servizi">
        <h2>
            <strong><?php echo Yii::t("strings", "servizi.pagina.titolo"); ?></strong>
        </h2>
        <h3>
            <?php echo Yii::t("strings", "servizi.pagina.sottotitolo"); ?>
        </h3>
        <p class="contenuto"><?php echo Yii::t("strings", "servizi.pagina.contenuto"); ?></p>
        <ul class="lista-servizi list-unstyled">
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.standard.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.standard"); ?>
            </li>
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.express.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.express"); ?>
            </li>
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.express_saver.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.express_saver"); ?>
            </li>
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.express_plus.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.express_plus"); ?></li>
            </li>
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.grandiconsegne.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.grandiconsegne"); ?>
            </li>
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.opzionali.contrassegno.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.opzionali.contrassegno"); ?>
            </li>
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.opzionali.signature.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.opzionali.signature"); ?>
            </li>
            <li class="servizio">
                <h4 class="nome-servizio">
                    <strong><?php echo Yii::t("strings", "servizi.opzionali.ritiro.titolo"); ?></strong>
                </h4>
                <?php echo Yii::t("strings", "servizi.opzionali.ritiro"); ?>
            </li>
        </ul>
    </div>
</div>