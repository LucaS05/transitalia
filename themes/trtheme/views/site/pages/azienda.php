<?php
    $themePath = Yii::app()->theme->baseUrl;
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($themePath . '/assets/css/inner.css');
    $cs->registerCssFile($themePath . '/assets/css/azienda.css');
    $cs->registerScriptFile($themePath . '/assets/js/inner.ui.js', CClientScript::POS_END);
?>
<div class="pane-mask">
    <div itemscope itemtype="http://schema.org/Organization" class="logo">
        <a itemprop="url" href="http://www.transitaliaexpress.it" class="home-link">Home</a>
        <?php 
            $imghtml = CHtml::image($themePath . "/assets/images/logo.png", 
                "Transitalia Express s.r.l.", array("itemprop" => "logo"));
            echo CHtml::link($imghtml, array('/', 'lang' => Yii::app()->language));
        ?>
    </div>
    <?php $this->widget('application.components.transita-menu.TransitaMenu'); ?>
    <div class="left-pane left-pane-azienda">
        <div id="overlay">
            <h1 class="pane-title"><strong><?php echo Yii::t("strings", "azienda.titolo"); ?></strong></h1>
            <h3><?php echo Yii::t("strings", "azienda.sottotitolo"); ?></h3>
        </div>
    </div>
    <div class="right-pane right-pane-azienda">
        <h2><strong><?php echo Yii::t("strings", "azienda.pagina.titolo"); ?></strong></h2>
        <h3><?php echo Yii::t("strings", "azienda.pagina.sottotitolo"); ?></h3>
        <p class="contenuto"><?php echo Yii::t("strings", "azienda.pagina.contenuto"); ?></p>
    </div>
</div>