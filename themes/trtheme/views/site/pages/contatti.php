<?php
    $themePath = Yii::app()->theme->baseUrl;
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($themePath . '/assets/css/inner.css');
    $cs->registerScriptFile($themePath . '/assets/js/inner.ui.js', CClientScript::POS_END);
?>
<div class="pane-mask contatti-pane-mask">
    <div itemscope itemtype="http://schema.org/Organization" class="logo">
        <a itemprop="url" href="http://www.transitaliaexpress.it" class="home-link">Home</a>
        <?php 
            $imghtml = CHtml::image($themePath . "/assets/images/logo.png", 
                "Transitalia Express s.r.l.", array("itemprop" => "logo"));
            echo CHtml::link($imghtml, array('/', 'lang' => Yii::app()->language));
        ?>
    </div>
    <?php $this->widget('application.components.transita-menu.TransitaMenu'); ?>
    <div class="left-pane left-pane-contatti">
        <div id="overlay">
            <h1 class="pane-title"><strong><?php echo Yii::t("strings", "contatti.titolo"); ?></strong></h1>
            <h3><?php echo Yii::t("strings", "contatti.sottotitolo"); ?></h3>
            <div itemscope itemtype="http://schema.org/LocalBusiness" id="business-info">
                <span itemprop="name">Transitalia Express s.r.l.</span>
                <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Km. 31.628 BT 76123, SP231</span>
                    <span itemprop="addressLocality">Andria</span>,
                    <span itemprop="addressRegion">BT</span>
                    <span itemprop="postalCode">76123</span>
                    <span itemprop="addressCountry">IT</span>
                </div>
                <?php echo Yii::t('strings', 'contatti.telefono'); ?>: <a class="contact_info" href="tel:+390883561469" itemprop="telephone">+39 0883 561469</a>
                Fax: <span itemprop="faxNumber">+39 0883 549505</span>
                <div itemprop="department" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name"><?php echo Yii::t('strings', 'contatti.ufficioamministrativo'); ?></span>
                    <a class="contact_info" href="mailto:transita@transitaliaexpress.it" itemprop="email">
                        transita@transitaliaexpress.it</a>
                </div>
                <div itemprop="department" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name"><?php echo Yii::t('strings', 'contatti.ufficiooperativo'); ?></span>
                    <a class="contact_info" href="mailto:tracking-tracing@transitaliaexpress.it" itemprop="email">
                        tracking-tracing@transitaliaexpress.it</a>
                </div>
                <div itemprop="department" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name"><?php echo Yii::t('strings', 'contatti.ufficiocommerciale'); ?></span>
                    <a class="contact_info" href="mailto:business@transitaliaexpress.it" itemprop="email">
                        business@transitaliaexpress.it</a>
                </div>
                <div itemprop="department" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name"><?php echo Yii::t('strings', 'contatti.ufficiocontabile'); ?></span>
                    <a class="contact_info" href="mailto:accountdept@transitaliaexpress.it" itemprop="email">
                        accountdept@transitaliaexpress.it</a>
                </div>
            </div>
        </div>
    </div>
    <div class="right-pane right-pane-contatti">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3001.285381882159!2d16.260485000000003!3d41.215551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x133810955c6031cf%3A0xf039cfcaf64aa79a!2sTransitalia+Express+srl!5e0!3m2!1sit!2sit!4v1442999217429" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>