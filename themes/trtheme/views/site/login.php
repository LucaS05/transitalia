<div style="text-align: center;">
    <h1>
        <?php
            $themePath = Yii::app()->theme->baseUrl;
            $imghtml = CHtml::image($themePath . '/assets/images/logo.png',
                'Logo di Transitalia Express s.r.l.', array('itemprop' => 'logo'));
            echo CHtml::link($imghtml, Yii::app()->request->getBaseUrl(true));
        ?>
    </h1>
</div>
<div class="login-form">
    <div class="login-form__column login-form__column_left col-md-6">
        <h2 class="login-form__title">Tracking Online</h2>
        <h4 class="login-form__sub">Inserisci <strong>username</strong> e <strong>password</strong>
            per accedere al tuo tracking online</h4>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        )); ?>
            <div class="form-group">
                <?php echo $form->label($trackingModel, 'username', array('class' => 'control-label')); ?>
                <?php echo $form->textField($trackingModel, 'username', array('class' => 'form-control')); ?>
                <?php echo $form->error($trackingModel, 'username', array('class' => 'control-label')); ?>
            </div>

            <div class="form-group">
                <?php echo $form->label($trackingModel, 'password', array('class' => 'control-label')); ?>
                <?php echo $form->passwordField($trackingModel, 'password', array('class' => 'form-control')); ?>
                <?php echo $form->error($trackingModel, 'password', array('class' => 'control-label')); ?>
            </div>
            <div class="form-group" style="text-align: center">
                <?php echo CHtml::submitButton('Accedi', array('class' => 'btn btn-login')); ?>
            </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="login-form__column col-md-6 login-form__column_right">
        <h3 class="login-form__title">Non disponi di username e password?</h3>
        <h4 class="login-form__sub">Inserisci il tuo <strong>numero di spedizione</strong>
            per accedere al tracking online</h4>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        )); ?>
        <div class="form-group">
            <?php echo $form->label($genericModel, 'numeroSpedizione', array('class' => 'control-label')); ?>
            <?php echo $form->textField($genericModel, 'numeroSpedizione', array('class' => 'form-control')); ?>
            <?php echo $form->error($genericModel, 'numeroSpedizione', array('class' => 'control-label')); ?>
        </div>
        <div class="form-group" style="text-align: center">
            <?php echo CHtml::submitButton('Accedi', array('class' => 'btn btn-login')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
