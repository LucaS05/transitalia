<?php
    $cs = Yii::app()->clientScript;
    $themePath = Yii::app()->theme->baseUrl;
    $cs->registerCssFile($themePath . '/assets/bootstrap/css/bootswatch2.min.css');
    $cs->registerCssFile($themePath . '/assets/fontawesome/css/font-awesome.min.css');
    $cs->registerCssFile($themePath . '/assets/css/main.css');
    $cs->registerCoreScript('jquery', CClientScript::POS_END);
    $cs->registerScriptFile($themePath . '/assets/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="it"> <!--<![endif]-->
	<head>
        <?php $this->widget('application.components.transita-meta.TransitaMeta'); ?>
	</head>
	<body>
		<?php echo $content; ?>
	</body>
</html>