<?php
  /**
   * Setup
   */
  $cs = Yii::app()->clientScript;
  $themePath = Yii::app()->theme->baseUrl;

  /**
   * StyleSheets BootSwatch Cosmo
   */
  $cs->registerCssFile($themePath . '/assets/bootstrap/css/bootswatch2.min.css');

  /**
   * StyleSheets FontAwesome
   */
  $cs->registerCssFile($themePath . '/assets/fontawesome/css/font-awesome.min.css');

  /**
   * Main StyleSheet
   */
  $cs->registerCssFile($themePath . '/assets/css/login.css');

  /**
   * JavaScripts
  */
  $cs->registerCoreScript('jquery', CClientScript::POS_END);
  $cs->registerCoreScript('jquery.ui', CClientScript::POS_END);
  $cs->registerScriptFile($themePath . '/assets/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
?>
<!DOCTYPE html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Transitalia Express | Tracking Online - Login</title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl . "/assets/images/favicon.ico" ?>">
        <meta http-equiv="content-language" content="IT">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
        <?php echo $content; ?>
	</body>
</html>
