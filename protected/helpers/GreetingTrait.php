<?php

trait GreetingTrait
{
    public function getGreeting()
    {
        $now = new DateTime(NULL, new DateTimeZone('Europe/Rome'));
        $hour = $now->format('G');

        if ($hour <= 12) {
            $greeting = 'Buongiorno';
        } else if($hour <= 17) {
            $greeting = 'Buon Pomeriggio';
        } else {
            $greeting = 'Buonasera';
        }
        return $greeting;
    }
}