<?php

class StatoController extends Controller
{
    public function actionCreate()
    {
        $postData = CJSON::decode(file_get_contents('php://input'));
        if (isset($postData)) {
            $modelStatoSpedizione = new TrStatoSpedizione();
            if (isset($postData['nome'])) {
                $modelStatoSpedizione->nome = $postData['nome'];
            }
            if (isset($postData['codice'])) {
                $modelStatoSpedizione->codice = $postData['codice'];
            }
            $modelStatoSpedizione->save();
        }
    }

    public function actionUpdate()
    {
        $postdata = CJSON::decode(file_get_contents('php://input'));
        if (isset($postdata) && isset($postdata['codice'])) {
            $modelStatoSpedizione = TrStatoSpedizione::model()->find('codice=:codice',
                array('codice' => $postdata['codice']));
            if (is_null($modelStatoSpedizione)) {
                header('HTTP/1.1 500 Nessuno stato esistente con il codice ' . $postdata['codice'] . '!');
                Yii::app()->end();
            }
            $modelStatoSpedizione->nome = $postdata['nome'];
            if ($modelStatoSpedizione->validate()) {
                $modelStatoSpedizione->update();
            }
        }
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('update', 'create'),
                'roles'   => array('admin'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
}