<?php

class FileSpedizioniController extends Controller
{
    public function actionCarica()
    {
        $model = new FileSpedizioni();
        try {
            if (isset($_FILES['FileSpedizioni'])) {
                $model->filesp = CUploadedFile::getInstanceByName('FileSpedizioni');
                $model->path = Yii::app()->basePath . '/../filespedizioni/';
                if ($model->validate()) {
                    $this->svuotaCartella($model->path);
                    $this->salvaFileSpedizioni($model->path, $model->filesp);
                    header('HTTP/1.0 200 OK');
                } else {
                    header('HTTP/1.0 400 Internal Server Error');
                    header('Content-type: application/json');
                    echo CJSON::encode($model->getErrors());
                }
            } else {
                throw new ReadCsvException();
            }
        } catch (ReadCsvException $e) {
            Yii::log($e->getTraceAsString(), 'error');
            header('HTTP/1.0 500 Internal Server Error');
            header('Content-type: application/json');
            $errorsArray = array('fileSp' => 'Nessun File Spedizioni!');
            echo CJSON::encode($errorsArray);
        }
        Yii::app()->end();
    }

    public function actionSalva()
    {
        try {
            $modelSaver = new CSVSpdSaver();
            $path = Yii::app()->basePath . '/../filespedizioni/';
            $returnValue = $modelSaver->salvaSpedizioni($path);
            if (is_array($returnValue)) {
                header('HTTP/1.0 500 Internal Server Error');
                header('Content-type: application/json');
                echo CJSON::encode($returnValue);
            } else {
                header('HTTP/1.0 200 OK');
            }
        } catch (Exception $e) {
            Yii::log($e->getMessage(), 'error');
            Yii::log($e->getTraceAsString(), 'error');
            header('HTTP/1.0 500 Impossibile salvare le spedizioni!');
        }
        Yii::app()->end();
    }

    private function svuotaCartella($path)
    {
        if (isset($path)) {
            $files = glob($path . '*');
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }
    }

    private function salvaFileSpedizioni($path, $fileSpedizioni)
    {
        if (isset($fileSpedizioni)) {
            $fileName = "{$fileSpedizioni}";
            $fileSpedizioni->saveAs($path . $fileName);
        }
    }

    public function accessRules()
    {
        return array(
            array('allow',
                    'actions' => array('salva', 'carica'),
                    'roles'   => array('admin'),
            ),
            array('deny',
                    'users' => array('*'),
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }
}