<?php

class ClientiController extends Controller
{
    public function actionSetState()
    {
        $stateData = CJSON::decode(file_get_contents("php://input"));
        if (isset($stateData)) {
            $username = $stateData["username"];
            $state = $stateData["state"];
            $modelCliente = new Cliente();
            $setStateResult = $modelCliente->setState($username, $state);
            if ($setStateResult === true) {
                header("HTTP/1.1 200 OK");
                Yii::app()->end();
            } elseif(is_array($setStateResult)){
                header("Content-Type: application/json");
                header("HTTP/1.1 500 Impossibile modificare lo stato dell'utente!");
                echo CJSON::encode($setStateResult);
            }
        }
    }

    public function actionCheckUsername()
    {
        $username = Yii::app()->request->getParam('username');
        if (isset($username)) {
            $modelCliente = new Cliente();
            $existResult = $modelCliente->existUsername($username);
            if($existResult !== false && $existResult instanceof User){
                $tempCliente = $existResult;
                header("Content-Type: application/json");
                header("HTTP/1.1 500 Username gia' usato per un altro cliente!");
                echo CJSON::encode(array(
                    "username" => $tempCliente->username,
                    "rag_soc" => Mittente::getByUserID($tempCliente->id)
                ));
                Yii::app()->end();
            } else {
                header("HTTP/1.1 200 OK");
                Yii::app()->end();
            }
        } else {
            header("HTTP/1.1 500 Nessun username specificato!");
        }
    }

    public function actionCreate()
    {
        $postdata = CJSON::decode(file_get_contents("php://input"));
        if (isset($postdata)) {
            $modelCliente = new Cliente();
            $arrayErrors = $modelCliente->save($postdata);
            if(count($arrayErrors) > 0){
                header("HTTP/1.1 500 Impossibile salvare il cliente!");
                echo CJSON::encode($arrayErrors);
            } else {
                header("HTTP/1.1 200 OK");
            }
        } else {
            header("HTTP/1.1 500 Nessun dato per il cliente da creare!");
        }
    }

    private function getEditReturn($editAttribute, $editValue, $successMessage)
    {
        if (isset($editValue)) {
            if (!is_array($editValue)) {
                header("Content-Type: application/json");
                header("HTTP/1.1 200" . $successMessage);
                echo CJSON::encode(array($editAttribute => $editValue));
            } else {
                header("HTTP/1.1 500 Dati errati!");
                echo CJSON::encode($editValue);
            }
        } else {
            header("HTTP/1.1 500 Dati errati!");
            Yii::app()->end();
        }
    }

    public function actionEditUsername()
    {
        $clienteData = CJSON::decode(file_get_contents("php://input"));
        if (isset($clienteData["username"]) && $clienteData["new_username"]) {
            $modelCliente = new Cliente();
            $username = $clienteData["username"];
            $newUsername = $clienteData["new_username"];
            $editResult = $modelCliente->editClienteAttribute($username, "username", $newUsername);
            if ($editResult === true) {
                return $this->getEditReturn("username", $newUsername, "Username Modificata!");
            } else {
                header("HTTP/1.1 500 Impossibile modificare l'username!");
                echo CJSON::encode($editResult);
                Yii::app()->end();
            }
        }
    }

    public function actionEditPassword()
    {
        $clienteData = CJSON::decode(file_get_contents("php://input"));
        if (isset($clienteData["password"]) && isset($clienteData["username"])) {
            $modelCliente = new Cliente();
            $username = $clienteData["username"];
            $newPassword = $clienteData["password"];
            $editResult = $modelCliente->editClienteAttribute($username, "password", $newPassword);
            if ($editResult === true) {
                return $this->getEditReturn("password", $newPassword, "Password Modificata!");
            } else {
                header("HTTP/1.1 500 Impossibile modificare la password!");
                echo CJSON::encode($editResult);
                Yii::app()->end();
            }
        }
    }

    public function actionAddRagSoc()
    {
        $clienteData = CJSON::decode(file_get_contents("php://input"));
        if (isset($clienteData["ragsoc"]) && isset($clienteData["username"])) {
            $modelCliente = new Cliente();
            $ragSoc = $clienteData["ragsoc"];
            $username = $clienteData["username"];
            $addResult = $modelCliente->addRagSoc($username, $ragSoc);
            if($addResult === true) {
                header("HTTP/1.1 200 Ragione sociale aggiunta!");
                echo CJSON::encode(array("ragsoc" => $ragSoc));
                Yii::app()->end();
            } elseif(is_array($addResult)) {
                header("HTTP/1.1 500 Impossibile aggiungere la ragione sociale!");
                echo CJSON::encode($addResult);
                Yii::app()->end();
            }
        } else {
            header("HTTP/1.1 500 Ragione sociale e username mancanti!");
            Yii::app()->end();
        }
    }

    public function actionRemoveRagSoc()
    {
        $clienteData = CJSON::decode(file_get_contents("php://input"));
        if (isset($clienteData["ragsoc"])) {
            $modelCliente = new Cliente();
            $ragSoc = $clienteData["ragsoc"];
            $removeResult = $modelCliente->removeRagSoc($ragSoc);
            if($removeResult === true) {
                header("HTTP/1.1 200 Ragione sociale rimoassa!");
                echo CJSON::encode(array("ragsoc" => $ragSoc));
                Yii::app()->end();
            } elseif(is_array($removeResult)) {
                header("HTTP/1.1 500 Impossibile rimuovere la ragione sociale!");
                echo CJSON::encode($removeResult);
                Yii::app()->end();
            }
        } else {
            header("HTTP/1.1 500 Ragione sociale mancante!");
            Yii::app()->end();
        }
    }

}