<?php
 
class DefaultController extends Controller
{
    use GreetingTrait;

    public $layout='/layouts/main';

    public function actionIndex()
    {
        if (Yii::app()->user->checkAccess('admin')) {
            $this->render('admin_index');
        } elseif (Yii::app()->user->checkAccess('cliente')) {
            $this->render('client_index', [
                'greetingMessage' => $this->getGreeting()
            ]);
        }
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index'),
                'roles'=>array('admin', 'cliente'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
}