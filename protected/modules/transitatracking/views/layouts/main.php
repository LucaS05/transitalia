<?php
    /**
    * Setup
    */
    $cs = Yii::app()->clientScript;

    $themePath = Yii::app()->theme->baseUrl;

    /**
    * StyleSheets BootStrap
    */
    $cs->registerCssFile($themePath . '/assets/bootstrap/css/bootstrap.css');
    /**
    * StyleSheets BootSwatch Cosmo
    */
    $cs->registerCssFile($themePath . '/assets/bootstrap/css/bootswatch.min.css');

    /**
    * StyleSheets FontAwesome
    */
    $cs->registerCssFile($themePath . '/assets/fontawesome/css/font-awesome.min.css');

    /**
     * Main StyleSheet
     */
    if(Yii::app()->user->checkAccess('admin')) {
        $cs->registerCssFile($this->module->assetsUrl . '/css/daterangepicker.css');
        $cs->registerCssFile($this->module->assetsUrl . '/css/admin.css');
    }

    if(Yii::app()->user->checkAccess('cliente')){
        $cs->registerCssFile($this->module->assetsUrl . '/css/cliente.css');
    }

    /**
    * JavaScripts
    */
    $cs->registerCoreScript('jquery');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Transitalia Express | Tracking Online</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?php echo $themePath . '/assets/images/favicon.ico'; ?>">
	</head>
	<body ng-app="TransitaTracking">
		<?php echo $content; ?>
	</body>
</html>