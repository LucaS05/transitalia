<div class="trwrapper" tr-full>
    <div ui-view>
        <div class="col-md-6 trhalf trhalf_success">
            <i class="fa fa-check trhalf__bicon"></i>
            {{uploadSalvate}} Spedizioni<br />
            salvate correttamente
        </div>
        <div class="col-md-6 trhalf trhalf_error" ng-if="uploadNonSalvate > 0">
            <i class="fa fa-exclamation-triangle trhalf__bicon"></i>
            {{uploadNonSalvate}} Spedizioni<br />
            non salvate correttamente<br />
            <div class="btn trhalf__btn trhalf__btn_error" ng-click="showErrors()">Visualizza Errori</div>
        </div>
    </div>
</div>