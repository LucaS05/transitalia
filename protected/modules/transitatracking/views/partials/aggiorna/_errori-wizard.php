<div class="errori-wizard">
    <div class="spedizione spedizione_errors col-md-6 col-md-offset-3">
        <h2 class="spedizione-item__nr">Spedizione nr. {{mainError.spedizione.num_spedizione}}</h2>
        <div class="errors-list">
            <div class="errors-list__main">
                <p class="errors-list__caption">Errori</p>
                <ul>
                    <li class="errors-list__item" ng-repeat="errore in mainError.errori">{{errore}}</li>
                </ul>
            </div>
            <!--<div class="btn btn-block errors-list__btmbutton">Visualizza Correzioni <i class="fa fa-arrow-right"></i></div>-->
        </div>
        <div class="spedizione__section spedizione__section_top">
            <div class="row_vert">
                <div class="spedizione-item spedizione-item__errddt">
                    <div class="spedizione-icon">
                        <i class="fa fa-file-text-o spedizione-icon__marker"></i>
                        <span class="spedizione-icon spedizione-icon__label">DDT</span>
                    </div>
                    <div class="spedizione-item__data">{{mainError.spedizione.ddt}}</div>
                </div>
                <div class="spedizione-item spedizione-item__errstato">
                    <div class="stato-spedizione stato-spedizione_list">
                        <div class="stato-spedizione__label">Stato</div>
                        <div class="stato-spedizione__stato">{{mainError.spedizione.stato}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="spedizione__section spedizione__section_dest">
            <div class="row">
                <div class="spedizione-item col-md-12">
                    <div class="spedizione__item spedizione-item_dest">{{mainError.spedizione.destinatario}}</div>
                </div>
                <div class="spedizione-item spedizione-item_place col-md-12">
                    <i class="fa fa-map-marker spedizione-icon__marker"></i>
                    <div class="spedizione-item__data">{{mainError.spedizione.luogo_arrivo}}</div>
                </div>
            </div>
        </div>
        <div class="spedizione__section">
            <div class="row">
                <div class="spedizione-item spedizione-item_date col-md-3">
                    <i class="fa fa-calendar-o spedizione-icon_top"></i>
                    <span class="spedizione-item__data">{{mainError.spedizione.data}}</span>
                </div>
                <div class="spedizione-item spedizione-item_colli col-md-3">
                    <i class="fa fa-archive spedizione-icon__marker"></i>
                    <div class="spedizione-item__data">{{mainError.spedizione.colli}} Colli</div>
                </div>
            </div>
        </div>
    </div>
    <div class="wiz-nav">
        <div class="btn trbtn-main trbtn-wiz trbtn-wiz_next" ng-if="nextNrSpedizione != -1" ng-click="getNextError()">
            Successiva: <strong>Spedizione nr. {{nextNrSpedizione}} <i class="fa fa-arrow-right"></i></strong>
        </div>
        <div class="btn trbtn-main trbtn-wiz trbtn-wiz_prev" ng-if="prevNrSpedizione != -1" ng-click="getPrevError()">
            Precedente: <strong>Spedizione nr. {{prevNrSpedizione}} <i class="fa fa-arrow-left"></i></strong>
        </div>
        <div class="wiz-nav__status">{{currIndex + 1}} di {{total}}</div>
    </div>
</div>