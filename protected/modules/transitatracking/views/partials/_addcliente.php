<div ui-view>
    <div class="row">
        <div class="toolbar col-md-12">
            <div class="col-md-12">
                <h1 class="toolbar-title">Inserisci Cliente</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 trform__wrapper">
            <?php $this->renderPartial("cliente/_formCliente"); ?>
        </div>
    </div>
</div>