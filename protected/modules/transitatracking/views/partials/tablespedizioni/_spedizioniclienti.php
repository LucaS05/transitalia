<div class="container-fluid" ui-view>
    <div class="row">
        <div class="toolbar col-md-12">
            <?php $this->widget('application.components.transita-filters.TransitaFilters'); ?>
        </div>
    </div>
    <div class="row">
        <div ng-if="loadComplete == false" class="loader spdloader">
            <div class="loader__msg">
                <span id="spinner"></span> Caricamento spedizioni in corso....
            </div>
        </div>
        <div ng-if="spedizioni.totalSpd == 0" class="spdstate spedstate_empty">
            <i class="fa fa-arrow-up spedstate_empty__arrow"></i>
            <h1 class="spdstate__message">Cerca la tua spedizione</h1>
            <div class="truckicon">
                <i class="fa fa-search truckicon__search"></i>
                <i class="fa fa-truck truckicon__truck"></i>
            </div>
        </div>
        <div ng-if="spedizioni.totalSpd == -1" class="spdstate spedstate_notfound">
            <h1 class="spdstate__message">Nessuna spedizione trovata!</h1>
            <h3 class="spdstate__message">In base ai filtri da te impostati!</h3>
            <div class="truckicon">
                <i class="fa fa-times truckicon_cross"></i>
                <i class="fa fa-truck truckicon_truck"></i>
            </div>
        </div>
        <div ng-if="spedizioni.totalSpd != 0">
            <div class="tr-listspedizioni container">
                <div class="row">
                    <div dir-paginate="spd in spedizioni.list | itemsPerPage: limit"
                        total-items="spedizioni.totalSpd"
                        ui-sref=".spedizione({idspedizione: spd.id})"
                        class="spedizione col-md-8 col-md-offset-2">
                            <div class="spedizione__section spedizione__section_top">
                                <div class="row">
                                    <div class="col-md-6 spedizione-item spedizione-item_date">
                                        <i class="fa fa-calendar-o spedizione-icon_top"></i>
                                        <span class="spedizione-item__data">{{spd.data}}</span>
                                    </div>
                                    <div class="col-md-6 spedizione-item">
                                        <div class="stato-spedizione stato-spedizione_list">
                                            <div class="stato-spedizione__label">Stato</div>
                                            <div class="stato-spedizione__stato">{{spd.stato}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="spedizione__section spedizione__section_dest">
                                <div class="row">
                                    <div class="spedizione-item col-md-12">
                                        <div class="spedizione__item spedizione-item_dest">{{spd.destinatario}}</div>
                                    </div>
                                    <div class="spedizione-item spedizione-item_place col-md-12">
                                        <i class="fa fa-map-marker spedizione-icon__marker"></i>
                                        <div class="spedizione-item__data">{{spd.luogo_arrivo}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="spedizione__section">
                                <div class="row">
                                    <div class="spedizione-item spedizione-item_colli col-md-3">
                                        <i class="fa fa-archive spedizione-icon__marker"></i>
                                        <div class="spedizione-item__data">{{spd.colli}} Colli</div>
                                    </div>
                                    <div class="spedizione-item col-md-3">
                                        <div class="spedizione-icon">
                                            <i class="fa fa-file-text-o spedizione-icon__marker"></i>
                                            <span class="spedizione-icon spedizione-icon__label">DDT</span>
                                        </div>
                                        <div class="spedizione-item__data">{{spd.ddt}}</div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="text-align: center;">
                <dir-pagination-controls on-page-change="pageChanged(newPageNumber)"></dir-pagination-controls>
            </div>
        </div>
    </div>
</div>