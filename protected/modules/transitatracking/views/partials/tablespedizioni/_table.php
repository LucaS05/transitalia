<div class="container-fluid" ui-view>
    <div class="row">
        <div class="toolbar col-md-12">
            <?php if(Yii::app()->user->checkAccess("admin")): ?>
                <div class="col-sm-3">
                    <div class="btn btn-block trbtn-main" ui-sref="aggiorna">
                        <i class="fa fa-upload"></i> Aggiorna Spedizioni
                    </div>
                </div>
            <?php endif; ?>
            <?php $this->widget('application.components.transita-filters.TransitaFilters'); ?>
        </div>
    </div>
    <div class="row tablewrap">
        <div ng-if="!loadComplete" class="loader spdloader" style="background-color: #e9edf5; color: #333;">
            <div class="loader__msg">
                <span id="spinner"></span> Caricamento spedizioni in corso....
            </div>
        </div>
        <h1 id="table-tit">Spedizioni</h1>
        <div ng-if="spedizioni.totalSpd == 0" id="empty-filter">Nessuna spedizione presente</div>
        <div ng-if="spedizioni.totalSpd != 0">
            <table class="table transita-table transita-table_spedizioni persist-area">
                <thead class="persist-header transita-table__head">
                    <tr>
                        <?php if(Yii::app()->user->checkAccess("admin")): ?>
                            <th class="head__item">Tracking</th>
                            <th class="head__item">Num. Spedizione</th>
                        <?php endif; ?>
                        <th class="head__item">DDT</th>
                        <th class="head__item">Data</th>
                        <th class="head__item">Destinatario</th>
                        <th class="head__item">Data di Consegna</th>
                        <th class="head__item">Stato</th>
                    </tr>
                </thead>
                <tbody class="transita-table__body">
                    <tr class="transita-table__row"
                        dir-paginate="spd in spedizioni.list | itemsPerPage: limit"
                        total-items="spedizioni.totalSpd"
                        ui-sref=".spedizione({idspedizione: spd.id})">
                            <?php if(Yii::app()->user->checkAccess("admin")): ?>
                                <td class="body__item" data-th="Tracking">{{spd.tracking}}</td>
                                <td class="body__item" data-th="Num. Spedizione">{{spd.num_spedizione}}</td>
                            <?php endif; ?>
                            <td class="body__item" data-th="DDT">{{spd.ddt}}</td>
                            <td class="body__item" data-th="Data">{{spd.data}}</td>
                            <td class="body__item" data-th="Destinatario">{{spd.destinatario}}</td>
                            <td class="body__item" data-th="Data Consegna">{{spd.data_consegna}}</td>
                            <td class="body__item" data-th="Stato">{{spd.stato}}</td>
                    </tr>
                </tbody>
            </table>
            <div class="col-md-12" style="text-align: center;">
                <dir-pagination-controls on-page-change="pageChanged(newPageNumber)"></dir-pagination-controls>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        function UpdateTableHeaders() {
            $(".persist-area").each(function() {

                var el             = $(this),
                    offset         = el.offset(),
                    scrollTop      = $(window).scrollTop(),
                    floatingHeader = $(".floatingHeader", this)

                if ((scrollTop > (offset.top - 40)) && (scrollTop < offset.top + el.height())) {
                    floatingHeader.css({
                        "visibility": "visible"
                    });
                } else {
                    floatingHeader.css({
                        "visibility": "hidden"
                    });
                };
            });
        }

// DOM Ready
        $(function() {

            var initTable = function(){
                var clonedHeaderRow;

                $(".persist-area").each(function() {
                    clonedHeaderRow = $(".persist-header", this);
                    clonedHeaderRow
                        .before(clonedHeaderRow.clone())
                        .css("width", clonedHeaderRow.width())
                        .addClass("floatingHeader");
                });
                var origHeader = $(".persist-header")[0];
                $(origHeader).find("th").each(function(th){
                    var thHeadElem = $(this).width();
                    $(".floatingHeader").find('th').eq(th).width(thHeadElem);
                });

                $(window)
                    .scroll(UpdateTableHeaders)
                    .trigger("scroll");
            };

            setTimeout(initTable, 7000)

        });
    });
</script>