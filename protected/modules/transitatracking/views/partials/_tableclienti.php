<div class="container-fluid" ui-view>
    <div class="row">
        <div class="toolbar col-md-12">
            <div class="col-md-12">
                <a class="btn btn-default trbtn-main" ui-sref=".inserisci" role="button">
                    <i class="fa fa-plus"></i> Inserisci Cliente
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table transita-table">
            <thead class="transita-table__head">
                <tr>
                    <th class="head__item col-md-10">Username</th>
                    <th class="head__item head__item_stato">Stato</th>
                </tr>
            </thead>
            <tbody class="transita-table__body">
                <tr class="transita-table__row"
                    ng-repeat="clt in clienti">
                    <td class="body__item" ui-sref=".modifica({idcliente : clt.id})">{{clt.username}}</td>
                    <td class="body__item">
                        <div class="stato-switch"
                             ng-class="clt.stato == 'attivo' ? 'stato-switch_attivo' : 'stato-switch_disattivo'">
                            <div class="wrap">
                                <div class="stato-switch__switcher"></div>
                                <div class="stato-switch__btn btn-attivo">
                                    <button type="button" class="btn btn-link btn-block"
                                            ng-click="enableUser(clt)">Attivo</button>
                                </div>
                                <div class="stato-switch__btn btn-disattivo">
                                    <button type="button" class="btn btn-link btn-block"
                                            ng-click="disableUser(clt)">Disattivo</button>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>