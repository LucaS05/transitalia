<div ui-view>
    <transita-loading></transita-loading>
	<div class="trwrapper" tr-full>
        <div class="upload-form col-sm-6 col-sm-offset-3">
            <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id'=>'main-fs-form',
                    'enableAjaxValidation' => FALSE,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
            ?>
                <div style="display: none">
                    <?php
                        echo $form->fileField($model, 'filesp',
                            array("id"=> "filespedizioni", "file-model" => "fileSp"));
                    ?>
                </div>
                <?php
                    $imageUrl = Yii::app()->theme->baseUrl . ('/assets/images/csv.png');
                    echo CHtml::image($imageUrl, "", array("class" => "upload-form__icon"));
                ?>
                <div class="btn btn-block trbtn-main trbtn-main_upload" ng-click="uploadClick()">
                    Seleziona il file <span class="trbtn-main__br">.csv delle spedizioni</span></div>
            <?php $this->endWidget(); ?>
        </div>
	</div>
</div>