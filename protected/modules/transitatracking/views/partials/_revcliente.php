<?php 
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile($this->module->assetsUrl . '/css/riepcliente.css');
?>
<div id="riepcliente" class="row">
    <div id="cliente">
        <i class="fa fa-check"></i>
        <div ng-if="action === 'edit'">
            <b>Cliente modificato correttamente!</b>
        </div>
        <div ng-if="action === 'create'">
            <b>Cliente inserito correttamente!</b>
        </div>
        <div id="main-info">
            <div class="user-info"><span class="label">User</span>{{cliente.username}}</div>
            <div class="user-info"><span class="label">Password</span>{{cliente.password}}</div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var height = $(window).height();
        $("#riepcliente").css("height", height);
    });
</script>