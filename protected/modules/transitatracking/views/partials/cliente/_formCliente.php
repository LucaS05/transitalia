<form class="form" name="clienteForm" ng-submit="addCliente(clienteForm)" novalidate>
    <div class="form-group">
        <div class="row">
            <div class="form-group col-md-6">
                <label for="usernameText" class="control-label">Username</label>
                <input type="text"
                       id="usernameText"
                       class="form-control trform__input"
                       name="username"
                       ng-model="cliente.username"
                       ng-change="checkUsername()"
                       required />
                <div ng-show="!clienteForm.username.$pristine">
                    <div ng-show="clienteForm.username.$error.required && clienteForm.username.$touched"
                         class="trform__errorlabel">L'username è obbligatorio</div>
                </div>
                <div ng-show="sameUsernameError"
                     class="trform__errorlabel">Username già usato per un altro cliente</div>
                <p class="trform__placeholder">Inserisci l'username per il cliente</p>
            </div>
            <div class="form-group col-md-6">
                <label for="codclienteText" class="control-label">Codice Cliente</label>
                <input type="text"
                       id="codclienteText"
                       name="codicepst"
                       class="form-control trform__input"
                       ng-model="cliente.codcliente"
                       ng-change="dataClienteChanged()"
                       required />
                <div ng-show="!clienteForm.codicepst.$pristine">
                    <div ng-show="clienteForm.codicepst.$error.required && clienteForm.codicepst.$touched"
                         class="trform__errorlabel">Il codice del PST è obbligatorio</div>
                </div>
                <p class="trform__placeholder">Inserisci il codice che il cliente ha nel PST</p>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12 ragsoc-list_addcliente">
                <?php $this->widget('application.components.transita-ragsocadder.TransitaRagSocAdder',
                    array('isRightColumn' => false)); ?>
            </div>
        </div>
    </div>
    <div ng-if="cliente.password && listaRagSoc.length > 0">
        <div class="trform__daticliente">
            <div ng-if="!editPassword">
                <h3 class="daticliente__titolo">Dati da fornire al cliente</h3>
                <div style="padding: 20px 0">
                    <div class="daticliente__dato">
                        <p class="daticliente__tipo">Username</p>
                        {{cliente.username}}
                    </div>
                    <div class="daticliente__dato">
                        <p class="daticliente__tipo">Password</p>
                        {{cliente.password}}
                    </div>
                </div>
                <div class="btn trbtn-main" ng-click="editPwdClick()">Cambia Password</div>
            </div>
            <div ng-if="editPassword">
                <h3 class="daticliente__titolo">Modifica la password del cliente</h3>
                <div class="row" style="margin: 56px 0;">
                    <div class="col-md-8">
                        <input type="text"
                               id="passwordText"
                               class="form-control"
                               ng-model="cliente.password"
                               required />
                    </div>
                    <div class="btn trbtn-main col-md-2" ng-click="editPwdClick()">Conferma</div>
                    <div class="btn trundo-link col-md-2" ng-click="undoEditPwd()">Annulla</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <save-button form-valid="formValid" form-type="formType" model-name="cliente"
                            ng-disabled="clienteForm.$invalid" />
        </div>
    </div>
</form>