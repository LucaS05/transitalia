<div class="overlay">
    <div class="trmodal container-fluid">
        <div class="success" ng-if="showSuccess">
            <i class="fa fa-check" style="display: block; font-size: 5em;"></i>
            <h3>Stato modificato correttamente!</h3>
            <input type="button" class="btn btn-link" ui-sref="stati" ui-sref-opts="{ reload: true }" value="Chiudi" />
        </div>
        <form class="form" name="statoForm" ng-submit="editStato(statoForm)" novalidate>
            <h2><strong>Modifica Stato</strong></h2>
            <div class="form-group">
                <label class="control-label">Codice</label>
                <h3>{{stato.codice}}</h3>
            </div>
            <div class="form-group">
                <label for="statoText" class="control-label">Nome</label>
                <input id="statoText" class="form-control" type="text" ng-model="stato.nome" />
            </div>
            <input type="submit" class="btn btn-success" value="Salva Modifche" />
            <input type="button" class="btn btn-link" ui-sref="stati" ui-sref-opts="{ reload: true }" value="Chiudi" />
        </form>
    </div>
</div>