<div class="container-fluid">
    <div class="row">
        <div class="toolbar col-md-12">
            <h4 class="toolbar-title col-md-2">Filtra Stati</h4>
            <div class="col-md-3">
                <button-filter filter="daDefinireFilter"
                               run="runFilter(filterType, filterValue)"
                               reset="resetStati()" />
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table transita-table">
            <thead class="transita-table__head">
                <tr>
                    <th class="head__item col-md-6">Nome</th>
                    <th class="head__item col-md-6">Codice</th>
                </tr>
            </thead>
            <tbody class="transita-table__body">
                <tr class="transita-table__row"
                    ng-repeat="stato in statiList">
                    <td class="body__item"
                        ui-sref=".modifica({idstato : stato.id})">{{stato.nome}}</td>
                    <td class="body__item">{{stato.codice}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal-wrapper" ui-view></div>