<div ui-view>
    <div class="row">
        <div class="toolbar col-md-12">
                <h1 class="toolbar-title col-md-12">Modifica Cliente</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="trform__wrapper trform__section">
                <div class="trform__successmessage" ng-if="ediussucc">
                    <strong>Username modificato correttamente!</strong><br />
                    {{editmessage}}<br>
                    <a class="btn btn-link succ-close" ng-click="editMessageClose()">Chiudi</a>
                </div>
                <form class="form" name="usernameForm" ng-submit="sendForm(usernameForm)" novalidate>
                    <h2 class="trform__title">Modifica Username</h2>
                    <div class="form-group trform__edit-group">
                        <label for="usernameText" class="clform__label">Username Attuale</label>
                        <p class="actual__username">{{cliente.username}}</p>
                    </div>
                    <div class="form-group trform__edit-group">
                        <label for="usernameText" class="clform__label">Nuova Username</label>
                        <input type="text"
                               id="usernameText"
                               class="form-control trform__input"
                               name="username"
                               ng-model="cliente.new_username"
                               ng-model-options="{allowInvalid: true}"
                               required />
                        <div ng-show="!usernameForm.username.$pristine">
                            <div ng-show="usernameForm.username.$error.required && usernameForm.username.$touched"
                                 class="clform__errorlabel">L'username è obbligatorio</div>
                        </div>
                        <div ng-show="usernameForm.username.$error.username"
                             class="clform__errorlabel">Username già usato per un altro cliente</div>
                        <p class="trform__placeholder">Inserisci il nuovo username per il cliente</p>
                    </div>
                    <save-button form-valid="formValid" form-type="formType" model-name="username" />
                </form>
            </div>
        </div>
        <div class="col-md-5">
            <?php $this->widget('application.components.transita-ragsocadder.TransitaRagSocAdder',
                array('isRightColumn' => true)); ?>
        </div>
        <div class="col-md-7">
            <div class="trform__wrapper trform__section">
                <div class="trform__successmessage" ng-if="edipwsucc">
                    <strong>Password modificata correttamente!</strong><br />
                    {{editmessage}}<br>
                    <a class="btn btn-link succ-close" ng-click="editMessageClose()">Chiudi</a>
                </div>
                <h2 class="trform__title">Modifica Password</h2>
                <form class="form" name="passwordForm" ng-submit="sendForm(passwordForm)" novalidate>
                    <div class="form-group trform__edit-group">
                        <label for="passwordText" class="clform__label">Nuova Password</label>
                        <input type="text"
                               id="passwordText"
                               class="form-control trform__input"
                               name="password"
                               ng-model="cliente.password"
                               ng-model-options="{allowInvalid: true}"
                               required />
                        <div ng-show="!passwordForm.password.$pristine">
                            <div ng-show="passwordForm.password.$error.required && passwordForm.password.$touched"
                                 class="clform__errorlabel">L'password è obbligatoria</div>
                        </div>
                        <p class="trform__placeholder">Inserisci la nuova password del cliente</p>
                    </div>
                    <save-button form-valid="formValid" form-type="formType" model-name="password" />
                </form>
            </div>
        </div>
    </div>
</div>