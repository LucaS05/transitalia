<?php
	$cs = Yii::app()->clientScript;
    $cs->registerScriptFile($this->module->assetsUrl . '/js/vendor/vendorbundle.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/cliente/clientebundle.js', CClientScript::POS_END);
?>
<div id="userbar">
    <div class="container">
        <h1 class="logo logo_userbar">
            <?php
                $imghtml = CHtml::image($this->module->assetsUrl . "/images/logo.png",
                    "Transitalia Express s.r.l.", array("itemprop" => "logo"));
                echo CHtml::link($imghtml, array('/transitatracking/#/'));
            ?>
        </h1>
        <p class="greeting-message"><?php echo $greetingMessage ?>, <?php echo Yii::app()->user->username; ?>
        </p>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('site/logout'); ?>" id="logout">
            <i class="fa fa-power-off"></i> Esci
        </a>
    </div>
</div>
<div ui-view id="main-tracking"></div>