<?php
	$cs = Yii::app()->clientScript;
	$cs->registerScriptFile($this->module->assetsUrl . '/js/vendor/moment.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->module->assetsUrl . '/js/vendor/daterangepicker.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/vendor/vendorbundle.js', CClientScript::POS_END);
    $cs->registerScriptFile($this->module->assetsUrl . '/js/admin/adminbundle.js', CClientScript::POS_END);
	$cs->registerScriptFile($this->module->assetsUrl . '/js/vendor/angular-daterangepicker.min.js', CClientScript::POS_END);
?>
<div id="userbar" style="position:fixed; top: 0px; z-index: 9;">
	<a href="<?php echo Yii::app()->createAbsoluteUrl('site/logout'); ?>" id="logout">Logout</a>
</div>
<div id="sidebar">
	<ul class="list-unstyled" id="mainmenu">
        <li class="menu-spedizioni" ui-sref-active="active">
            <?php
                $serUrl = $this->createUrl("#/aggiorna/");
                $linkHtml = '<i class="fa fa-upload"></i>Aggiorna';
                echo CHtml::link($linkHtml, $serUrl, array("ui-sref" => "aggiorna", "ui-sref-opts" => "{ reload: true }"));
            ?>
        </li>
		<li class="menu-spedizioni" ui-sref-active="active">
			<?php
				$serUrl = $this->createUrl("#/spedizioni");
				$linkHtml = '<i class="fa fa-truck"></i>Spedizioni';
				echo CHtml::link($linkHtml, $serUrl, array("ui-sref" => "spedizioni", "ui-sref-opts" => "{ reload: true }"));
			?>
		</li>
		<li class="menu-spedizioni" ui-sref-active="active">
			<?php
				$serUrl = $this->createUrl('#/clienti');
				$linkHtml = '<i class="fa fa-users"></i>Clienti';
				echo CHtml::link($linkHtml, $serUrl, array("ui-sref" => "clienti"));
			?>
		</li>
		<li class="menu-spedizioni" ui-sref-active="active">
            <?php
                $serUrl = $this->createUrl('#/stati');
                $linkHtml = '<i class="fa fa-info"></i>Stati';
                echo CHtml::link($linkHtml, $serUrl, array("ui-sref" => "stati"));
            ?>
        </li>
	</ul>
	<div class="calendario">
		<?php
			$day = date('w');
			$month = date('n');

			$days = Yii::app()->locale->getWeekDayNames("abbreviated");
			$months = Yii::app()->locale->getMonthNames("abbreviated");
		?>
		<p class="calendario__title">Oggi</p>
		<div class="calendario__data"><?php echo $days[$day] ?></div>
		<div class="calendario__giorno"><?php echo $nDay = date('j'); ?></div>
		<div class="calendariodata"><?php echo ucfirst($months[$month]); ?></div>
	</div>
</div>
<div ui-view id="main-tracking"></div>