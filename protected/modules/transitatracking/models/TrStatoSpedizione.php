<?php

class TrStatoSpedizione extends Statispedizione
{
    /**
     * Ritorna l'id dello stato in base a al nome parsato nel csv,
     * specificato come parametro
     */
    public static function getStatoID($parsedStato){
        $criteria = new CDbCriteria;
        $criteria->select = 'id';
        $criteria->condition = 'codice=:codice';
        $criteria->params = array(':codice' => $parsedStato);
        $stato = Statispedizione::model()->find($criteria);
        return $stato;
    }

    public static function getNonInseritoID(){
        return self::getStatoID('NN');
    }

    public static function getConsegnataID(){
        return self::getStatoID('C');
    }

    public function isConsegna(){
        return $this->codice === 'C';
    }

    public function isPrenotazione(){
        return $this->codice === 'PRE';
    }

    public function isAssegnazione(){
        return $this->codice === 'ASS';
    }

    public function isSpecialState()
    {
        switch ($this->codice) {
            case 'C':
                $isSpecialState = true;
                break;
            case 'PRE':
                $isSpecialState = true;
                break;
            case 'ASS':
                $isSpecialState = true;
                break;
            case 'GIA':
                $isSpecialState = true;
                break;
            case 'RIE':
                $isSpecialState = true;
                break;
            default:
                $isSpecialState = false;
                break;
        }
        return $isSpecialState;
    }
} 