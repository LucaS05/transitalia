<?php

/**
 * This is the model class for table "tbl_spedizioni".
 *
 * The followings are the available columns in table 'tbl_spedizioni':
 * @property integer $id
 * @property string $num_spedizione
 * @property string $ddt
 * @property string $data
 * @property string $luogo_arrivo
 * @property string $colli
 * @property string $data_consegna
 * @property string $firmato_da
 * @property integer $stato
 * @property string $destinatario
 * @property integer $mittente
 * @property string $tracking
 *
 * The followings are the available model relations:
 * @property Cronologia[] $cronologiaSp
 * @property Mittente $mittenteSp
 * @property Statispedizione $statoSp
 */
class Spedizioni extends CActiveRecord
{

    protected static $tableName = 'tbl_spedizioni';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Spedizioni::$tableName;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('num_spedizione, destinatario, data, luogo_arrivo, mittente', 'required',
                'message' => '{attribute} non inserito'),
			array('stato, mittente', 'numerical', 'integerOnly' => true),
			array('num_spedizione, ddt, luogo_arrivo, colli, firmato_da, destinatario, tracking', 'length', 'max' => 255),
			array('id, num_spedizione, ddt, data, luogo_arrivo, colli, data_consegna, firmato_da, stato, destinatario, mittente', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, num_spedizione, ddt, data, luogo_arrivo, colli, data_consegna, firmato_da, stato, destinatario, mittente, tracking', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'cronologiaSp' => array(self::HAS_MANY, 'Cronologia', 'idspedizione', 'order'=>'data DESC'),
			'mittenteSp' => array(self::BELONGS_TO, 'Mittente', 'mittente'),
			'statoSp' => array(self::BELONGS_TO, 'Statispedizione', 'stato'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'num_spedizione' => 'Num Spedizione',
			'ddt' => 'Ddt',
			'data' => 'Data',
			'luogo_arrivo' => 'Luogo Arrivo',
			'colli' => 'Colli',
			'data_consegna' => 'Data Consegna',
			'firmato_da' => 'Firmato Da',
			'stato' => 'Stato',
			'destinatario' => 'Destinatario',
			'mittente' => 'Mittente',
			'tracking' => 'Tracking',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Spedizioni the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}