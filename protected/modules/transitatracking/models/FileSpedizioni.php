<?php

/**
 *
 * @property string $path
 * @property integer $user
 *
 */
class FileSpedizioni extends CModel
{
	public $filesp;
    public $path;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('path, filesp', 'required'),
			array('path', 'length', 'max' => 255),
			array('filesp', 'file', 'types' => 'csv', 'safe' => true),
		);
	}

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array('path', 'filesp');
    }
}
