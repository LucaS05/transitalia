<?php

class TrnMittente extends Mittente
{
    public static function getFreeMittente($ragSoc)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("(user IS NULL) AND (rag_soc=:ragsoc)");
        $criteria->params = array(":ragsoc" => $ragSoc);
        return self::model()->find($criteria);
    }

    public static function getByRagSoc($ragSoc)
    {
        return self::model()->find("rag_soc=:ragsoc", array("ragsoc" => $ragSoc));
    }

    public static function getByUserID($userID)
    {
        return self::model()->findAll("user=:user", array("user" => $userID));
    }
} 