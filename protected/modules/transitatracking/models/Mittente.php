<?php

/**
 * This is the model class for table "tbl_mittente".
 *
 * The followings are the available columns in table 'tbl_mittente':
 * @property integer $id
 * @property string $rag_soc
 * @property string $uid
 * @property integer $user
 *
 * The followings are the available model relations:
 * @property User $user0
 * @property Spedizioni[] $spedizionis
 */
class Mittente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_mittente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('rag_soc', 'required'),
			array('user', 'numerical', 'integerOnly' => true),
			array('rag_soc', 'length', 'max' => 255),
			array('uid', 'length', 'max' => 50),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user'),
			'spedizioni' => array(self::HAS_MANY, 'Spedizioni', 'mittente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rag_soc' => 'Rag Soc',
			'uid' => 'Partita Iva / C. F.',
			'user' => 'User',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mittente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    // @todo Funzioni da rivedere. Controllare l'utilizzo

    public static function getFreeMittente($ragSoc){
        $criteria = new CDbCriteria();
        $criteria->addCondition("(user IS NULL) AND (rag_soc=:ragsoc)");
        $criteria->params = array(":ragsoc" => $ragSoc);
        return self::model()->find($criteria);
    }

    public static function getByRagSoc($ragSoc){
        return self::model()->find("rag_soc=:ragsoc", array("ragsoc" => $ragSoc));
    }

    public static function getByUserID($userID){
        return self::model()->findAll("user=:user", array("user" => $userID));
    }
}
