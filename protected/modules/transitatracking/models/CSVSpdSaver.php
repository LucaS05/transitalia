<?php

class CSVSpdSaver extends CModel
{
    /**
     * @param $path
     * @return array|int|null
     * @throws Exception
     * @throws FileOpenException
     */
    public function salvaSpedizioni($path)
    {
        $returnValue = null;
        $spedizioniErrors = array();
        $spedizioniSaved = array();
        $files = glob($path . '*');
        $spedizioniErrors['spedizioni'] = array();
        if (is_file($files[0])) {
            if (($fsphandler = fopen($files[0], 'r')) !== false) {
                while(($data = fgetcsv($fsphandler)) !== false) {
                    if (! $this->isRitiro($data[19])) {
                        $modelSpedizione = new TrSpedizione();
                        $modelSpedizione->tracking = $this->getTrackingSpedizione($data[20]);
                        $modelSpedizione->num_spedizione = $this->getNumeroSpedizione($data[0]);
                        $modelSpedizione->data = $this->getDataSpedizione($data[1]);
                        $modelSpedizione->ddt = $this->getDdtSpedizione($data[3]);
                        $modelSpedizione->mittente = $this->getIDMittente($data[4], $data[21], $data[22]);
                        $modelSpedizione->colli = $this->getColli($data[7]);
                        $modelSpedizione->destinatario = iconv('ISO-8859-1', 'UTF-8', $data[10]);
                        $modelSpedizione->luogo_arrivo = iconv('ISO-8859-1', 'UTF-8', $data[11]);
                        $modelSpedizione->data_consegna = $this->getDataConsegna($data[14]);
                        $modelSpedizione->firmato_da = $data[15];
                        $modelSpedizione->stato = $this->getStatoSpedizione($data[17]);
                        try {
                            if ($modelSpedizione->validate()){
                                if ($this->salvaSpedizione($modelSpedizione) === false) {
                                    $errors = $modelSpedizione->getErrors();
                                    $this->addSpedizioniError($spedizioniErrors['spedizioni'], $errors, $modelSpedizione);
                                } else {
                                    array_push($spedizioniSaved, $modelSpedizione);
                                }
                            } else {
                                $errors = $modelSpedizione->getErrors();
                                $this->addSpedizioniError($spedizioniErrors['spedizioni'], $errors, $modelSpedizione);
                            }
                        } catch (CDbException $e) {
                            $this->addSpedizioniError($spedizioniErrors['spedizioni'], $e, $modelSpedizione);
                        } catch (Exception $e) {
                            $this->addSpedizioniError($spedizioniErrors['spedizioni'], $e, $modelSpedizione);
                        }
                    }
                }
                fclose($fsphandler);
            } else {
                throw new FileOpenException('Impossibile leggere il file csv delle spedizioni');
            }
        } else {
            throw new Exception('File csv delle spedizioni non trovato');
        }
        if (count($spedizioniErrors) > 0) {
            $spedizioniErrors['salvate'] = count($spedizioniSaved);
            $spedizioniErrors['non_salvate'] = count($spedizioniErrors['spedizioni']);
            $returnValue = $spedizioniErrors;
        } else {
            $returnValue = count($spedizioniSaved);
        }
        return $returnValue;
    }

    private function salvaSpedizione($modelSpedizione)
    {
        $isSaved = false;
        if ($modelSpedizione instanceof TrSpedizione) {
            if ($this->isAggiornata($modelSpedizione)) {
                $spdEsistente = $this->getSpedizioneEsistente($modelSpedizione);
                if(isset($spdEsistente)){
                    if ($spdEsistente->stato != $modelSpedizione->stato) {
                        if ($this->isConsegnata($spdEsistente)) {
                            $this->deleteCronoConsegnata($spdEsistente->id);
                        }
                        $this->aggiungiNuovaCronologia($spdEsistente, $modelSpedizione);
                    } elseif ($this->isDataConsegnaAggiornata($spdEsistente->data_consegna,
                        $modelSpedizione->data_consegna)) {
                        if (is_null($spdEsistente->data_consegna)) {
                            $spdEsistente->data_consegna = $modelSpedizione->data_consegna;
                        } elseif (isset($spdEsistente->data_consegna) && $this->isConsegnata($modelSpedizione)) {
                            $nuovaData = new DateTime($modelSpedizione->data_consegna);
                            $tempCronologia = $this->getCronoByDataConsegna($spdEsistente);
                            $tempCronologia->data = $nuovaData->format('Y-m-d');
                            $tempCronologia->update();
                            $spdEsistente->data_consegna = $nuovaData->format('Y-m-d');
                        } elseif (isset($spdEsistente->data_consegna)
                                && $this->isInSpecialState($modelSpedizione)) {
                            $this->aggiungiNuovaCronologia($spdEsistente, $modelSpedizione);
                        }
                    }
                    $spdEsistenteID = $spdEsistente->id;
                    $spdEsistente->setAttributes($modelSpedizione->attributes);
                    $spdEsistente->statoSp = $modelSpedizione->statoSp();
                    $spdEsistente->id = $spdEsistenteID;
                    if ($spdEsistente->validate()) {
                        $spdEsistente->update();
                        $isSaved = true;
                    }
                }
            } elseif ($this->isEsistente($modelSpedizione->num_spedizione) == false) {
                if ($modelSpedizione->save()) {
                    $modelCronologia = new TrCronologia();
                    $modelCronologia->idspedizione = $modelSpedizione->id;
                    $modelCronologia->idstato = $modelSpedizione->stato;
                    if ($this->isInSpecialState($modelSpedizione)) {
                        $modelCronologia->data = $modelSpedizione->data_consegna;
                    } else {
                        $modelCronologia->data = $modelSpedizione->data;
                    }
                    $modelCronologia->save();
                    $isSaved = true;
                }
            }
        }
        return $isSaved;
    }

    private function addSpedizioniError(&$errorsArray, $error, $modelSpedizione)
    {
        $tempArray = array();
        $modelSpedizione->stato = $modelSpedizione->statoSp->nome;
        $tempArray['spedizione'] = $modelSpedizione->getAttributes();
        if ($error instanceof Exception) {
            Yii::log($error, 'error');
            Yii::log('spedizione:' .
                CVarDumper::dumpAsString($modelSpedizione->getAttributes()), 'trace');
            $tempArray['errori'] = $error->getMessage();
        } else if (is_array($error)) {
            $errorMessageArray = array();
            foreach ($error as $attributeArray) {
                foreach ($attributeArray as $index => $errorMessage) {
                    array_push($errorMessageArray, $errorMessage);
                }
            }
            $tempArray['errori'] = $errorMessageArray;
            array_push($errorsArray, $tempArray);
        }
    }

    /**
     * Il metodo ritorna la spedizione esistente a partire dal numero di spedizione oppure dal ddt e dal mittente
     * della spedizione passata in input.
     * Può capitare che siamo nel caso in cui il numero di spedizione sia stato modificato, ma ddt e mittente siano
     * rimasti uguali.
     * In questo caso la spedizione, che deve essere aggiornata e non aggiunta al DB, viene recuperata attraverso
     * il ddt e il mittente, nel secondo if.
     * Nel caso in cui il numero è modificato, ma corrisponde al numero di spedizione di una spedizione diversa
     * rispetto a quella passata in input, il controllo sul ddt si preoccupa di ritornare la spedizione corretta.
     * Perché se il ddt è diverso, vuol dire che è stata recuperata una spedizione diversa, rispetto a quella passata
     * in input.
     * Se nessuno dei casi precedenti è verificato, il metodo ritorna null.
     * @link
     * @param $modelSpedizione
     * @return CActiveRecord|null
     */
    private function getSpedizioneEsistente($modelSpedizione)
    {
        $spdEsistente = null;
        if (isset($modelSpedizione)) {
            $spdEsistente = TrSpedizione::getSpedizioneByNumero($modelSpedizione->num_spedizione);
            if ($spdEsistente === null) {
                $spdEsistente = TrSpedizione::getSpedizioneByDdtMittente($modelSpedizione);
            }
            if (($spdEsistente->ddt != $modelSpedizione->ddt)) {
                $spdEsistente = TrSpedizione::getSpedizioneByDdtMittente($modelSpedizione);
            }
        }
        return $spdEsistente;
    }

    private function getNumeroSpedizione($numeroSpedizione)
    {
        if (isset($numeroSpedizione) && strlen($numeroSpedizione) > 0) {
            return trim($numeroSpedizione);
        }
    }

    private function getTrackingSpedizione($trackingSpedizione)
    {
        if (isset($trackingSpedizione) && strlen($trackingSpedizione) > 0){
            return trim($trackingSpedizione, ' ');
        } else {
            return TrSpedizione::getNoTrackingString();
        }
    }

    private function getDdtSpedizione($ddtSpedizione)
    {
        if (isset($ddtSpedizione) && strlen($ddtSpedizione) > 0){
            return trim($ddtSpedizione, ' ');
        } else {
            return TrSpedizione::getDdtEsenteString();
        }
    }

    private function getIDMittente($mittenteSpedizione, $partitaIva, $codiceFiscale)
    {
        if (empty($mittenteSpedizione)) {
            return;
        }
        $modelMittente = Mittente::model()->find(
            'rag_soc=:ragSoc', array(':ragSoc' => $mittenteSpedizione));
        if(is_null($modelMittente)){
            $modelMittente = new Mittente();

            if (strlen($mittenteSpedizione) < 0) {
                $modelMittente->rag_soc = "";
            } else {
                $modelMittente->rag_soc = $mittenteSpedizione;
            }

            $modelMittente->save();
        }

        if (is_null($modelMittente->uid) && strlen($partitaIva) > 0) {
            $modelMittente->uid = $partitaIva;
            $modelMittente->update();
        } else if (is_null($modelMittente->uid) && strlen($codiceFiscale) > 0) {
            $modelMittente->uid = $codiceFiscale;
            $modelMittente->update();
        }

        return $modelMittente->id;
    }

    private function getColli($colliSpedizione)
    {
        $colli = null;
        if (is_null($colliSpedizione) || (strlen($colliSpedizione) == 0)) {
            $colli = 'n. d.';
        } else {
            $colli = $colliSpedizione;
        }
        return $colli;
    }

    private function getDataSpedizione($dataSpedizione)
    {
        $dataSpd = null;
        if (isset($dataSpedizione) && (strlen($dataSpedizione) > 0)) {
            $strDataSpd = strtotime(str_replace('/', '-', $dataSpedizione));
            if ($strDataSpd !== false) {
                $dataSpd = date('Y-m-d', $strDataSpd);
            }
        }
        return $dataSpd;
    }

    private function getDataConsegna($dataConsegna)
    {
        $dataCons = null;
        if (preg_match('#[0-9]#', $dataConsegna)) {
            $strDataConsegna = strtotime(str_replace('/', '-', $dataConsegna));
            if ($strDataConsegna !== false) {
                $dataCons = date('Y-m-d', $strDataConsegna);
            }
        }
        return $dataCons;
    }

    private function getStatoSpedizione($statoSpedizione)
    {
        $statoSp = null;
        if (isset($statoSpedizione) && strlen($statoSpedizione) > 0) {
            $stato = TrStatoSpedizione::getStatoID($statoSpedizione);
            if (is_null($stato)) {
                $stato = new TrStatoSpedizione();
                $stato->nome = 'Da Definire';
                $stato->codice = $statoSpedizione;
                $stato->save();
            }
            $statoSp = $stato->id;
        } else {
            $statoSp = TrStatoSpedizione::getNonInseritoID();
            $statoSp = $statoSp->id;
        }
        return $statoSp;
    }

    private function isRitiro($tipoSpedizione)
    {
        $isRitiro = false;
        if (isset($tipoSpedizione)){
            $trimTipoSpedizione = trim($tipoSpedizione);
            if (strlen($trimTipoSpedizione) === 1) {
                if (strcasecmp($trimTipoSpedizione, 'R') === 0 || strcasecmp($trimTipoSpedizione, 'S') === 0) {
                    $isRitiro = true;
                }
            }
        }
        return $isRitiro;
    }

    private function getCronoByDataConsegna($modelSpedizione)
    {
        if (isset($modelSpedizione)){
            $dataConsegna = DateTime::createFromFormat('Y-m-d', $modelSpedizione->data_consegna);
            if ($dataConsegna !== false){
                $modelCronologia = TrCronologia::model()->find('data=:data AND idspedizione=:idspd AND idstato=:idstato',
                    array(':data' => $dataConsegna->format('Y-m-d'),
                        ':idstato' => $modelSpedizione->stato,
                        ':idspd' => $modelSpedizione->id
                    ));
            }
            return $modelCronologia;
        }
    }

    private function isEsistente($modelSpedizione)
    {
        $isEsistente = false;
        if (isset($modelSpedizione) && ($modelSpedizione instanceof TrSpedizione)) {
            $spdByNumero = TrSpedizione::getSpedizioneByNumero($modelSpedizione->num_spedizione);
            if (isset($spdByNumero) && ($spdByNumero->ddt == $modelSpedizione->ddt)) {
                $isEsistente = true;
            } else {
                $spdByDdtMittente = TrSpedizione::getSpedizioneByDdtMittente($modelSpedizione);
                if (isset($spdByDdtMittente)) {
                    $isEsistente = true;
                }
            }
        }
        return $isEsistente;
    }

    private function isAggiornata($modelSpedizione)
    {
        $isAggiornata = false;
        if (isset($modelSpedizione) && ($modelSpedizione instanceof TrSpedizione)) {
            $attrSpedizione = $modelSpedizione->attributes;
            unset($attrSpedizione['id']);
            $spdByAttrs = TrSpedizione::model()->findByAttributes($attrSpedizione);
            if (is_null($spdByAttrs) && $this->isEsistente($modelSpedizione)) {
                $isAggiornata = true;
            }
        }
        return $isAggiornata;
    }

    private function isDataConsegnaAggiornata($dataEsistente, $dataSpedizione)
    {
        $isAggiornata = false;
        if (isset($dataEsistente) && isset($dataSpedizione)) {
            $exData = new DateTime($dataEsistente);
            $nuovaData = DateTime::createFromFormat('Y-m-d', $dataSpedizione);
            if ($nuovaData != false) {
                if ($nuovaData->format('Y-m-d') != $exData->format('Y-m-d')){
                    $isAggiornata = true;
                }
            }
        } elseif (isset($dataSpedizione) && is_null($dataEsistente)){
            $isAggiornata = true;
        }
        return $isAggiornata;
    }

    private function isConsegnata($spedizione)
    {
        if ($spedizione instanceof TrSpedizione) {
            return $spedizione->statoSp->isConsegna();
        }
    }

    private function getCronoConsegnata($idSpedizione)
    {
        $modelCronologia = null;
        if(isset($idSpedizione)){
            $statoConsegnata = TrStatoSpedizione::getConsegnataID();
            $modelCronologia = TrCronologia::model()->find('idstato=:idstato AND idspedizione=:idspd',
                array(':idstato' => $statoConsegnata->id, ':idspd' => $idSpedizione));
        }
        return $modelCronologia;
    }

    private function deleteCronoConsegnata($idSpedizione)
    {
        $cronoConsegna = $this->getCronoConsegnata($idSpedizione);
        if (!is_null($cronoConsegna)) {
            $cronoConsegna->delete();
        }
    }

    private function isInSpecialState($modelSpedizione)
    {
        $isSpecialState = false;
        if ($modelSpedizione instanceof TrSpedizione) {
            $isSpecialState = $modelSpedizione->statoSp->isSpecialState();
        }
        return $isSpecialState;
    }

    private function aggiungiNuovaCronologia($spdEsistente, $spdNuova)
    {
        if (($spdEsistente instanceof TrSpedizione) && ($spdNuova instanceof TrSpedizione)) {
            $modelCronologia = new TrCronologia();
            $modelCronologia->idspedizione = $spdEsistente->id;
            $modelCronologia->idstato = $spdNuova->stato;
            if ($this->isInSpecialState($spdNuova)){
                $modelCronologia->data = $spdNuova->data_consegna;
            } else {
                $modelCronologia->data = date('Y-m-d');
            }
            $modelCronologia->save();
        }
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        // TODO: Implement attributeNames() method.
    }
}