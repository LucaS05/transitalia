<?php

class TrCronologia extends Cronologia
{
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = parent::rules();
        array_push($rules, array('idstato', 'chekUnique'));
        return $rules;
    }

    public function chekUnique($attribute){
        if (isset($this->idstato) && isset($this->idspedizione) && isset($this->data)) {
            $modelCronologia = TrCronologia::find('idstato=:stato AND idspedizione=:spedizione
                     AND data=:data', array(':stato' => $this->idstato,
                     ':spedizione' => $this->idspedizione, ':data' => $this->data));
            if(isset($modelCronologia)){
                $this->addError($attribute, 'Dato di cronologia già presente');
            }
        }
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}