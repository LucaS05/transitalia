<?php

class TrSpedizione extends Spedizioni
{

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = parent::rules();
        array_push($rules, array('stato', 'validState'));
        array_push($rules, array('data', 'date', 'format'=>'yyyy-M-d'));
        array_push($rules, array('data_consegna', 'date', 'format'=>'yyyy-M-d'));
        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['statoSp'] = array(self::BELONGS_TO, 'TrStatoSpedizione', 'stato');
        return $relations;
    }

    public function validState($attribute){
        if(isset($this->data_consegna)){
            if(!$this->statoSp->isSpecialState()){
                $this->addError($attribute, 'La spedizione ha una data di consegna, ma non è in uno dei seguenti stati:
                    Consegnata, Assegnata in Distribuzione, Prenotata, Giacenza, Reso Giacenza');
            }
        } else if($this->statoSp->isSpecialState() && is_null($this->data_consegna)){
            $this->addError($attribute, 'La spedizione è in è in uno dei seguenti stati:
                    Consegnata, Assegnata in Distribuzione, Prenotata, Giacenza, Reso Giacenza, 
                    ma la data di consegna è assente');
        }
    }

    public static function getSpedizioneByDdt($ddt){
        if (isset($ddt)) {
            return TrSpedizione::model()->find('ddt=:ddt',
                array(':ddt' => $ddt));
        } else {
            return null;
        }
    }

    public static function getSpedizioneByNumero($numeroSpedizione){
        if (isset($numeroSpedizione)) {
            return TrSpedizione::model()->find('num_spedizione=:numspd',
                array(':numspd' => $numeroSpedizione));
        } else {
            return null;
        }
    }

    public static function getSpedizioneByMittente($mittente){
        if (isset($mittente)) {
            return TrSpedizione::model()->find('mittente=:mittente',
                array(':mittente' => $mittente));
        } else {
            return null;
        }
    }

    public static function getSpedizioneByDdtMittente($modelSpedizione){
        if (isset($modelSpedizione) && ($modelSpedizione->ddt !== TrSpedizione::getDdtEsenteString())) {
            return TrSpedizione::model()->find('ddt=:ddt AND mittente=:mittente',
                array(':ddt' => $modelSpedizione->ddt, ':mittente' => $modelSpedizione->mittente));
        }
    }

    public static function getDdtEsenteString(){
        return 'ESENTE';
    }

    public static function getNoTrackingString(){
        return '/';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
} 