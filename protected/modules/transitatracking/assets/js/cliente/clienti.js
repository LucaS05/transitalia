'use strict';

require('./controllers');

require('../spedizioni');

require('../filters');

require('../trconstants');

require('../less/cliente');

var trackingApp = angular.module('TransitaTracking', [
    'ui.router',
    'ngResource',
    'ngAnimate',
    'TransitaServices',
    'FilterModule',
    'ClienteControllers',
    'angular-loading-bar',
    'angularUtils.directives.dirPagination']
);

trackingApp.config(['$stateProvider', '$urlRouterProvider', 'cfpLoadingBarProvider',
	function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider){
        var templateUrl = 'partials/render/template/';
        var widgetUrl = 'partials/renderWidget/name/';
        cfpLoadingBarProvider.includeSpinner = false;

		$urlRouterProvider.otherwise('/');

		$stateProvider
            .state('spedizioni', {
                cache: false,
                url: '/',
                templateUrl: templateUrl + 'tablespedizioni/_spedizioniclienti',
                controller: 'MainCtrl'
            })		
            .state('spedizioni.spedizione', {
                url: 'spedizioni/:idspedizione/',
                templateUrl: widgetUrl + 'transita-spedizione.TransitaSpedizione',
                controller: 'SpedizioneCtrl',
                resolve : {
                    spedizione: function(SpedizioneService, $stateParams){
                        return SpedizioneService.getSpedizione($stateParams.idspedizione).$promise;
                    }
                }
            });
    }
]);