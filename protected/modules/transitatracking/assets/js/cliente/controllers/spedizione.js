module.exports = function ($scope, spedizione, SpedizioneService){
    $scope.isConsegnata = SpedizioneService.isConsegnata();
    $scope.spedizione = spedizione;
};