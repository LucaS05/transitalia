module.exports = function ($scope, SpedizioneService, SpedizioniFilterService){
    $scope.loadComplete = true;
    $scope.ddtFilter = SpedizioniFilterService.getDdtFilter();
    $scope.destFilter = SpedizioniFilterService.getDestFilter();
    $scope.numSpedizioneFilter = SpedizioniFilterService.getNumeroDiSpedizioneFilter();
    var initListSpedizioni = function(){
        $scope.spedizioni = {};
        $scope.spedizioni.totalSpd = 0;
    };
    initListSpedizioni();
    $scope.resetSpedizioni = function(){
        initListSpedizioni();
    };
    $scope.runFilter = function(filterType, filterValue){
        $scope.loadComplete = false;
        if(angular.isUndefined(filterValue) === false){
            if(filterValue === ""){
                filterValue = null;
            }
            var param = { page : 1 };
            param[filterType] = filterValue;
            var spedizioni = SpedizioneService.getSpedizioni().query(param, function(){
                $scope.spedizioni.list = spedizioni[3].spedizioni;
                if(spedizioni[0].count === 0){
                    $scope.spedizioni.totalSpd = -1;
                    $scope.loadComplete = true;
                } else {
                    $scope.spedizioni.totalSpd = spedizioni[0].count;
                    $scope.loadComplete = true;
                }
            });
        }
    };
};