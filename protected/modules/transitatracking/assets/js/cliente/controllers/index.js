var clienteControllers = angular.module('ClienteControllers', []);

clienteControllers.controller('MainCtrl', require('./main'));
clienteControllers.controller('SpedizioneCtrl', require('./spedizione'));