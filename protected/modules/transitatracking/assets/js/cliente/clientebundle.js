/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(42);
	
	__webpack_require__(23);
	
	__webpack_require__(17);
	
	__webpack_require__(25);
	
	__webpack_require__(45);
	
	var trackingApp = angular.module('TransitaTracking', [
	    'ui.router',
	    'ngResource',
	    'ngAnimate',
	    'TransitaServices',
	    'FilterModule',
	    'ClienteControllers',
	    'angular-loading-bar',
	    'angularUtils.directives.dirPagination']
	);
	
	trackingApp.config(['$stateProvider', '$urlRouterProvider', 'cfpLoadingBarProvider',
		function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider){
	        var templateUrl = 'partials/render/template/';
	        var widgetUrl = 'partials/renderWidget/name/';
	        cfpLoadingBarProvider.includeSpinner = false;
	
			$urlRouterProvider.otherwise('/');
	
			$stateProvider
	            .state('spedizioni', {
	                cache: false,
	                url: '/',
	                templateUrl: templateUrl + 'tablespedizioni/_spedizioniclienti',
	                controller: 'MainCtrl'
	            })		
	            .state('spedizioni.spedizione', {
	                url: 'spedizioni/:idspedizione/',
	                templateUrl: widgetUrl + 'transita-spedizione.TransitaSpedizione',
	                controller: 'SpedizioneCtrl',
	                resolve : {
	                    spedizione: function(SpedizioneService, $stateParams){
	                        return SpedizioneService.getSpedizione($stateParams.idspedizione).$promise;
	                    }
	                }
	            });
	    }
	]);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

	var spedizioniFilterModule = angular.module('FilterModule', []);
	
	spedizioniFilterModule.directive('textFilter', __webpack_require__(18));
	spedizioniFilterModule.directive('buttonFilter', __webpack_require__(19));
	spedizioniFilterModule.service('SpedizioniFilterService', __webpack_require__(20));

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function() {
	    return {
	        restrict: 'E',
	        replace: true,
	        scope: {
	            filter : '=',
	            run : '&',
	            reset: '&'
	        },
	        template:   '<div class="filter spedizioni-filter">\
		    					<label class="filter-label">{{filter.label}}</label>\
		    					<div class="filter-wrap">\
		        					<input type="text" id="filter-text" class="filter-textfield form-control" \
		        					placeholder="{{filter.placeholder}}" ng-keypress="sendQuery($event)">\
		    					</div>\
							    <div class="filter-active" ng-show="filterActive">\
							    	<div class="filter-value-wrap">\
							    		<div class="filter-value">\
	                                        <span class="filter-value__label">{{filter.label}}</span>\
	                                        <span class="filter-value__value">{{filterVal}}</span>\
	                                    </div>\
							    		<div class="close-filter-btn" ng-click="resetFilter()">\
							    			<i class="fa fa-times"></i>\
							    		</div>\
							    	</div>\
							    </div>\
							</div>',
	        link: function (scope, element){
	            scope.filterActive = false;
	            var filterTextField = element.find("input#filter-text");
	            scope.resetFilter = function(){
	                scope.filterActive = false;
	                filterTextField.val("");
	                scope.reset();
	            };
	            scope.sendQuery = function($event){
	                if($event.keyCode === 13){
	                    scope.filterActive = true;
	                    var value = filterTextField.val();
	                    scope.filterVal = value;
	                    scope.run({filterType: scope.filter.tipo, filterValue : scope.filterVal});
	                    scope.$emit('trfilter:sendQuery');
	                }
	            };
	        }
	    };
	};

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function() {
	    return {
	        restrict: 'E',
	        replace: true,
	        scope: {
	            filter : '=',
	            run : '&',
	            reset: '&'
	        },
	        template:   '<div class="filter filter_button">\
	                        <a class="btn btn-block" \
	                            ng-click="sendQuery($event)">{{filter.label}}</a>\
	                        <div class="filter-active" ng-show="filterActive">\
	                            <div class="filter-value">\
	                                <span class="filter-value__label">{{filter.label}}</span>\
	                            </div>\
	                            <div class="close-filter-btn" ng-click="resetFilter()">\
	                                <i class="fa fa-times"></i>\
	                            </div>\
	                        </div>\
						</div>',
	        link: function (scope){
	            scope.filterActive = false;
	            scope.resetFilter = function(){
	                scope.filterActive = false;
	                scope.reset();
	            };
	            scope.sendQuery = function(){
	                scope.filterActive = true;
	                var value = scope.filter.label;
	                scope.run({filterType: scope.filter.tipo, filterValue : value});
	            };
	        }
	    };
	};

/***/ }),
/* 20 */
/***/ (function(module, exports) {

	"use strict";
	
	module.exports = function () {
	  return {
	    getDestFilter: function () {
	      return {
	        tipo: "dest",
	        placeholder: "Esempio: Mario Rossi",
	        label: "Destinatario",
	      };
	    },
	    getTrackingFilter: function () {
	      return {
	        tipo: "tracking",
	        placeholder: "Numero di tracking",
	        label: "761",
	      };
	    },
	    getNumeroDiSpedizioneFilter: function () {
	      return {
	        tipo: "numero_spedizione",
	        placeholder: "Numero di spedizione",
	        label: "#",
	      };
	    },
	    getDdtFilter: function () {
	      return {
	        tipo: "ddt",
	        placeholder: "Esempio: 104",
	        label: "DDT",
	      };
	    },
	    getDateOptions: function () {
	      return {
	        locale: {
	          applyClass: "btn-green",
	          applyLabel: "Mostra Spedizioni",
	          fromLabel: "From",
	          format: "DD/MM/YYYY",
	          toLabel: "To",
	          cancelLabel: "Annulla",
	          customRangeLabel: "Inserisci Intervallo",
	        },
	        ranges: {
	          "Ultimi 7 Giorni": [moment().subtract(7, "days"), moment()],
	          "Ultimi 30 Giorni": [moment().subtract(30, "days"), moment()],
	        },
	      };
	    },
	  };
	};
	


/***/ }),
/* 21 */,
/* 22 */,
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	var transitaServices = angular.module('TransitaServices', []);
	
	transitaServices.service('SpedizioneService', __webpack_require__(24));

/***/ }),
/* 24 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function($resource){
	    var spedizione = null;
	    var checkState = function(state){
	        return spedizione.stato === state ? true : false;
	    };
	    this.isConsegnata = function(){
	        return checkState('Consegnata');
	    };
	    this.isPrenotata = function(){
	        return checkState('Prenotata');
	    };
	    this.getSpedizione = function(tracking){
	        if(!angular.isUndefined(tracking)){
	            var Spedizione = $resource('../api/spedizione/:tracking');
	            spedizione = Spedizione.get({ tracking : tracking });
	            return spedizione;
	        }
	    };
	    this.getSpedizioni = function() {
	        return $resource('../api/spedizioni/', {
	            dataIniziale: '@startDate',
	            dataFinale: '@endDate',
	            ddt: '@ddt',
	            dest: '@dest',
	            page: '@page'
	        });
	    }
	};

/***/ }),
/* 25 */
/***/ (function(module, exports) {

	var trConstants = angular.module('TransitaConstants', []);
	
	trConstants.constant('TransitaConstants', {
	    'FORMS' : {
	        'EDIT': 'modifica',
	        'CREATE': 'crea'
	    },
	    'API_CLIENTE' : {
	
	            'SPEDIZIONI_CLIENTE' : '../api/spdcliente/'
	    },
	    'API_MAIN' :{
	        'CLIENTI' : '../api/clienti',
	        'CREA_CLIENTE' : 'clienti/create',
	        'ATTIVA_CLIENTE' : 'clienti/attiva',
	        'DISATTIVA_CLIENTE' : 'clienti/disattiva',
	        'SPEDIZIONI' : '../api/spedizioni',
	        'SPEDIZIONE' : '../api/spedizione',
	        'CARICA_SPEDIZIONI' : 'fileSpedizioni/carica',
	        'SALVA_SPEDIZIONI' : 'fileSpedizioni/salva'
	    },
	    'EVENTS' : {
	        'FILE_SPEDIZIONI_CARICATO' : 'uploadFileSpedizioni'
	    }
	});

/***/ }),
/* 26 */,
/* 27 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 36 */,
/* 37 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

	var clienteControllers = angular.module('ClienteControllers', []);
	
	clienteControllers.controller('MainCtrl', __webpack_require__(43));
	clienteControllers.controller('SpedizioneCtrl', __webpack_require__(44));

/***/ }),
/* 43 */
/***/ (function(module, exports) {

	module.exports = function ($scope, SpedizioneService, SpedizioniFilterService){
	    $scope.loadComplete = true;
	    $scope.ddtFilter = SpedizioniFilterService.getDdtFilter();
	    $scope.destFilter = SpedizioniFilterService.getDestFilter();
	    $scope.numSpedizioneFilter = SpedizioniFilterService.getNumeroDiSpedizioneFilter();
	    var initListSpedizioni = function(){
	        $scope.spedizioni = {};
	        $scope.spedizioni.totalSpd = 0;
	    };
	    initListSpedizioni();
	    $scope.resetSpedizioni = function(){
	        initListSpedizioni();
	    };
	    $scope.runFilter = function(filterType, filterValue){
	        $scope.loadComplete = false;
	        if(angular.isUndefined(filterValue) === false){
	            if(filterValue === ""){
	                filterValue = null;
	            }
	            var param = { page : 1 };
	            param[filterType] = filterValue;
	            var spedizioni = SpedizioneService.getSpedizioni().query(param, function(){
	                $scope.spedizioni.list = spedizioni[3].spedizioni;
	                if(spedizioni[0].count === 0){
	                    $scope.spedizioni.totalSpd = -1;
	                    $scope.loadComplete = true;
	                } else {
	                    $scope.spedizioni.totalSpd = spedizioni[0].count;
	                    $scope.loadComplete = true;
	                }
	            });
	        }
	    };
	};

/***/ }),
/* 44 */
/***/ (function(module, exports) {

	module.exports = function ($scope, spedizione, SpedizioneService){
	    $scope.isConsegnata = SpedizioneService.isConsegnata();
	    $scope.spedizione = spedizione;
	};

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(27);
	__webpack_require__(35);
	__webpack_require__(46);
	__webpack_require__(37);

/***/ }),
/* 46 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=clientebundle.js.map