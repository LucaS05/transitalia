'use strict';

module.exports = function($resource) {
    var cliente;
    this.setCliente = function(clt){
        if(!angular.isUndefined(clt)){
            cliente = clt;
        }
    };
    this.getCliente = function(){
        return cliente;
    };
    this.salvaCliente = function(cliente, successCallback){
        if(!angular.isUndefined(cliente)){
            var Cliente = $resource("clienti/create");
            var cliente = Cliente.save(cliente, function(){
                successCallback(cliente);
            });
        }
    };
    this.modificaCliente = function(cliente, successCallback){
        if(!angular.isUndefined(cliente)){
            var Cliente = $resource("clienti/edit");
            var cliente = Cliente.save(cliente, function(){
                successCallback(cliente);
            });
        }
    };
    this.reqCliente = function(idcliente){
        if(!angular.isUndefined(idcliente)){
            var Cliente = $resource("../api/cliente/:idcliente");
            cliente = Cliente.get({ idcliente : idcliente });
            return cliente;
        }
    };
    this.checkUsername = function(username, successCallback, errorCallback){
        if(!angular.isUndefined(username)){
            var Cliente = $resource("clienti/checkUsername");
            var cliente = Cliente.get({username : username}, function(){
                successCallback();
            }, function(){
                errorCallback();
            });
            return cliente.$promise;
        }
    };
    this.editUsername = function(username, successCallback){
        if(!angular.isUndefined(username)){
            var Cliente = $resource('clienti/editUsername');
            var cliente = Cliente.save(username, function(){
                successCallback(username);
            });
        }
    };
    this.editPassword = function(password, successCallback){
        if(!angular.isUndefined(password)){
            var Cliente = $resource('clienti/editPassword');
            var cliente = Cliente.save(password, function(){
                successCallback(password);
            });
        }
    };
    this.addRagSoc = function(ragSoc, successCallback){
        if(!angular.isUndefined(ragSoc)){
            var RagSoc = $resource('clienti/addRagSoc');
            var cliente = RagSoc.save(ragSoc, function(){
                successCallback(ragSoc);
            });
        }
    };
    this.removeRagSoc = function(ragSoc, successCallback){
        if(!angular.isUndefined(ragSoc)){
            var RagSoc = $resource('clienti/removeRagSoc');
            var cliente = RagSoc.save(ragSoc, function(){
                successCallback(ragSoc);
            });
        }
    };
};