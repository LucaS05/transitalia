'use strict';

module.exports = function($resource) {
    this.getStati = function(){
        return $resource('../api/stati', {
            dd: '@dd'
        });
    };
    this.getStato = function(idstato){
        var Spedizione = $resource('../api/stato/:idstato');
        return Spedizione.get({ idstato : idstato });
    };
    this.modificaStato = function(stato, successCallback){
        if(!angular.isUndefined(stato)){
            var Stato = $resource("stato/update");
            var stato = Stato.save(stato, function(){
                successCallback(stato);
            });
        }
    }
};