module.exports = function($scope, $state, $stateParams) {
    if ($stateParams.uploadData !== null) {
        $scope.uploadSalvate = $stateParams.uploadData.salvate;
        $scope.uploadNonSalvate = $stateParams.uploadData.non_salvate;
        if ($scope.spdNonSalvate > 0) {
            $scope.spdNonSalvateList = $stateParams.uploadData.spedizioni;
        }
    } else {
        $state.go('aggiorna');
    }
    $scope.showErrors = function(){
        if ($scope.uploadNonSalvate > 0) {
            $state.go('aggiorna.riepilogo.errori', {errori: $stateParams.uploadData.spedizioni});
        }
    };
    //$scope.showErrors = function(){
    //    $state.go('aggiorna.riepilogo.errori');
    //};
    //
    //$scope.uploadSalvate = 88;
    //$scope.uploadNonSalvate = 50;
};
