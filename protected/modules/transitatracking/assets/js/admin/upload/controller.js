module.exports = function($scope, $state, $http, $timeout) {
    $scope.$on("uploadFileSpedizioni", function(event, data){
        $scope.uploadFile(data);
    });

    $scope.uploadClick = function(){
        $timeout(function() {
            angular.element("#filespedizioni").trigger("click");
        }, 1);
    };

    $scope.uploadFile = function(file){
        var fd = new FormData();
        fd.append("FileSpedizioni", file);

        $scope.loading = true;

        $http.post("fileSpedizioni/carica", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(data, status, headers, config) {

            var loadingBar = $(".loadingbar .loading-text");
            var loadingText = loadingBar.text();
            loadingBar.html("Salvataggio delle spedizioni in corso...");

            $http.post("fileSpedizioni/salva")
                .success(function(){
                    loadingBar.html("Spedizioni salvate correttamente!");
                    $scope.loading = false;
                    $state.go('aggiorna.riepilogo');
                }).error(function(data){
                    $state.go('aggiorna.riepilogo', {uploadData : data});
                });
        }).error(function(data) {
            console.log(data);
            $scope.loading = false;
            var errorBar = $("#messagebar");
            errorBar.html(data);
            errorBar.addClass("errormsg");
        });
    };
};