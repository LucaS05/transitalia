'use strict';

module.exports = function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            listaErrori: '='
        },
        templateUrl: 'partials/render/template/aggiorna/_errori-wizard',
        link: function(scope) {
            scope.errori = scope.listaErrori;
            scope.total = scope.errori.length;
            scope.currIndex = 0;
            var isErroreUndefined = function(index){
                if (typeof scope.errori[index] == 'undefined') {
                    return true;
                } else {
                    return false;
                }
            };
            var getNumSpedizione = function(index){
                return scope.errori[index].spedizione.num_spedizione;
            };
            var updateNrSpedizione = function() {
                if (isErroreUndefined(scope.currIndex + 1) == false) {
                    scope.nextNrSpedizione = getNumSpedizione(scope.currIndex + 1);
                } else {
                    scope.nextNrSpedizione = -1;
                }
                if (isErroreUndefined(scope.currIndex - 1) == false) {
                    scope.prevNrSpedizione = getNumSpedizione(scope.currIndex - 1);
                } else {
                    scope.prevNrSpedizione = -1;
                }
            };

            scope.mainError = scope.errori[scope.currIndex];
            updateNrSpedizione();

            scope.getNextError = function(){
                scope.currIndex++;
                scope.mainError = scope.errori[scope.currIndex];
                updateNrSpedizione();
            };

            scope.getPrevError = function(){
                scope.currIndex--;
                scope.mainError = scope.errori[scope.currIndex];
                updateNrSpedizione();
            };
        }
    };
};