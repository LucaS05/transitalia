var transitaUpload = angular.module('transitaUpload', []);

transitaUpload.controller('UploadController', require('./controller'));
transitaUpload.controller('RiepilogoController', require('./riepilogo'));
transitaUpload.controller('ErroriController', require('./errori'));
transitaUpload.directive('erroriWizard', require('./wizerrori'));