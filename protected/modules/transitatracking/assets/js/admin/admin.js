"use strict";

require("./controllers");

require("./upload");

require("../filters");

require("../buttons");

require("../spedizioni");

require("../trconstants");

require("../less/admin");

var trackingApp = angular.module("TransitaTracking", [
  "ui.router",
  "ngResource",
  "ngAnimate",
  "TransitaServices",
  "transitaUpload",
  "FilterModule",
  "ButtonModule",
  "transitaControllers",
  "TransitaConstants",
  "angularUtils.directives.dirPagination",
  "angucomplete-alt",
  "daterangepicker",
  "angular-loading-bar",
]);

trackingApp.service("ClienteService", require("./cliente/service"));
trackingApp.service("StatoService", require("./stato/service"));

trackingApp.config(require("./config"));

trackingApp.directive("fileModel", [
  "$parse",
  function ($parse) {
    return {
      restrict: "A",
      link: function (scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind("change", function () {
          scope.$apply(function () {
            modelSetter(scope, element[0].files[0]);
            scope.$emit("uploadFileSpedizioni", scope.fileSp);
          });
        });
      },
    };
  },
]);

trackingApp.directive("transitaLoading", [
  "$timeout",
  function ($timeout) {
    return {
      restrict: "E",
      replace: true,
      template:
        '<div class="loadingbar" style="display: none;">\
                            <span id="spinner"></span>\
                            <span class="loading-text">Caricamento file spedizioni in corso...</span>\
                       </div>',
      link: function (scope, element) {
        var hideBar = function () {
          $(element).hide();
        };
        scope.$watch("loading", function (val) {
          if (val) {
            $(element).show();
          } else {
            scope.spdSalvate = true;
            $timeout(hideBar, 3000);
          }
        });
      },
    };
  },
]);

trackingApp.directive("trFull", [
  "$window",
  function ($window) {
    return {
      restrict: "A",
      link: function (scope, element) {
        var setElementHeight = function () {
          element.height($window.innerHeight - 40);
        };
        angular.element(window).on("resize", setElementHeight);
        setElementHeight();
      },
    };
  },
]);

trackingApp.directive("username", [
  "$http",
  "$q",
  function ($http, $q) {
    return {
      require: "ngModel",
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$asyncValidators.username = function (modelValue, viewValue) {
          var def = $q.defer();
          var checkService = $http.get("clienti/checkUsername/", {
            params: { username: modelValue },
          });
          checkService.then(
            function successCallback() {
              def.resolve();
            },
            function errorCallback() {
              def.reject();
            }
          );
          return def.promise;
        };
      },
    };
  },
]);

trackingApp.directive("btnaddcliente", [
  "TransitaConstants",
  function (transitaConstants) {
    return {
      restrict: "E",
      replace: true,
      scope: {
        formValid: "=",
        formType: "=",
      },
      template:
        '<input type="submit" class="btn btn-block btn-success"\
		                value="{{btnLabel}}" />',
      link: function (scope, element, attr) {
        if (scope.formType === transitaConstants.FORMS.EDIT) {
          scope.btnLabel = "Salva le modifiche";
        } else if (scope.formType === transitaConstants.FORMS.CREATE) {
          scope.btnLabel = "Inserisci Cliente";
        }
        scope.$watch("formValid", function (val) {
          if (val === true) {
            if (scope.formType === transitaConstants.FORMS.EDIT) {
              $(element).val("Modifica cliente in corso...");
            } else if (scope.formType === transitaConstants.FORMS.CREATE) {
              $(element).val("Inserimento cliente in corso...");
            }
            $(element).prop("disabled", true);
          } else if (val === false) {
            $(element).prop("disabled", true);
          }
        });
      },
    };
  },
]);

