/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	__webpack_require__(4);
	
	__webpack_require__(12);
	
	__webpack_require__(17);
	
	__webpack_require__(21);
	
	__webpack_require__(23);
	
	__webpack_require__(25);
	
	__webpack_require__(26);
	
	var trackingApp = angular.module("TransitaTracking", [
	  "ui.router",
	  "ngResource",
	  "ngAnimate",
	  "TransitaServices",
	  "transitaUpload",
	  "FilterModule",
	  "ButtonModule",
	  "transitaControllers",
	  "TransitaConstants",
	  "angularUtils.directives.dirPagination",
	  "angucomplete-alt",
	  "daterangepicker",
	  "angular-loading-bar",
	]);
	
	trackingApp.service("ClienteService", __webpack_require__(39));
	trackingApp.service("StatoService", __webpack_require__(40));
	
	trackingApp.config(__webpack_require__(41));
	
	trackingApp.directive("fileModel", [
	  "$parse",
	  function ($parse) {
	    return {
	      restrict: "A",
	      link: function (scope, element, attrs) {
	        var model = $parse(attrs.fileModel);
	        var modelSetter = model.assign;
	
	        element.bind("change", function () {
	          scope.$apply(function () {
	            modelSetter(scope, element[0].files[0]);
	            scope.$emit("uploadFileSpedizioni", scope.fileSp);
	          });
	        });
	      },
	    };
	  },
	]);
	
	trackingApp.directive("transitaLoading", [
	  "$timeout",
	  function ($timeout) {
	    return {
	      restrict: "E",
	      replace: true,
	      template:
	        '<div class="loadingbar" style="display: none;">\
	                            <span id="spinner"></span>\
	                            <span class="loading-text">Caricamento file spedizioni in corso...</span>\
	                       </div>',
	      link: function (scope, element) {
	        var hideBar = function () {
	          $(element).hide();
	        };
	        scope.$watch("loading", function (val) {
	          if (val) {
	            $(element).show();
	          } else {
	            scope.spdSalvate = true;
	            $timeout(hideBar, 3000);
	          }
	        });
	      },
	    };
	  },
	]);
	
	trackingApp.directive("trFull", [
	  "$window",
	  function ($window) {
	    return {
	      restrict: "A",
	      link: function (scope, element) {
	        var setElementHeight = function () {
	          element.height($window.innerHeight - 40);
	        };
	        angular.element(window).on("resize", setElementHeight);
	        setElementHeight();
	      },
	    };
	  },
	]);
	
	trackingApp.directive("username", [
	  "$http",
	  "$q",
	  function ($http, $q) {
	    return {
	      require: "ngModel",
	      link: function (scope, elm, attrs, ctrl) {
	        ctrl.$asyncValidators.username = function (modelValue, viewValue) {
	          var def = $q.defer();
	          var checkService = $http.get("clienti/checkUsername/", {
	            params: { username: modelValue },
	          });
	          checkService.then(
	            function successCallback() {
	              def.resolve();
	            },
	            function errorCallback() {
	              def.reject();
	            }
	          );
	          return def.promise;
	        };
	      },
	    };
	  },
	]);
	
	trackingApp.directive("btnaddcliente", [
	  "TransitaConstants",
	  function (transitaConstants) {
	    return {
	      restrict: "E",
	      replace: true,
	      scope: {
	        formValid: "=",
	        formType: "=",
	      },
	      template:
	        '<input type="submit" class="btn btn-block btn-success"\
			                value="{{btnLabel}}" />',
	      link: function (scope, element, attr) {
	        if (scope.formType === transitaConstants.FORMS.EDIT) {
	          scope.btnLabel = "Salva le modifiche";
	        } else if (scope.formType === transitaConstants.FORMS.CREATE) {
	          scope.btnLabel = "Inserisci Cliente";
	        }
	        scope.$watch("formValid", function (val) {
	          if (val === true) {
	            if (scope.formType === transitaConstants.FORMS.EDIT) {
	              $(element).val("Modifica cliente in corso...");
	            } else if (scope.formType === transitaConstants.FORMS.CREATE) {
	              $(element).val("Inserimento cliente in corso...");
	            }
	            $(element).prop("disabled", true);
	          } else if (val === false) {
	            $(element).prop("disabled", true);
	          }
	        });
	      },
	    };
	  },
	]);
	


/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	var transitaControllers = angular.module('transitaControllers', []);
	
	transitaControllers.controller('ClientiCrtl', __webpack_require__(5));
	transitaControllers.controller('ClienteCtrl', __webpack_require__(6));
	transitaControllers.controller('RevClienteCrtl', __webpack_require__(7));
	transitaControllers.controller('SpedizioneCtrl', __webpack_require__(8))
	transitaControllers.controller('MainCtrl', __webpack_require__(9));
	transitaControllers.controller('StatiCtrl', __webpack_require__(10));
	transitaControllers.controller('StatoCtrl', __webpack_require__(11));


/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = function($scope, $http) {
	    $scope.moduleState = 'list';
	
	    $http.get('../api/clienti').success(function(data) {
	        if (data instanceof Array) {
	            data.forEach(function(elem) {
	                if (elem.stato == '1') {
	                    elem.stato = 'attivo';
	                } else if (elem.stato == '0') {
	                    elem.stato = 'disattivo';
	                }
	            });
	            $scope.clienti = data;
	        }
	    });
	
	    var setState = function(cliente, state){
	        var stateObj = {username : cliente.username, state : state};
	        $http.post('clienti/setState', stateObj).
	        success(function() {
	                if(state === 1){
	                    cliente.stato = 'attivo';
	                } else if(state == 0){
	                    cliente.stato = 'disattivo';
	                }
	        });
	    };
	
	    $scope.enableUser = function(cliente) {
	        setState(cliente, 1);
	    };
	
	    $scope.disableUser = function(cliente) {
	        setState(cliente, 0);
	    };
	};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	module.exports = function($scope, ClienteService, $state, cliente, form, TransitaConstants) {
	    $scope.formType = form.tipo;
	    $scope.formValid;
	    $scope.editMode = false;
	    $scope.editPassword = false;
	    $scope.cliente = {};
	    $scope.listaRagSoc = [];
	    $scope.validationFailed = false;
	    var selectedRagSoc;
	
	    if(form.tipo === TransitaConstants.FORMS.EDIT){
	        $scope.editMode = true;
	    }
	
	    if(!angular.isUndefined(cliente)){
	        $scope.cliente.username = cliente.username;
	        $scope.listaRagSoc = cliente.rag_soc;
	    }
	
	    $scope.clienteSelected = function(selected){
	        if(!angular.isUndefined(selected)){
	            selectedRagSoc = selected.originalObject.rag_soc;
	        }
	    };
	
	    var oldPassword;
	    $scope.editPwdClick = function(){
	        oldPassword = $scope.cliente.password;
	        $scope.editPassword = !$scope.editPassword;
	    };
	
	    $scope.undoEditPwd = function(){
	        $scope.editPassword = !$scope.editPassword;
	        $scope.cliente.password = oldPassword;
	    };
	
	    $scope.dataClienteChanged = function(){
	        var pwdRagSoc, pwdCodCliente;
	        if(!angular.isUndefined($scope.listaRagSoc[0])){
	            pwdRagSoc = $scope.listaRagSoc[0];
	            pwdRagSoc = pwdRagSoc.replace(/[^a-zA-Z]/g, '');
	            pwdRagSoc = pwdRagSoc.slice(0,4);
	            pwdRagSoc = pwdRagSoc.toUpperCase();
	        }
	        if(!angular.isUndefined($scope.cliente.codcliente)){
	            pwdCodCliente = $scope.cliente.codcliente + '!';
	        }
	        if(pwdCodCliente && pwdRagSoc){
	            $scope.cliente.password = pwdRagSoc + pwdCodCliente;
	        } else {
	            $scope.cliente.password = undefined;
	        }
	    };
	
	    $scope.checkUsername = function(){
	        var errorCallback = function(){
	            $scope.sameUsernameError = true;
	        };
	        var successCallback = function(){
	            $scope.sameUsernameError = false;
	        };
	        if($scope.cliente.username.length > 3) {
	            ClienteService.checkUsername($scope.cliente.username, successCallback,
	                errorCallback);
	        }
	    };
	
	    var userAddedCallback = function(cliente){
	        ClienteService.setCliente(cliente);
	        var mode = {};
	        if($scope.editMode){
	            mode.action = 'edit';
	        } else {
	            mode.action = 'create';
	        }
	        $state.go('clienti.riepilogo', mode);
	    };
	
	    $scope.addCliente = function(clienteForm) {
	        if ($scope.listaRagSoc.length === 0 || $scope.sameUsernameError == true) {
	            $scope.validationFailed = true;
	            clienteForm.$invalid = true;
	        } else if(clienteForm.$valid) {
	            $scope.validationFailed = false;
	            $scope.formValid = true;
	            $scope.cliente.listaRagSoc = $scope.listaRagSoc;
	            ClienteService.salvaCliente($scope.cliente, userAddedCallback);
	        }
	    };
	
	    $scope.ediussucc = false;
	    $scope.edipwsucc = false;
	
	    $scope.editMessageClose = function(){
	        $scope.ediussucc = false;
	        $scope.edipwsucc = false;
	        $scope.formValid = false;
	    };
	
	    var successEditUsername = function(response){
	        $scope.ediussucc = true;
	        $scope.cliente.username = response.new_username;
	        $scope.editmessage = 'Nuovo response: ' + response.new_username;
	    };
	
	    var successEditPassword = function(response){
	        $scope.edipwsucc = true;
	        $scope.cliente.password = response.password;
	        $scope.editmessage = 'Nuova password: ' + response.password;
	    };
	
	    $scope.sendForm = function(form){
	        if(form.$valid){
	            $scope.formValid = true;
	            switch(form.$name){
	                case 'usernameForm':
	                    ClienteService.editUsername($scope.cliente, successEditUsername);
	                    break;
	                case 'passwordForm':
	                    ClienteService.editPassword($scope.cliente, successEditPassword);
	                    break;
	                case 'ragSocForm':
	                    var datiCliente = { username : $scope.cliente.username, ragsoc : selectedRagSoc };
	                    ClienteService.addRagSoc(datiCliente, successAddRagSoc);
	                    break;
	                default:
	                    console.error('Add form case');
	                    break;
	            }
	        }
	    };
	
	    var successAddRagSoc = function(ragioneSociale){
	        $scope.listaRagSoc.push(ragioneSociale);
	        $scope.$broadcast('angucomplete-alt:clearInput');
	        selectedRagSoc = undefined;
	    };
	
	    var successDeletedRagSoc = function(index){
	        $scope.listaRagSoc.splice(index, 1);
	    };
	
	    $scope.removeRagSoc = function(index){
	        if($scope.listaRagSoc.length === 1){
	            $scope.lastRagSoc = true;
	        } else {
	            var ragSoc = $scope.listaRagSoc[index];
	            if (!angular.isUndefined(ragSoc)) {
	                var datiCliente = {ragsoc: ragSoc};
	                ClienteService.removeRagSoc(datiCliente, function () {
	                    successDeletedRagSoc(index)
	                });
	            }
	        }
	    };
	
	    $scope.addRagSoc = function(){
	        if(!angular.isUndefined(selectedRagSoc)) {
	            if($scope.editMode === true) {
	                var datiCliente = {ragsoc: selectedRagSoc, username: $scope.cliente.username};
	                ClienteService.addRagSoc(datiCliente, function () {
	                    successAddRagSoc(selectedRagSoc)
	                });
	            } else {
	                $scope.listaRagSoc.push(selectedRagSoc);
	                $scope.dataClienteChanged();
	                $scope.$broadcast('angucomplete-alt:clearInput');
	            }
	        }
	    };
	};

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	module.exports = function($scope, ClienteService, $state, $stateParams) {
	        $scope.cliente = ClienteService.getCliente();
	        if(angular.isUndefined($scope.cliente)){
	            $state.go("clienti");
	        }
	        $scope.action = $stateParams.action;
	};

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	module.exports = function($scope, spedizione, SpedizioneService) {
	    $scope.isConsegnata = SpedizioneService.isConsegnata();
	    $scope.isPrenotata = SpedizioneService.isPrenotata();
	    $scope.spedizione = spedizione;
	};

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	module.exports = function (
	  $scope,
	  SpedizioneService,
	  $http,
	  $window,
	  $timeout
	) {
	  var startPage = 1;
	  $scope.loadComplete = false;
	  $scope.spedizioni = { list: [], totalSpd: -1 };
	  $scope.datePicker = {};
	  $scope.datePicker.date = { startDate: null, endDate: null };
	  moment.locale("it");
	  $scope.datePicker.opts = {
	    locale: {
	      applyClass: "btn-green",
	      applyLabel: "Mostra Spedizioni",
	      fromLabel: "From",
	      format: "DD/MM/YYYY",
	      toLabel: "To",
	      cancelLabel: "Annulla",
	      customRangeLabel: "Inserisci Intervallo",
	    },
	    ranges: {
	      "Ultimi 7 Giorni": [moment().subtract(7, "days"), moment()],
	      "Ultimi 30 Giorni": [moment().subtract(30, "days"), moment()],
	    },
	  };
	
	  $scope.trackingFilter = {
	    tipo: "tracking",
	    placeholder: "Numero di tracking",
	    label: "761",
	  };
	
	  $scope.ddtFilter = {
	    tipo: "ddt",
	    placeholder: "Esempio: 104",
	    label: "DDT",
	  };
	
	  $scope.destFilter = {
	    tipo: "dest",
	    placeholder: "Esempio: Mario Rossi",
	    label: "Destinatario",
	  };
	
	  $scope.resetSpedizioni = function () {
	    getRisultatiPagina(startPage);
	    $scope.selStartDate = null;
	    $scope.selEndDate = null;
	    $scope.datePicker.date = { startDate: null, endDate: null };
	  };
	
	  $scope.$watch(
	    "datePicker.date",
	    function (newDate) {
	      if (newDate.startDate && newDate.endDate) {
	        $scope.selStartDate = newDate.startDate.format("DD/MM/YYYY");
	        $scope.selEndDate = newDate.endDate.format("DD/MM/YYYY");
	        var dateRange = {
	          dataIniziale: newDate.startDate.format("YYYY-MM-DD"),
	          dataFinale: newDate.endDate.format("YYYY-MM-DD"),
	        };
	        var spedizioni = SpedizioneService.getSpedizioni().query(
	          dateRange,
	          function () {
	            $scope.spedizioni.totalSpd = spedizioni[0].count;
	            $scope.spedizioni.list = spedizioni[3].spedizioni;
	          }
	        );
	      }
	    },
	    false
	  );
	
	  getRisultatiPagina(startPage);
	
	  $scope.pageChanged = function (pageNumber) {
	    if (startPage === pageNumber) {
	      startPage = -1;
	    } else {
	      getRisultatiPagina(pageNumber);
	    }
	  };
	
	  function getRisultatiPagina(pageNumber) {
	    $scope.loadComplete = false;
	    $window.scrollTo(0, 0);
	    var getUrl = "../api/spedizioni?page=" + pageNumber;
	    $http.get(getUrl).success(function (data) {
	      debugger;
	      if (!(data instanceof Array)) {
	        $scope.spedizioni.list = [];
	      } else {
	        $timeout(function () {
	          $scope.loadComplete = true;
	          $scope.spedizioni.list = data[3].spedizioni;
	          $scope.spedizioni.totalSpd = data[0].count;
	          $scope.limit = data[4].limit;
	        }, 2000);
	      }
	    });
	  }
	
	  $scope.runFilter = function (filterType, filterValue) {
	    if (angular.isUndefined(filterValue) === false) {
	      if (filterValue === "") {
	        filterValue = null;
	      }
	      var param = { page: 1 };
	      param[filterType] = filterValue;
	      var spedizioni = SpedizioneService.getSpedizioni().query(
	        param,
	        function () {
	          debugger;
	          $scope.spedizioni.list = spedizioni[3].spedizioni;
	          if (spedizioni[0].count === 0) {
	            $scope.spedizioni.totalSpd = -1;
	          } else {
	            $scope.spedizioni.totalSpd = spedizioni[0].count;
	          }
	          $scope.loadComplete = true;
	        }
	      );
	    }
	  };
	
	  $scope.$on("trfilter:sendQuery", function () {
	    $scope.loadComplete = false;
	  });
	};
	


/***/ }),
/* 10 */
/***/ (function(module, exports) {

	module.exports = function($scope, stati, StatoService) {
	    var originalList = stati;
	    $scope.statiList = originalList;
	    $scope.daDefinireFilter = {tipo : "dd", label : "Da Definire"};
	    var getStatiList = function (param) {
	        var statiList = StatoService.getStati().query(param, function(){
	            $scope.statiList = statiList;
	        });
	    };
	    $scope.runFilter = function(filterType){
	        var param = {};
	        param[filterType] = 1;
	        getStatiList(param);
	    };
	    $scope.resetStati = function() {
	        getStatiList(null);
	    }
	};

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	module.exports = function($scope, stato, StatoService) {
	    $scope.stato = stato;
	    var editSuccessCallback = function(){
	        $scope.showSuccess = true;
	    };
	    $scope.editStato = function(statoForm){
	        StatoService.modificaStato($scope.stato, editSuccessCallback);
	    }
	};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	var transitaUpload = angular.module('transitaUpload', []);
	
	transitaUpload.controller('UploadController', __webpack_require__(13));
	transitaUpload.controller('RiepilogoController', __webpack_require__(14));
	transitaUpload.controller('ErroriController', __webpack_require__(15));
	transitaUpload.directive('erroriWizard', __webpack_require__(16));

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = function($scope, $state, $http, $timeout) {
	    $scope.$on("uploadFileSpedizioni", function(event, data){
	        $scope.uploadFile(data);
	    });
	
	    $scope.uploadClick = function(){
	        $timeout(function() {
	            angular.element("#filespedizioni").trigger("click");
	        }, 1);
	    };
	
	    $scope.uploadFile = function(file){
	        var fd = new FormData();
	        fd.append("FileSpedizioni", file);
	
	        $scope.loading = true;
	
	        $http.post("fileSpedizioni/carica", fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        }).success(function(data, status, headers, config) {
	
	            var loadingBar = $(".loadingbar .loading-text");
	            var loadingText = loadingBar.text();
	            loadingBar.html("Salvataggio delle spedizioni in corso...");
	
	            $http.post("fileSpedizioni/salva")
	                .success(function(){
	                    loadingBar.html("Spedizioni salvate correttamente!");
	                    $scope.loading = false;
	                    $state.go('aggiorna.riepilogo');
	                }).error(function(data){
	                    $state.go('aggiorna.riepilogo', {uploadData : data});
	                });
	        }).error(function(data) {
	            console.log(data);
	            $scope.loading = false;
	            var errorBar = $("#messagebar");
	            errorBar.html(data);
	            errorBar.addClass("errormsg");
	        });
	    };
	};

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	module.exports = function($scope, $state, $stateParams) {
	    if ($stateParams.uploadData !== null) {
	        $scope.uploadSalvate = $stateParams.uploadData.salvate;
	        $scope.uploadNonSalvate = $stateParams.uploadData.non_salvate;
	        if ($scope.spdNonSalvate > 0) {
	            $scope.spdNonSalvateList = $stateParams.uploadData.spedizioni;
	        }
	    } else {
	        $state.go('aggiorna');
	    }
	    $scope.showErrors = function(){
	        if ($scope.uploadNonSalvate > 0) {
	            $state.go('aggiorna.riepilogo.errori', {errori: $stateParams.uploadData.spedizioni});
	        }
	    };
	    //$scope.showErrors = function(){
	    //    $state.go('aggiorna.riepilogo.errori');
	    //};
	    //
	    //$scope.uploadSalvate = 88;
	    //$scope.uploadNonSalvate = 50;
	};


/***/ }),
/* 15 */
/***/ (function(module, exports) {

	module.exports = function($scope, $stateParams) {
	    //var spedizione = {
	    //    stato: "1",
	    //    tracking: null,
	    //    num_spedizione: "7131",
	    //    data: "2016-07-25",
	    //    ddt: "30",
	    //    mittente: "102",
	    //    colli: "4",
	    //    destinatario: "MASCHILLA ANTONIA MICHELA",
	    //    luogo_arrivo: "BARLETTA",
	    //    data_consegna: "2016-07-27",
	    //    firmato_da: "MASCHILLA",
	    //    id: null
	    //};
	    //
	    //var errori = {
	    //    tracking: ["Tracking non inserito"]
	    //};
	    //
	    //var arrElem = {
	    //    spedizione : spedizione,
	    //    errori     : errori
	    //};
	    //
	    //
	    //$scope.errori = [arrElem];
	    $scope.errori = $stateParams.errori;
	};

/***/ }),
/* 16 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function() {
	    return {
	        restrict: 'E',
	        replace: true,
	        scope: {
	            listaErrori: '='
	        },
	        templateUrl: 'partials/render/template/aggiorna/_errori-wizard',
	        link: function(scope) {
	            scope.errori = scope.listaErrori;
	            scope.total = scope.errori.length;
	            scope.currIndex = 0;
	            var isErroreUndefined = function(index){
	                if (typeof scope.errori[index] == 'undefined') {
	                    return true;
	                } else {
	                    return false;
	                }
	            };
	            var getNumSpedizione = function(index){
	                return scope.errori[index].spedizione.num_spedizione;
	            };
	            var updateNrSpedizione = function() {
	                if (isErroreUndefined(scope.currIndex + 1) == false) {
	                    scope.nextNrSpedizione = getNumSpedizione(scope.currIndex + 1);
	                } else {
	                    scope.nextNrSpedizione = -1;
	                }
	                if (isErroreUndefined(scope.currIndex - 1) == false) {
	                    scope.prevNrSpedizione = getNumSpedizione(scope.currIndex - 1);
	                } else {
	                    scope.prevNrSpedizione = -1;
	                }
	            };
	
	            scope.mainError = scope.errori[scope.currIndex];
	            updateNrSpedizione();
	
	            scope.getNextError = function(){
	                scope.currIndex++;
	                scope.mainError = scope.errori[scope.currIndex];
	                updateNrSpedizione();
	            };
	
	            scope.getPrevError = function(){
	                scope.currIndex--;
	                scope.mainError = scope.errori[scope.currIndex];
	                updateNrSpedizione();
	            };
	        }
	    };
	};

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

	var spedizioniFilterModule = angular.module('FilterModule', []);
	
	spedizioniFilterModule.directive('textFilter', __webpack_require__(18));
	spedizioniFilterModule.directive('buttonFilter', __webpack_require__(19));
	spedizioniFilterModule.service('SpedizioniFilterService', __webpack_require__(20));

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function() {
	    return {
	        restrict: 'E',
	        replace: true,
	        scope: {
	            filter : '=',
	            run : '&',
	            reset: '&'
	        },
	        template:   '<div class="filter spedizioni-filter">\
		    					<label class="filter-label">{{filter.label}}</label>\
		    					<div class="filter-wrap">\
		        					<input type="text" id="filter-text" class="filter-textfield form-control" \
		        					placeholder="{{filter.placeholder}}" ng-keypress="sendQuery($event)">\
		    					</div>\
							    <div class="filter-active" ng-show="filterActive">\
							    	<div class="filter-value-wrap">\
							    		<div class="filter-value">\
	                                        <span class="filter-value__label">{{filter.label}}</span>\
	                                        <span class="filter-value__value">{{filterVal}}</span>\
	                                    </div>\
							    		<div class="close-filter-btn" ng-click="resetFilter()">\
							    			<i class="fa fa-times"></i>\
							    		</div>\
							    	</div>\
							    </div>\
							</div>',
	        link: function (scope, element){
	            scope.filterActive = false;
	            var filterTextField = element.find("input#filter-text");
	            scope.resetFilter = function(){
	                scope.filterActive = false;
	                filterTextField.val("");
	                scope.reset();
	            };
	            scope.sendQuery = function($event){
	                if($event.keyCode === 13){
	                    scope.filterActive = true;
	                    var value = filterTextField.val();
	                    scope.filterVal = value;
	                    scope.run({filterType: scope.filter.tipo, filterValue : scope.filterVal});
	                    scope.$emit('trfilter:sendQuery');
	                }
	            };
	        }
	    };
	};

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function() {
	    return {
	        restrict: 'E',
	        replace: true,
	        scope: {
	            filter : '=',
	            run : '&',
	            reset: '&'
	        },
	        template:   '<div class="filter filter_button">\
	                        <a class="btn btn-block" \
	                            ng-click="sendQuery($event)">{{filter.label}}</a>\
	                        <div class="filter-active" ng-show="filterActive">\
	                            <div class="filter-value">\
	                                <span class="filter-value__label">{{filter.label}}</span>\
	                            </div>\
	                            <div class="close-filter-btn" ng-click="resetFilter()">\
	                                <i class="fa fa-times"></i>\
	                            </div>\
	                        </div>\
						</div>',
	        link: function (scope){
	            scope.filterActive = false;
	            scope.resetFilter = function(){
	                scope.filterActive = false;
	                scope.reset();
	            };
	            scope.sendQuery = function(){
	                scope.filterActive = true;
	                var value = scope.filter.label;
	                scope.run({filterType: scope.filter.tipo, filterValue : value});
	            };
	        }
	    };
	};

/***/ }),
/* 20 */
/***/ (function(module, exports) {

	"use strict";
	
	module.exports = function () {
	  return {
	    getDestFilter: function () {
	      return {
	        tipo: "dest",
	        placeholder: "Esempio: Mario Rossi",
	        label: "Destinatario",
	      };
	    },
	    getTrackingFilter: function () {
	      return {
	        tipo: "tracking",
	        placeholder: "Numero di tracking",
	        label: "761",
	      };
	    },
	    getNumeroDiSpedizioneFilter: function () {
	      return {
	        tipo: "numero_spedizione",
	        placeholder: "Numero di spedizione",
	        label: "#",
	      };
	    },
	    getDdtFilter: function () {
	      return {
	        tipo: "ddt",
	        placeholder: "Esempio: 104",
	        label: "DDT",
	      };
	    },
	    getDateOptions: function () {
	      return {
	        locale: {
	          applyClass: "btn-green",
	          applyLabel: "Mostra Spedizioni",
	          fromLabel: "From",
	          format: "DD/MM/YYYY",
	          toLabel: "To",
	          cancelLabel: "Annulla",
	          customRangeLabel: "Inserisci Intervallo",
	        },
	        ranges: {
	          "Ultimi 7 Giorni": [moment().subtract(7, "days"), moment()],
	          "Ultimi 30 Giorni": [moment().subtract(30, "days"), moment()],
	        },
	      };
	    },
	  };
	};
	


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

	var buttonModule = angular.module('ButtonModule', []);
	
	buttonModule.directive('saveButton', __webpack_require__(22));

/***/ }),
/* 22 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(TransitaConstants) {
	    return {
	        restrict: 'E',
	        replace:  true,
	        scope : {
	            formValid : '=',
	            formType : '=',
	            modelName : '@'
	        },
	        template: '<input type="submit" class="btn btn-block btn-success"\
			                value="{{btnLabel}}" />',
	        link: function (scope, element) {
	            var oldLabel;
	            if(scope.formType === TransitaConstants.FORMS.EDIT){
	                scope.btnLabel = 'Salva le modifiche';
	            } else if(scope.formType === TransitaConstants.FORMS.CREATE){
	                scope.btnLabel = 'Inserisci ' + scope.modelName;
	            }
	            oldLabel = scope.btnLabel;
	            scope.$watch('formValid', function(val){
	                if(val === true){
	                    if(scope.formType === TransitaConstants.FORMS.EDIT){
	                        scope.btnLabel = 'Modifica ' + scope.modelName + ' in corso...';
	                    } else if(scope.formType === TransitaConstants.FORMS.CREATE){
	                        scope.btnLabel = 'Inserimento ' + scope.modelName + ' in corso...';
	                    }
	                    $(element).prop('disabled', true);
	                } else if(val === false){
	                    scope.btnLabel = oldLabel;
	                    $(element).prop('disabled', false);
	                }
	            });
	        }
	    };
	};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	var transitaServices = angular.module('TransitaServices', []);
	
	transitaServices.service('SpedizioneService', __webpack_require__(24));

/***/ }),
/* 24 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function($resource){
	    var spedizione = null;
	    var checkState = function(state){
	        return spedizione.stato === state ? true : false;
	    };
	    this.isConsegnata = function(){
	        return checkState('Consegnata');
	    };
	    this.isPrenotata = function(){
	        return checkState('Prenotata');
	    };
	    this.getSpedizione = function(tracking){
	        if(!angular.isUndefined(tracking)){
	            var Spedizione = $resource('../api/spedizione/:tracking');
	            spedizione = Spedizione.get({ tracking : tracking });
	            return spedizione;
	        }
	    };
	    this.getSpedizioni = function() {
	        return $resource('../api/spedizioni/', {
	            dataIniziale: '@startDate',
	            dataFinale: '@endDate',
	            ddt: '@ddt',
	            dest: '@dest',
	            page: '@page'
	        });
	    }
	};

/***/ }),
/* 25 */
/***/ (function(module, exports) {

	var trConstants = angular.module('TransitaConstants', []);
	
	trConstants.constant('TransitaConstants', {
	    'FORMS' : {
	        'EDIT': 'modifica',
	        'CREATE': 'crea'
	    },
	    'API_CLIENTE' : {
	
	            'SPEDIZIONI_CLIENTE' : '../api/spdcliente/'
	    },
	    'API_MAIN' :{
	        'CLIENTI' : '../api/clienti',
	        'CREA_CLIENTE' : 'clienti/create',
	        'ATTIVA_CLIENTE' : 'clienti/attiva',
	        'DISATTIVA_CLIENTE' : 'clienti/disattiva',
	        'SPEDIZIONI' : '../api/spedizioni',
	        'SPEDIZIONE' : '../api/spedizione',
	        'CARICA_SPEDIZIONI' : 'fileSpedizioni/carica',
	        'SALVA_SPEDIZIONI' : 'fileSpedizioni/salva'
	    },
	    'EVENTS' : {
	        'FILE_SPEDIZIONI_CARICATO' : 'uploadFileSpedizioni'
	    }
	});

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(27);
	__webpack_require__(31);
	__webpack_require__(33);
	__webpack_require__(35);
	__webpack_require__(37);

/***/ }),
/* 27 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 32 */,
/* 33 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 34 */,
/* 35 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 36 */,
/* 37 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 38 */,
/* 39 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function($resource) {
	    var cliente;
	    this.setCliente = function(clt){
	        if(!angular.isUndefined(clt)){
	            cliente = clt;
	        }
	    };
	    this.getCliente = function(){
	        return cliente;
	    };
	    this.salvaCliente = function(cliente, successCallback){
	        if(!angular.isUndefined(cliente)){
	            var Cliente = $resource("clienti/create");
	            var cliente = Cliente.save(cliente, function(){
	                successCallback(cliente);
	            });
	        }
	    };
	    this.modificaCliente = function(cliente, successCallback){
	        if(!angular.isUndefined(cliente)){
	            var Cliente = $resource("clienti/edit");
	            var cliente = Cliente.save(cliente, function(){
	                successCallback(cliente);
	            });
	        }
	    };
	    this.reqCliente = function(idcliente){
	        if(!angular.isUndefined(idcliente)){
	            var Cliente = $resource("../api/cliente/:idcliente");
	            cliente = Cliente.get({ idcliente : idcliente });
	            return cliente;
	        }
	    };
	    this.checkUsername = function(username, successCallback, errorCallback){
	        if(!angular.isUndefined(username)){
	            var Cliente = $resource("clienti/checkUsername");
	            var cliente = Cliente.get({username : username}, function(){
	                successCallback();
	            }, function(){
	                errorCallback();
	            });
	            return cliente.$promise;
	        }
	    };
	    this.editUsername = function(username, successCallback){
	        if(!angular.isUndefined(username)){
	            var Cliente = $resource('clienti/editUsername');
	            var cliente = Cliente.save(username, function(){
	                successCallback(username);
	            });
	        }
	    };
	    this.editPassword = function(password, successCallback){
	        if(!angular.isUndefined(password)){
	            var Cliente = $resource('clienti/editPassword');
	            var cliente = Cliente.save(password, function(){
	                successCallback(password);
	            });
	        }
	    };
	    this.addRagSoc = function(ragSoc, successCallback){
	        if(!angular.isUndefined(ragSoc)){
	            var RagSoc = $resource('clienti/addRagSoc');
	            var cliente = RagSoc.save(ragSoc, function(){
	                successCallback(ragSoc);
	            });
	        }
	    };
	    this.removeRagSoc = function(ragSoc, successCallback){
	        if(!angular.isUndefined(ragSoc)){
	            var RagSoc = $resource('clienti/removeRagSoc');
	            var cliente = RagSoc.save(ragSoc, function(){
	                successCallback(ragSoc);
	            });
	        }
	    };
	};

/***/ }),
/* 40 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function($resource) {
	    this.getStati = function(){
	        return $resource('../api/stati', {
	            dd: '@dd'
	        });
	    };
	    this.getStato = function(idstato){
	        var Spedizione = $resource('../api/stato/:idstato');
	        return Spedizione.get({ idstato : idstato });
	    };
	    this.modificaStato = function(stato, successCallback){
	        if(!angular.isUndefined(stato)){
	            var Stato = $resource("stato/update");
	            var stato = Stato.save(stato, function(){
	                successCallback(stato);
	            });
	        }
	    }
	};

/***/ }),
/* 41 */
/***/ (function(module, exports) {

	module.exports = function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider, TransitaConstants){
	    cfpLoadingBarProvider.includeSpinner = false;
	
	    $urlRouterProvider.otherwise('/aggiorna/');
	    
	    var templateUrl = 'partials/render/template/';
	    var widgetUrl = 'partials/renderWidget/name/';
	    $stateProvider
	        .state('aggiorna', {
	            url: '/aggiorna/',
	            templateUrl: templateUrl +  '_aggiorna',
	            controller: 'UploadController'
	        })
	        .state('spedizioni', {
	            url: '/spedizioni/',
	            templateUrl: templateUrl + 'tablespedizioni/_table',
	            controller: 'MainCtrl'
	        })
	        .state('aggiorna.riepilogo', {
	            url: 'riepilogo/',
	            params: {
	                uploadData: null
	            },
	            templateUrl: templateUrl +  'aggiorna/_riepilogo',
	            controller: 'RiepilogoController'
	        })
	        .state('aggiorna.riepilogo.errori', {
	            url: 'errori/',
	            params: {
	                errori: null
	            },
	            templateUrl: templateUrl +  'aggiorna/_errori',
	            controller: 'ErroriController'
	        })
	        .state('spedizioni.spedizione', {
	            url: ':idspedizione/',
	            templateUrl: widgetUrl + 'transita-spedizione.TransitaSpedizione',
	            controller: 'SpedizioneCtrl',
	            resolve:{
	                spedizione: function(SpedizioneService, $stateParams){
	                    return SpedizioneService.getSpedizione($stateParams.idspedizione).$promise;
	                }
	            }
	        })
	        .state('clienti', {
	            url: '/clienti/',
	            templateUrl: templateUrl +  '_tableclienti',
	            controller: 'ClientiCrtl'
	        })
	        .state('clienti.inserisci', {
	            url: 'inserisci/',
	            templateUrl: templateUrl +  '_addcliente',
	            controller: 'ClienteCtrl',
	            resolve:{
	                cliente: function(){
	                    return undefined;
	                },
	                form : function(){
	                    return { tipo : TransitaConstants.FORMS.CREATE }
	                }
	            }
	        })
	        .state('clienti.modifica', {
	            url: 'modifica/:idcliente/',
	            templateUrl: templateUrl +  '_editcliente',
	            controller: 'ClienteCtrl',
	            resolve:{
	                cliente: function(ClienteService, $stateParams){
	                    return ClienteService.reqCliente($stateParams.idcliente).$promise;
	                },
	                form : function(){
	                    return { tipo : TransitaConstants.FORMS.EDIT }
	                }
	            }
	        })
	        .state('clienti.riepilogo', {
	            url: 'riepilogo/:action',
	            templateUrl: templateUrl +  '_revcliente',
	            controller: 'RevClienteCrtl'
	        })
	        .state('stati', {
	            url: '/stati/',
	            templateUrl: templateUrl +  'stato/_table',
	            controller: 'StatiCtrl',
	            resolve: {
	                stati: function(StatoService){
	                    return StatoService.getStati().query();
	                }
	            }
	        })
	        .state('stati.modifica', {
	            url: 'modifica/:idstato',
	            templateUrl: templateUrl +  'stato/_modal',
	            controller: 'StatoCtrl',
	            resolve: {
	                stato: function(StatoService, $stateParams){
	                    return StatoService.getStato($stateParams.idstato).$promise;
	                }
	            }
	        })
	};

/***/ })
/******/ ]);
//# sourceMappingURL=adminbundle.js.map