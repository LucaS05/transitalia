module.exports = function($scope, ClienteService, $state, cliente, form, TransitaConstants) {
    $scope.formType = form.tipo;
    $scope.formValid;
    $scope.editMode = false;
    $scope.editPassword = false;
    $scope.cliente = {};
    $scope.listaRagSoc = [];
    $scope.validationFailed = false;
    var selectedRagSoc;

    if(form.tipo === TransitaConstants.FORMS.EDIT){
        $scope.editMode = true;
    }

    if(!angular.isUndefined(cliente)){
        $scope.cliente.username = cliente.username;
        $scope.listaRagSoc = cliente.rag_soc;
    }

    $scope.clienteSelected = function(selected){
        if(!angular.isUndefined(selected)){
            selectedRagSoc = selected.originalObject.rag_soc;
        }
    };

    var oldPassword;
    $scope.editPwdClick = function(){
        oldPassword = $scope.cliente.password;
        $scope.editPassword = !$scope.editPassword;
    };

    $scope.undoEditPwd = function(){
        $scope.editPassword = !$scope.editPassword;
        $scope.cliente.password = oldPassword;
    };

    $scope.dataClienteChanged = function(){
        var pwdRagSoc, pwdCodCliente;
        if(!angular.isUndefined($scope.listaRagSoc[0])){
            pwdRagSoc = $scope.listaRagSoc[0];
            pwdRagSoc = pwdRagSoc.replace(/[^a-zA-Z]/g, '');
            pwdRagSoc = pwdRagSoc.slice(0,4);
            pwdRagSoc = pwdRagSoc.toUpperCase();
        }
        if(!angular.isUndefined($scope.cliente.codcliente)){
            pwdCodCliente = $scope.cliente.codcliente + '!';
        }
        if(pwdCodCliente && pwdRagSoc){
            $scope.cliente.password = pwdRagSoc + pwdCodCliente;
        } else {
            $scope.cliente.password = undefined;
        }
    };

    $scope.checkUsername = function(){
        var errorCallback = function(){
            $scope.sameUsernameError = true;
        };
        var successCallback = function(){
            $scope.sameUsernameError = false;
        };
        if($scope.cliente.username.length > 3) {
            ClienteService.checkUsername($scope.cliente.username, successCallback,
                errorCallback);
        }
    };

    var userAddedCallback = function(cliente){
        ClienteService.setCliente(cliente);
        var mode = {};
        if($scope.editMode){
            mode.action = 'edit';
        } else {
            mode.action = 'create';
        }
        $state.go('clienti.riepilogo', mode);
    };

    $scope.addCliente = function(clienteForm) {
        if ($scope.listaRagSoc.length === 0 || $scope.sameUsernameError == true) {
            $scope.validationFailed = true;
            clienteForm.$invalid = true;
        } else if(clienteForm.$valid) {
            $scope.validationFailed = false;
            $scope.formValid = true;
            $scope.cliente.listaRagSoc = $scope.listaRagSoc;
            ClienteService.salvaCliente($scope.cliente, userAddedCallback);
        }
    };

    $scope.ediussucc = false;
    $scope.edipwsucc = false;

    $scope.editMessageClose = function(){
        $scope.ediussucc = false;
        $scope.edipwsucc = false;
        $scope.formValid = false;
    };

    var successEditUsername = function(response){
        $scope.ediussucc = true;
        $scope.cliente.username = response.new_username;
        $scope.editmessage = 'Nuovo response: ' + response.new_username;
    };

    var successEditPassword = function(response){
        $scope.edipwsucc = true;
        $scope.cliente.password = response.password;
        $scope.editmessage = 'Nuova password: ' + response.password;
    };

    $scope.sendForm = function(form){
        if(form.$valid){
            $scope.formValid = true;
            switch(form.$name){
                case 'usernameForm':
                    ClienteService.editUsername($scope.cliente, successEditUsername);
                    break;
                case 'passwordForm':
                    ClienteService.editPassword($scope.cliente, successEditPassword);
                    break;
                case 'ragSocForm':
                    var datiCliente = { username : $scope.cliente.username, ragsoc : selectedRagSoc };
                    ClienteService.addRagSoc(datiCliente, successAddRagSoc);
                    break;
                default:
                    console.error('Add form case');
                    break;
            }
        }
    };

    var successAddRagSoc = function(ragioneSociale){
        $scope.listaRagSoc.push(ragioneSociale);
        $scope.$broadcast('angucomplete-alt:clearInput');
        selectedRagSoc = undefined;
    };

    var successDeletedRagSoc = function(index){
        $scope.listaRagSoc.splice(index, 1);
    };

    $scope.removeRagSoc = function(index){
        if($scope.listaRagSoc.length === 1){
            $scope.lastRagSoc = true;
        } else {
            var ragSoc = $scope.listaRagSoc[index];
            if (!angular.isUndefined(ragSoc)) {
                var datiCliente = {ragsoc: ragSoc};
                ClienteService.removeRagSoc(datiCliente, function () {
                    successDeletedRagSoc(index)
                });
            }
        }
    };

    $scope.addRagSoc = function(){
        if(!angular.isUndefined(selectedRagSoc)) {
            if($scope.editMode === true) {
                var datiCliente = {ragsoc: selectedRagSoc, username: $scope.cliente.username};
                ClienteService.addRagSoc(datiCliente, function () {
                    successAddRagSoc(selectedRagSoc)
                });
            } else {
                $scope.listaRagSoc.push(selectedRagSoc);
                $scope.dataClienteChanged();
                $scope.$broadcast('angucomplete-alt:clearInput');
            }
        }
    };
};