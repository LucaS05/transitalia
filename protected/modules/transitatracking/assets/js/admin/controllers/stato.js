module.exports = function($scope, stato, StatoService) {
    $scope.stato = stato;
    var editSuccessCallback = function(){
        $scope.showSuccess = true;
    };
    $scope.editStato = function(statoForm){
        StatoService.modificaStato($scope.stato, editSuccessCallback);
    }
};