module.exports = function($scope, spedizione, SpedizioneService) {
    $scope.isConsegnata = SpedizioneService.isConsegnata();
    $scope.isPrenotata = SpedizioneService.isPrenotata();
    $scope.spedizione = spedizione;
};