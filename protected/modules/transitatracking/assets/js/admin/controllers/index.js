var transitaControllers = angular.module('transitaControllers', []);

transitaControllers.controller('ClientiCrtl', require('./clienti'));
transitaControllers.controller('ClienteCtrl', require('./cliente'));
transitaControllers.controller('RevClienteCrtl', require('./revcliente'));
transitaControllers.controller('SpedizioneCtrl', require('./spedizione'))
transitaControllers.controller('MainCtrl', require('./main'));
transitaControllers.controller('StatiCtrl', require('./stati'));
transitaControllers.controller('StatoCtrl', require('./stato'));
