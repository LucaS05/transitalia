module.exports = function($scope, stati, StatoService) {
    var originalList = stati;
    $scope.statiList = originalList;
    $scope.daDefinireFilter = {tipo : "dd", label : "Da Definire"};
    var getStatiList = function (param) {
        var statiList = StatoService.getStati().query(param, function(){
            $scope.statiList = statiList;
        });
    };
    $scope.runFilter = function(filterType){
        var param = {};
        param[filterType] = 1;
        getStatiList(param);
    };
    $scope.resetStati = function() {
        getStatiList(null);
    }
};