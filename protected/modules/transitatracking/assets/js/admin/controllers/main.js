module.exports = function ($scope, SpedizioneService, $http, $window, $timeout) {
    var startPage = 1;
    $scope.loadComplete = false;
    $scope.spedizioni = {list : [], totalSpd : -1};
    $scope.datePicker = {};
    $scope.datePicker.date = {startDate: null, endDate: null};
    moment.locale("it");
    $scope.datePicker.opts = {
        locale: {
            applyClass: "btn-green",
            applyLabel: "Mostra Spedizioni",
            fromLabel: "From",
            format: "DD/MM/YYYY",
            toLabel: "To",
            cancelLabel: "Annulla",
            customRangeLabel: "Inserisci Intervallo"
        },
        ranges: {
            'Ultimi 7 Giorni': [moment().subtract(7, 'days'), moment()],
            'Ultimi 30 Giorni': [moment().subtract(30, 'days'), moment()]
        }
    };

    $scope.trackingFilter = {
        tipo : "tracking",
        placeholder : "Numero di tracking",
        label : "761"
    };

    $scope.ddtFilter = {
        tipo: "ddt",
        placeholder: "Esempio: 104",
        label: "DDT"
    };

    $scope.destFilter = {
        tipo: "dest",
        placeholder: "Esempio: Mario Rossi",
        label: "Destinatario"
    };

    $scope.resetSpedizioni = function(){
        getRisultatiPagina(startPage);
        $scope.selStartDate = null;
        $scope.selEndDate = null;
        $scope.datePicker.date = {startDate: null, endDate: null};
    };

    $scope.$watch("datePicker.date", function(newDate) {
        if(newDate.startDate && newDate.endDate){
            $scope.selStartDate = newDate.startDate.format("DD/MM/YYYY");
            $scope.selEndDate = newDate.endDate.format("DD/MM/YYYY");
            var dateRange = { dataIniziale :  newDate.startDate.format("YYYY-MM-DD"),
                dataFinale   :  newDate.endDate.format("YYYY-MM-DD")};
            var spedizioni = SpedizioneService.getSpedizioni().query(dateRange, function(){
                $scope.spedizioni.totalSpd = spedizioni[0].count;
                $scope.spedizioni.list = spedizioni[3].spedizioni;
            });
        }
    }, false);

    getRisultatiPagina(startPage);

    $scope.pageChanged = function(pageNumber){
        if(startPage === pageNumber){
            startPage = -1;
        } else {
            getRisultatiPagina(pageNumber);
        }
    };

    function getRisultatiPagina(pageNumber){
        $scope.loadComplete = false;
        $window.scrollTo(0,0);
        var getUrl = "../api/spedizioni?page=" + pageNumber;
        $http.get(getUrl).success(function(data) {
            if (!(data instanceof Array)) {
                $scope.spedizioni.list = [];
            } else {
                $timeout(function(){
                    $scope.loadComplete = true;
                    $scope.spedizioni.list = data[3].spedizioni;
                    $scope.spedizioni.totalSpd = data[0].count;
                    $scope.limit = data[4].limit;
                }, 2000);
            }
        });
    }

    $scope.runFilter = function(filterType, filterValue){
        if(angular.isUndefined(filterValue) === false){
            if(filterValue === ""){
                filterValue = null;
            }
            var param = { page : 1 };
            param[filterType] = filterValue;
            var spedizioni = SpedizioneService.getSpedizioni().query(param, function(){
                $scope.spedizioni.list = spedizioni[3].spedizioni;
                if(spedizioni[0].count === 0){
                    $scope.spedizioni.totalSpd = -1;
                } else {
                    $scope.spedizioni.totalSpd = spedizioni[0].count;
                }
                $scope.loadComplete = true;
            });
        }
    };

    $scope.$on('trfilter:sendQuery', function(){
        $scope.loadComplete = false;
    });
};