module.exports = function($scope, $http) {
    $scope.moduleState = 'list';

    $http.get('../api/clienti').success(function(data) {
        if (data instanceof Array) {
            data.forEach(function(elem) {
                if (elem.stato == '1') {
                    elem.stato = 'attivo';
                } else if (elem.stato == '0') {
                    elem.stato = 'disattivo';
                }
            });
            $scope.clienti = data;
        }
    });

    var setState = function(cliente, state){
        var stateObj = {username : cliente.username, state : state};
        $http.post('clienti/setState', stateObj).
        success(function() {
                if(state === 1){
                    cliente.stato = 'attivo';
                } else if(state == 0){
                    cliente.stato = 'disattivo';
                }
        });
    };

    $scope.enableUser = function(cliente) {
        setState(cliente, 1);
    };

    $scope.disableUser = function(cliente) {
        setState(cliente, 0);
    };
};