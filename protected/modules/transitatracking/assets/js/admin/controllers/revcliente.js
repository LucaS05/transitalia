module.exports = function($scope, ClienteService, $state, $stateParams) {
        $scope.cliente = ClienteService.getCliente();
        if(angular.isUndefined($scope.cliente)){
            $state.go("clienti");
        }
        $scope.action = $stateParams.action;
};