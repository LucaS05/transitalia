module.exports = function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider, TransitaConstants){
    cfpLoadingBarProvider.includeSpinner = false;

    $urlRouterProvider.otherwise('/aggiorna/');
    
    var templateUrl = 'partials/render/template/';
    var widgetUrl = 'partials/renderWidget/name/';
    $stateProvider
        .state('aggiorna', {
            url: '/aggiorna/',
            templateUrl: templateUrl +  '_aggiorna',
            controller: 'UploadController'
        })
        .state('spedizioni', {
            url: '/spedizioni/',
            templateUrl: templateUrl + 'tablespedizioni/_table',
            controller: 'MainCtrl'
        })
        .state('aggiorna.riepilogo', {
            url: 'riepilogo/',
            params: {
                uploadData: null
            },
            templateUrl: templateUrl +  'aggiorna/_riepilogo',
            controller: 'RiepilogoController'
        })
        .state('aggiorna.riepilogo.errori', {
            url: 'errori/',
            params: {
                errori: null
            },
            templateUrl: templateUrl +  'aggiorna/_errori',
            controller: 'ErroriController'
        })
        .state('spedizioni.spedizione', {
            url: ':idspedizione/',
            templateUrl: widgetUrl + 'transita-spedizione.TransitaSpedizione',
            controller: 'SpedizioneCtrl',
            resolve:{
                spedizione: function(SpedizioneService, $stateParams){
                    return SpedizioneService.getSpedizione($stateParams.idspedizione).$promise;
                }
            }
        })
        .state('clienti', {
            url: '/clienti/',
            templateUrl: templateUrl +  '_tableclienti',
            controller: 'ClientiCrtl'
        })
        .state('clienti.inserisci', {
            url: 'inserisci/',
            templateUrl: templateUrl +  '_addcliente',
            controller: 'ClienteCtrl',
            resolve:{
                cliente: function(){
                    return undefined;
                },
                form : function(){
                    return { tipo : TransitaConstants.FORMS.CREATE }
                }
            }
        })
        .state('clienti.modifica', {
            url: 'modifica/:idcliente/',
            templateUrl: templateUrl +  '_editcliente',
            controller: 'ClienteCtrl',
            resolve:{
                cliente: function(ClienteService, $stateParams){
                    return ClienteService.reqCliente($stateParams.idcliente).$promise;
                },
                form : function(){
                    return { tipo : TransitaConstants.FORMS.EDIT }
                }
            }
        })
        .state('clienti.riepilogo', {
            url: 'riepilogo/:action',
            templateUrl: templateUrl +  '_revcliente',
            controller: 'RevClienteCrtl'
        })
        .state('stati', {
            url: '/stati/',
            templateUrl: templateUrl +  'stato/_table',
            controller: 'StatiCtrl',
            resolve: {
                stati: function(StatoService){
                    return StatoService.getStati().query();
                }
            }
        })
        .state('stati.modifica', {
            url: 'modifica/:idstato',
            templateUrl: templateUrl +  'stato/_modal',
            controller: 'StatoCtrl',
            resolve: {
                stato: function(StatoService, $stateParams){
                    return StatoService.getStato($stateParams.idstato).$promise;
                }
            }
        })
};