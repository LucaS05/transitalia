'use strict';

module.exports = function(TransitaConstants) {
    return {
        restrict: 'E',
        replace:  true,
        scope : {
            formValid : '=',
            formType : '=',
            modelName : '@'
        },
        template: '<input type="submit" class="btn btn-block btn-success"\
		                value="{{btnLabel}}" />',
        link: function (scope, element) {
            var oldLabel;
            if(scope.formType === TransitaConstants.FORMS.EDIT){
                scope.btnLabel = 'Salva le modifiche';
            } else if(scope.formType === TransitaConstants.FORMS.CREATE){
                scope.btnLabel = 'Inserisci ' + scope.modelName;
            }
            oldLabel = scope.btnLabel;
            scope.$watch('formValid', function(val){
                if(val === true){
                    if(scope.formType === TransitaConstants.FORMS.EDIT){
                        scope.btnLabel = 'Modifica ' + scope.modelName + ' in corso...';
                    } else if(scope.formType === TransitaConstants.FORMS.CREATE){
                        scope.btnLabel = 'Inserimento ' + scope.modelName + ' in corso...';
                    }
                    $(element).prop('disabled', true);
                } else if(val === false){
                    scope.btnLabel = oldLabel;
                    $(element).prop('disabled', false);
                }
            });
        }
    };
};