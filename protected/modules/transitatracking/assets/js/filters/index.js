var spedizioniFilterModule = angular.module('FilterModule', []);

spedizioniFilterModule.directive('textFilter', require('./textDirective'));
spedizioniFilterModule.directive('buttonFilter', require('./buttonDirective'));
spedizioniFilterModule.service('SpedizioniFilterService', require('./service'));