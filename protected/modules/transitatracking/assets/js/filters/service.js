"use strict";

module.exports = function () {
  return {
    getDestFilter: function () {
      return {
        tipo: "dest",
        placeholder: "Esempio: Mario Rossi",
        label: "Destinatario",
      };
    },
    getTrackingFilter: function () {
      return {
        tipo: "tracking",
        placeholder: "Numero di tracking",
        label: "761",
      };
    },
    getNumeroDiSpedizioneFilter: function () {
      return {
        tipo: "numero_spedizione",
        placeholder: "Numero di spedizione",
        label: "#",
      };
    },
    getDdtFilter: function () {
      return {
        tipo: "ddt",
        placeholder: "Esempio: 104",
        label: "DDT",
      };
    },
    getDateOptions: function () {
      return {
        locale: {
          applyClass: "btn-green",
          applyLabel: "Mostra Spedizioni",
          fromLabel: "From",
          format: "DD/MM/YYYY",
          toLabel: "To",
          cancelLabel: "Annulla",
          customRangeLabel: "Inserisci Intervallo",
        },
        ranges: {
          "Ultimi 7 Giorni": [moment().subtract(7, "days"), moment()],
          "Ultimi 30 Giorni": [moment().subtract(30, "days"), moment()],
        },
      };
    },
  };
};

