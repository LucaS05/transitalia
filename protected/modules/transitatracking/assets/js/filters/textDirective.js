'use strict';

module.exports = function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            filter : '=',
            run : '&',
            reset: '&'
        },
        template:   '<div class="filter spedizioni-filter">\
	    					<label class="filter-label">{{filter.label}}</label>\
	    					<div class="filter-wrap">\
	        					<input type="text" id="filter-text" class="filter-textfield form-control" \
	        					placeholder="{{filter.placeholder}}" ng-keypress="sendQuery($event)">\
	    					</div>\
						    <div class="filter-active" ng-show="filterActive">\
						    	<div class="filter-value-wrap">\
						    		<div class="filter-value">\
                                        <span class="filter-value__label">{{filter.label}}</span>\
                                        <span class="filter-value__value">{{filterVal}}</span>\
                                    </div>\
						    		<div class="close-filter-btn" ng-click="resetFilter()">\
						    			<i class="fa fa-times"></i>\
						    		</div>\
						    	</div>\
						    </div>\
						</div>',
        link: function (scope, element){
            scope.filterActive = false;
            var filterTextField = element.find("input#filter-text");
            scope.resetFilter = function(){
                scope.filterActive = false;
                filterTextField.val("");
                scope.reset();
            };
            scope.sendQuery = function($event){
                if($event.keyCode === 13){
                    scope.filterActive = true;
                    var value = filterTextField.val();
                    scope.filterVal = value;
                    scope.run({filterType: scope.filter.tipo, filterValue : scope.filterVal});
                    scope.$emit('trfilter:sendQuery');
                }
            };
        }
    };
};