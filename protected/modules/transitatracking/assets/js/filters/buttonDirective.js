'use strict';

module.exports = function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            filter : '=',
            run : '&',
            reset: '&'
        },
        template:   '<div class="filter filter_button">\
                        <a class="btn btn-block" \
                            ng-click="sendQuery($event)">{{filter.label}}</a>\
                        <div class="filter-active" ng-show="filterActive">\
                            <div class="filter-value">\
                                <span class="filter-value__label">{{filter.label}}</span>\
                            </div>\
                            <div class="close-filter-btn" ng-click="resetFilter()">\
                                <i class="fa fa-times"></i>\
                            </div>\
                        </div>\
					</div>',
        link: function (scope){
            scope.filterActive = false;
            scope.resetFilter = function(){
                scope.filterActive = false;
                scope.reset();
            };
            scope.sendQuery = function(){
                scope.filterActive = true;
                var value = scope.filter.label;
                scope.run({filterType: scope.filter.tipo, filterValue : value});
            };
        }
    };
};