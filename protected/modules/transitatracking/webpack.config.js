'use strict';
var webpack = require('webpack');

var path = require('path');

var APP = __dirname + '/assets';

var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    debug: true,
    watch: true,
    devtool: 'source-map',
    entry: {
        admin: './assets/js/admin/admin.js',
        cliente: './assets/js/cliente/clienti.js',
        vendor: ['angular', 'angular-ui-router', 'angular-resource', 'angucomplete-alt', 'angular-spinner',
            'angular-utils-pagination', 'angular-loading-bar', 'angular-animate'],
    },
    output: {
        filename: './assets/js/[name]/[name]bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader'),
            },
        ]
    },
    resolve: {
        alias: {
            'spin': 'spin.js'
        }
    },
    plugins: [
        new ExtractTextPlugin('./assets/css/[name].css'),
    ],
};
