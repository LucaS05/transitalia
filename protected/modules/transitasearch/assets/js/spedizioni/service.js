'use strict';

module.exports = function($resource){
    var spedizione = null;
    var checkState = function(state){
        return spedizione.stato === state;
    };
    this.isConsegnata = function(){
        return checkState('Consegnata');
    };
    this.isPrenotata = function(){
        return checkState('Prenotata');
    };
    this.getSpedizione = function(tracking){
        if(!angular.isUndefined(tracking)){
            var Spedizione = $resource('../api/spedizione/:tracking');
            spedizione = Spedizione.get({ tracking : tracking });
            return spedizione;
        }
    };
    this.getSpedizioni = function() {
        return $resource('../api/spedizioni/', {
            dataIniziale: '@startDate',
            dataFinale: '@endDate',
            ddt: '@ddt',
            dest: '@dest',
            page: '@page'
        });
    }
};