var spedizioniFilterModule = angular.module('SpedizioniFilterModule', []);

spedizioniFilterModule.directive('filterSpedizioni', require('./directive'));
spedizioniFilterModule.service('SpedizioniFilterService', require('./service'));