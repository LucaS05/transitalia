'use strict';

module.exports = function(SpedizioneService) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            filter : '=',
            filterList : '=',
            reset: '&'
        },
        template:   '<div class="filter spedizioni-filter">\
	    					<label class="filter-label">{{filter.label}}</label>\
	    					<div class="filter-wrap">\
	        					<input type="text" id="filter-text" class="filter-textfield form-control" placeholder="{{filter.placeholder}}" ng-keypress="sendQuery($event)">\
	    					</div>\
						    <div class="filter-active" ng-show="filterActive">\
						    	<div class="filter-value-wrap">\
						    		<div class="filter-value">\
                                        <span class="filter-value__label">{{filter.label}}</span>\
                                        <span class="filter-value__value">{{filterVal}}</span>\
                                    </div>\
						    		<div class="close-filter-btn" ng-click="resetFilter()">\
						    			<i class="fa fa-times"></i>\
						    		</div>\
						    	</div>\
						    </div>\
						</div>',
        link: function (scope, element){
            scope.filterActive = false;
            var filterTextField = element.find("input#filter-text");
            var runFilter = function(filterValue){
                if(angular.isUndefined(filterValue) === false){
                    if(filterValue === ""){
                        filterValue = null;
                    }
                    var filterType = scope.filter.tipo;
                    var param = { page : 1 };
                    param[filterType] = filterValue;
                    var spedizioni = SpedizioneService.getSpedizioni().query(param, function(){
                        scope.filterList.list = spedizioni[3].spedizioni;
                        if(spedizioni[0].count === 0){
                            scope.filterList.totalSpd = -1;
                        } else {
                            scope.filterList.totalSpd = spedizioni[0].count;
                        }
                    });
                }
            };
            scope.resetFilter = function(){
                scope.filterActive = false;
                filterTextField.val("");
                scope.reset();
            };
            scope.sendQuery = function($event){
                if($event.keyCode === 13){
                    scope.filterActive = true;
                    var filterValue = filterTextField.val();
                    scope.filterVal = filterValue;
                    runFilter(filterValue);
                }
            };
        }
    };
};