var spinnerModule = angular.module('SpinnerModule', []);

spinnerModule.directive('trFullSpinner', require('./fullSpinnerDirective'));
spinnerModule.directive('trSpinner', require('./spinnerDirective'));