'use strict';

// var opts = {
//     lines: 9, // The number of lines to draw
//     length: 4, // The length of each line
//     width: 4, // The line thickness
//     radius: 6, // The radius of the inner circle
//     corners: 0.8, // Corner roundness (0..1)
//     rotate: 30, // The rotation offset
//     direction: 1, // 1: clockwise, -1: counterclockwise
//     color: '#fff', // #rgb or #rrggbb or array of colors
//     speed: 1.5, // Rounds per second
//     trail: 47, // Afterglow percentage
//     shadow: false, // Whether to render a shadow
//     hwaccel: false, // Whether to use hardware acceleration
//     className: 'spinner', // The CSS class to assign to the spinner
//     zIndex: 2e9, // The z-index (defaults to 2000000000)
//     top: '50%', // Top position relative to parent
//     left: '50%' // Left position relative to parent
// };
// var spinner = new Spinner(opts);

module.exports = function($timeout) {
    return {
        restrict: 'E',
        replace:  true,
        scope : {
            color : '=',
        },
        template: '<div class="loader spdloader">\
                        <div class="loader__msg">\
                            <span us-spinner="{radius:6, width:4, length: 4, color: \'#fff\', top: \'20%\'}"></span>\
                        </div>\
                   </div>',
        link: function (scope, element) {
            scope.$on('spinner:hide', function(){
                $timeout(function(){
                    element.addClass('hide');
                }, 3000);
            });
        }
    };
};