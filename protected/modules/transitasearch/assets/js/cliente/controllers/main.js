module.exports = function ($scope, $rootScope){
    $rootScope.$broadcast('fullspinner:hide');
    $scope.spedizione = window.__spedizione__;
    $scope.isConsegnata = $scope.spedizione.stato === 'Consegnata';
};