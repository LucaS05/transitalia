webpackJsonp([0],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(4);
	
	__webpack_require__(7);
	
	__webpack_require__(9);
	
	__webpack_require__(12);
	
	__webpack_require__(13);
	
	__webpack_require__(24);
	
	var trackingApp = angular.module('TransitaTracking', [
	    'ui.router',
	    'ngResource',
	    'ngAnimate',
	    'TransitaServices',
	    'SpedizioniFilterModule',
	    'ClienteControllers',
	    'angularSpinner',
	    'angular-loading-bar',
	    'SpinnerModule']
	);
	
	trackingApp.config(['$stateProvider', '$urlRouterProvider','cfpLoadingBarProvider',
	    function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {
	        var widgetUrl = '/transitasearch/partials/renderWidget/name/';
	        cfpLoadingBarProvider.includeSpinner = false;
			$urlRouterProvider.otherwise('/');
			$stateProvider
	            .state('spedizioni', {
	                cache: false,
	                url: '/',
	                templateUrl: widgetUrl + 'transita-spedizione.TransitaSpedizione',
	                controller: 'MainCtrl'
	            })
	    }
	]);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	var clienteControllers = angular.module('ClienteControllers', []);
	
	clienteControllers.controller('MainCtrl', __webpack_require__(5));
	clienteControllers.controller('SpedizioneCtrl', __webpack_require__(6));

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = function ($scope, $rootScope){
	    $rootScope.$broadcast('fullspinner:hide');
	    $scope.spedizione = window.__spedizione__;
	    $scope.isConsegnata = $scope.spedizione.stato === 'Consegnata';
	};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	module.exports = function ($scope, spedizione, SpedizioneService){
	    $scope.isConsegnata = SpedizioneService.isConsegnata();
	    $scope.spedizione = spedizione;
	};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	var transitaServices = angular.module('TransitaServices', []);
	
	transitaServices.service('SpedizioneService', __webpack_require__(8));

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function($resource){
	    var spedizione = null;
	    var checkState = function(state){
	        return spedizione.stato === state;
	    };
	    this.isConsegnata = function(){
	        return checkState('Consegnata');
	    };
	    this.isPrenotata = function(){
	        return checkState('Prenotata');
	    };
	    this.getSpedizione = function(tracking){
	        if(!angular.isUndefined(tracking)){
	            var Spedizione = $resource('../api/spedizione/:tracking');
	            spedizione = Spedizione.get({ tracking : tracking });
	            return spedizione;
	        }
	    };
	    this.getSpedizioni = function() {
	        return $resource('../api/spedizioni/', {
	            dataIniziale: '@startDate',
	            dataFinale: '@endDate',
	            ddt: '@ddt',
	            dest: '@dest',
	            page: '@page'
	        });
	    }
	};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	var spedizioniFilterModule = angular.module('SpedizioniFilterModule', []);
	
	spedizioniFilterModule.directive('filterSpedizioni', __webpack_require__(10));
	spedizioniFilterModule.service('SpedizioniFilterService', __webpack_require__(11));

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function(SpedizioneService) {
	    return {
	        restrict: 'E',
	        replace: true,
	        scope: {
	            filter : '=',
	            filterList : '=',
	            reset: '&'
	        },
	        template:   '<div class="filter spedizioni-filter">\
		    					<label class="filter-label">{{filter.label}}</label>\
		    					<div class="filter-wrap">\
		        					<input type="text" id="filter-text" class="filter-textfield form-control" placeholder="{{filter.placeholder}}" ng-keypress="sendQuery($event)">\
		    					</div>\
							    <div class="filter-active" ng-show="filterActive">\
							    	<div class="filter-value-wrap">\
							    		<div class="filter-value">\
	                                        <span class="filter-value__label">{{filter.label}}</span>\
	                                        <span class="filter-value__value">{{filterVal}}</span>\
	                                    </div>\
							    		<div class="close-filter-btn" ng-click="resetFilter()">\
							    			<i class="fa fa-times"></i>\
							    		</div>\
							    	</div>\
							    </div>\
							</div>',
	        link: function (scope, element){
	            scope.filterActive = false;
	            var filterTextField = element.find("input#filter-text");
	            var runFilter = function(filterValue){
	                if(angular.isUndefined(filterValue) === false){
	                    if(filterValue === ""){
	                        filterValue = null;
	                    }
	                    var filterType = scope.filter.tipo;
	                    var param = { page : 1 };
	                    param[filterType] = filterValue;
	                    var spedizioni = SpedizioneService.getSpedizioni().query(param, function(){
	                        scope.filterList.list = spedizioni[3].spedizioni;
	                        if(spedizioni[0].count === 0){
	                            scope.filterList.totalSpd = -1;
	                        } else {
	                            scope.filterList.totalSpd = spedizioni[0].count;
	                        }
	                    });
	                }
	            };
	            scope.resetFilter = function(){
	                scope.filterActive = false;
	                filterTextField.val("");
	                scope.reset();
	            };
	            scope.sendQuery = function($event){
	                if($event.keyCode === 13){
	                    scope.filterActive = true;
	                    var filterValue = filterTextField.val();
	                    scope.filterVal = filterValue;
	                    runFilter(filterValue);
	                }
	            };
	        }
	    };
	};

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	'use strict';
	
	module.exports = function($resource) {
	    return {
	        getDestFilter: function () {
	            return {
	                tipo: 'dest',
	                placeholder: 'Esempio: Mario Rossi',
	                label: 'Destinatario'
	            };
	        },
	        getTrackingFilter: function () {
	            return {
	                tipo: 'tracking',
	                placeholder: 'Numero di tracking',
	                label: '761'
	            };
	        },
	        getDdtFilter: function () {
	            return {
	                tipo: 'ddt',
	                placeholder: 'Esempio: 104',
	                label: 'DDT'
	            };
	        },
	        getDateOptions: function () {
	            return {
	                locale: {
	                    applyClass: 'btn-green',
	                    applyLabel: 'Mostra Spedizioni',
	                    fromLabel: 'From',
	                    format: 'DD/MM/YYYY',
	                    toLabel: 'To',
	                    cancelLabel: 'Annulla',
	                    customRangeLabel: 'Inserisci Intervallo'
	                },
	                ranges: {
	                    'Ultimi 7 Giorni': [moment().subtract(7, 'days'), moment()],
	                    'Ultimi 30 Giorni': [moment().subtract(30, 'days'), moment()]
	                }
	            };
	        }
	
	    }
	};

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	var trConstants = angular.module('TransitaConstants', []);
	
	trConstants.constant('TransitaConstants', {
	    'FORMS' : {
	        'EDIT': 'modifica',
	        'CREATE': 'crea'
	    },
	    'API_CLIENTE' : {
	
	            'SPEDIZIONI_CLIENTE' : '../api/spdcliente/'
	    },
	    'API_MAIN' :{
	        'CLIENTI' : '../api/clienti',
	        'CREA_CLIENTE' : 'clienti/create',
	        'ATTIVA_CLIENTE' : 'clienti/attiva',
	        'DISATTIVA_CLIENTE' : 'clienti/disattiva',
	        'SPEDIZIONI' : '../api/spedizioni',
	        'SPEDIZIONE' : '../api/spedizione',
	        'CARICA_SPEDIZIONI' : 'fileSpedizioni/carica',
	        'SALVA_SPEDIZIONI' : 'fileSpedizioni/salva'
	    },
	    'EVENTS' : {
	        'FILE_SPEDIZIONI_CARICATO' : 'uploadFileSpedizioni'
	    }
	});

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(14);
	__webpack_require__(18);
	__webpack_require__(20);
	__webpack_require__(22);

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 19 */,
/* 20 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 21 */,
/* 22 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }),
/* 23 */,
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

	var spinnerModule = angular.module('SpinnerModule', []);
	
	spinnerModule.directive('trFullSpinner', __webpack_require__(25));
	spinnerModule.directive('trSpinner', __webpack_require__(26));

/***/ }),
/* 25 */
/***/ (function(module, exports) {

	'use strict';
	
	// var opts = {
	//     lines: 9, // The number of lines to draw
	//     length: 4, // The length of each line
	//     width: 4, // The line thickness
	//     radius: 6, // The radius of the inner circle
	//     corners: 0.8, // Corner roundness (0..1)
	//     rotate: 30, // The rotation offset
	//     direction: 1, // 1: clockwise, -1: counterclockwise
	//     color: '#fff', // #rgb or #rrggbb or array of colors
	//     speed: 1.5, // Rounds per second
	//     trail: 47, // Afterglow percentage
	//     shadow: false, // Whether to render a shadow
	//     hwaccel: false, // Whether to use hardware acceleration
	//     className: 'spinner', // The CSS class to assign to the spinner
	//     zIndex: 2e9, // The z-index (defaults to 2000000000)
	//     top: '50%', // Top position relative to parent
	//     left: '50%' // Left position relative to parent
	// };
	// var spinner = new Spinner(opts);
	
	module.exports = function($timeout) {
	    return {
	        restrict: 'E',
	        replace:  true,
	        scope : {
	            color : '=',
	        },
	        template: '<div class="loader spdloader">\
	                        <div class="loader__msg">\
	                            <span us-spinner="{radius:6, width:4, length: 4, color: \'#fff\', top: \'20%\'}"></span>\
	                            Caricamento spedizione in corso....\
	                        </div>\
	                   </div>',
	        link: function (scope, element) {
	            scope.$on('fullspinner:hide', function(){
	                console.log('hide');
	                $(element).hide();
	            });
	            scope.$on('fullspinner:show', function(){
	                console.log('show');
	                element.removeClass('hide');
	                $timeout(function(){
	                    element.addClass('show');
	                });
	            });
	        }
	    };
	};

/***/ }),
/* 26 */
/***/ (function(module, exports) {

	'use strict';
	
	// var opts = {
	//     lines: 9, // The number of lines to draw
	//     length: 4, // The length of each line
	//     width: 4, // The line thickness
	//     radius: 6, // The radius of the inner circle
	//     corners: 0.8, // Corner roundness (0..1)
	//     rotate: 30, // The rotation offset
	//     direction: 1, // 1: clockwise, -1: counterclockwise
	//     color: '#fff', // #rgb or #rrggbb or array of colors
	//     speed: 1.5, // Rounds per second
	//     trail: 47, // Afterglow percentage
	//     shadow: false, // Whether to render a shadow
	//     hwaccel: false, // Whether to use hardware acceleration
	//     className: 'spinner', // The CSS class to assign to the spinner
	//     zIndex: 2e9, // The z-index (defaults to 2000000000)
	//     top: '50%', // Top position relative to parent
	//     left: '50%' // Left position relative to parent
	// };
	// var spinner = new Spinner(opts);
	
	module.exports = function($timeout) {
	    return {
	        restrict: 'E',
	        replace:  true,
	        scope : {
	            color : '=',
	        },
	        template: '<div class="loader spdloader">\
	                        <div class="loader__msg">\
	                            <span us-spinner="{radius:6, width:4, length: 4, color: \'#fff\', top: \'20%\'}"></span>\
	                        </div>\
	                   </div>',
	        link: function (scope, element) {
	            scope.$on('spinner:hide', function(){
	                $timeout(function(){
	                    element.addClass('hide');
	                }, 3000);
	            });
	        }
	    };
	};

/***/ })
]);
//# sourceMappingURL=clientebundle.js.map