'use strict';

require('./controllers');

require('../spedizioni');

require('../spedizioni/filters');

require('../trconstants');

require('../less');

require('../spinner');

var trackingApp = angular.module('TransitaTracking', [
    'ui.router',
    'ngResource',
    'ngAnimate',
    'TransitaServices',
    'SpedizioniFilterModule',
    'ClienteControllers',
    'angularSpinner',
    'angular-loading-bar',
    'SpinnerModule']
);

trackingApp.config(['$stateProvider', '$urlRouterProvider','cfpLoadingBarProvider',
    function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {
        var widgetUrl = '/transitasearch/partials/renderWidget/name/';
        cfpLoadingBarProvider.includeSpinner = false;
		$urlRouterProvider.otherwise('/');
		$stateProvider
            .state('spedizioni', {
                cache: false,
                url: '/',
                templateUrl: widgetUrl + 'transita-spedizione.TransitaSpedizione',
                controller: 'MainCtrl'
            })
    }
]);