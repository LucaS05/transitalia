<div class="container-fluid" ui-view>
    <div class="row">
        <div ng-if="spedizioni.totalSpd == -1" class="spedizionestate spedstate_notfound">
            <h1 class="spedizionestate__message">Nessuna spedizione!</h1>
            <h3 class="spedizionestate__message">Non abbiamo tue spedizioni da mostrare, riprova più tardi</h3>
            <div class="truckicon">
                <i class="fa fa-times truckicon_cross"></i>
                <i class="fa fa-truck truckicon_truck"></i>
            </div>
        </div>
        <div ng-if="spedizioni.totalSpd > 0">
            <div class="tr-listspedizioni container">
                <div class="row">
                    <div ui-sref=".spedizione({idspedizione: spedizione.id})"
                        class="spedizione col-md-8 col-md-offset-2">
                            <div class="spedizione__section spedizione__section_top">
                                <div class="row">
                                    <div class="col-md-6 spedizione-item spedizione-item_date">
                                        <i class="fa fa-calendar-o spedizione-icon_top"></i>
                                        <span class="spedizione-item__data">{{spedizione.data}}</span>
                                    </div>
                                    <div class="col-md-6 spedizione-item">
                                        <div class="stato-spedizione stato-spedizione_list">
                                            <div class="stato-spedizione__label">Stato</div>
                                            <div class="stato-spedizione__stato">{{spedizione.stato}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="spedizione__section spedizione__section_dest">
                                <div class="row">
                                    <div class="spedizione-item col-md-12">
                                        <div class="spedizione__item spedizione-item_dest">
                                            {{spedizione.destinatario}}</div>
                                    </div>
                                    <div class="spedizione-item spedizione-item_place col-md-12">
                                        <i class="fa fa-map-marker spedizione-icon__marker"></i>
                                        <div class="spedizione-item__data">{{spedizione.luogo_arrivo}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="spedizione__section">
                                <div class="row">
                                    <div class="spedizione-item spedizione-item_colli col-md-3">
                                        <i class="fa fa-archive spedizione-icon__marker"></i>
                                        <div class="spedizione-item__data">{{spedizione.colli}} Colli</div>
                                    </div>
                                    <div class="spedizione-item col-md-3">
                                        <div class="spedizione-icon">
                                            <i class="fa fa-file-text-o spedizione-icon__marker"></i>
                                            <span class="spedizione-icon spedizione-icon__label">DDT</span>
                                        </div>
                                        <div class="spedizione-item__data">{{spedizione.ddt}}</div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="calendario">
        <?php
        $day = date('w');
        $month = date('n');

        $days = Yii::app()->locale->getWeekDayNames("abbreviated");
        $months = Yii::app()->locale->getMonthNames("abbreviated");
        ?>
        <p class="calendario__title">Oggi</p>
        <div class="calendario__data"><?php echo $days[$day] ?></div>
        <div class="calendario__giorno"><?php echo $nDay = date('j'); ?></div>
        <div class="calendariodata"><?php echo ucfirst($months[$month]); ?></div>
    </div>
</div>