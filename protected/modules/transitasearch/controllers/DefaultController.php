<?php

class DefaultController extends Controller
{
    public $layout='/layouts/main';

	public function actionIndex()
	{
        $this->redirect([
            '/transitasearch/spedizione/' . Yii::app()->session['numeroSpedizione']
        ]);
	}
}