<?php

class SpedizioneController extends Controller
{
    use GreetingTrait;

    public $layout='/layouts/main';

    public function actionIndex($id)
    {
        $modelSpedizione = Spedizioni::model()->findByAttributes(array('num_spedizione' => $id));

        if (empty($modelSpedizione)) {
            $this->redirect('/');
            return;
        }

        $this->render('index',
            array(
                'greetingMessage' => $this->getGreeting(),
                'spedizione' => $this
                    ->widget('application.components.representer.RepresenterSpedizione')
                    ->render($modelSpedizione)
            )
        );
    }
}