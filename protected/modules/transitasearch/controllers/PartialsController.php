<?php

class PartialsController extends Controller
{
    public function actionRender()
    {
        $pos = strpos(Yii::app()->request->pathInfo, 'template');
        if ($pos === false) {
            throw new CException('Parametro template assente.');
        }
        $path = substr(Yii::app()->request->pathInfo, $pos + strlen('template') + 1);
        $this->render($path);
    }

    public function actionRenderWidget()
    {
        $widget = 'application.components.';
        $pos = strpos(Yii::app()->request->pathInfo, 'name');
        $path = substr(Yii::app()->request->pathInfo, $pos + strlen('name') + 1);
        return $this->widget($widget . $path);
    }
}