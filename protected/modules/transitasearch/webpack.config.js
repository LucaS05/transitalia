'use strict';

var webpack = require('webpack');

var path = require('path');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

var APP = __dirname + '/assets';

module.exports = {
    debug: true,
    watch: true,
    devtool: 'source-map',
    entry: {
        cliente: './assets/js/cliente/clienti.js',
        vendor: ['angular', 'angular-ui-router', 'angular-resource', 'angucomplete-alt', 'angular-spinner',
            'angular-loading-bar', 'angular-animate'],
    },
    output: {
        filename: './assets/js/[name]/[name]bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader'),
            },
        ]
    },
    resolve: {
        alias: {
            'spin': 'spin.js'
        }
    },
    plugins: [
        new ExtractTextPlugin('./assets/css/transitasearch.css'),
        new webpack.optimize.CommonsChunkPlugin('vendor', './assets/js/vendor.bundle.js')
    ],
};
