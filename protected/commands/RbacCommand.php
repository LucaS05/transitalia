<?php

class RbacCommand extends CConsoleCommand
{
    private $_authManager;

	public function getHelp()
	{
		$description = 'DESCRIPTION\n';
		$description .= '    '.'This command generates an initial RBAC authorization hierarchy.\n';
		return parent::getHelp() . $description;
	}

	/**
	 * The default action - create the RBAC structure.
	 */
	public function actionIndex()
	{
        $this->ensureAuthManagerDefined();

		$message = "Il comando si occupa di creare:\n";
        $message .= "Ruoli:\n";
        $message .= "Admin\n";
        $message .= "Cliente\n\n";
		$message .= "Permessi:\n";
		$message .= "create, read, update and delete user\n";
		$message .= "create, read, update spedizione\n";
		$message .= "Continuare?";

		if ($this->confirm($message)) {

			$this->_authManager->clearAll();

			$this->_authManager->createOperation('createUser', 'crea un nuovo utente');
			$this->_authManager->createOperation('readUser', 'legge le informazioni di profilo dell\'utente');
			$this->_authManager->createOperation('updateUser', 'modifica le informazioni di profilo dell\'utente');
			$this->_authManager->createOperation('deleteUser', 'rimuove un utente');
            $this->_authManager->createOperation('createSpedizione', 'crea una spedizione');
			$this->_authManager->createOperation('readSpedizione', 'legge le informazioni di una spedizione');
            $this->_authManager->createOperation('updateSpedizione', 'modifica le informazioni di una spedizione');

			$role = $this->_authManager->createRole('cliente');
            $role->addChild('readSpedizione');

            $role = $this->_authManager->createRole('admin');
			$role->addChild('createUser');
			$role->addChild('readUser');
			$role->addChild('updateUser');
			$role->addChild('deleteUser');
			$role->addChild('readSpedizione');
			$role->addChild('updateSpedizione');

			echo "RBAC generata correttamente.\n";
		} else {
			echo "Operazione annullata.\n";
		}
    }

	public function actionDelete()
	{
		$this->ensureAuthManagerDefined();
		$message = "Questo comando eliminerà i permessi definiti nella RBAC.\n";
		$message .= 'Continuare?';
		if ($this->confirm($message)) {
			$this->_authManager->clearAll();
			echo "RBAC rimossa.\n";
		} else {
            echo "Operazione annullata.\n";
        }
	}
	
	protected function ensureAuthManagerDefined()
	{
		if (($this->_authManager = Yii::app()->authManager) === null) {
			$message = 'Error: an authorization manager, named \'authManager\' must be 
			    con-figured to use this command.';
			$this->usageError($message);
		}
	}
}