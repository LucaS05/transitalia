<?php

class ArchiveCommand extends CConsoleCommand
{
    public function getHelp()
    {
        $description = 'DESCRIPTION' . PHP_EOL;
        $description .= '    '.'Archivia spedizioni più vecchie di 4 mesi';
        return parent::getHelp() . $description;
    }

    public function actionIndex()
    {
        $cdbConnection = Yii::app()->db;
        $transaction = $cdbConnection->beginTransaction();

        try {
            $archiviate = $cdbConnection->createCommand(
                'INSERT INTO tbl_archivio
                            (num_spedizione,
                             ddt,
                             data,
                             luogo_arrivo,
                             colli,
                             data_consegna,
                             firmato_da,
                             stato,
                             destinatario,
                             mittente,
                             tracking)
                SELECT num_spedizione,
                       ddt,
                       data,
                       luogo_arrivo,
                       colli,
                       data_consegna,
                       firmato_da,
                       stato,
                       destinatario,
                       mittente,
                       tracking
                FROM   tbl_spedizioni
                WHERE  tbl_spedizioni.data <= Now() - interval 4 month; ')->execute();
            $cdbConnection->createCommand('
                DELETE FROM tbl_spedizioni 
                WHERE tbl_spedizioni.data <= NOW() - INTERVAL 4 MONTH;')->execute();
            $transaction->commit();
            Yii::log('Spedizioni archiviate: ' . $archiviate, CLogger::LEVEL_WARNING);
        } catch(Exception $e) {
            $transaction->rollback();
        }
    }
}