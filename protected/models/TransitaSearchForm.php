<?php

class TransitaSearchForm extends CFormModel
{
    public $numeroSpedizione;

	public function rules()
	{
		return array(
			array('numeroSpedizione', 'validateNumeroSpedizione'),
		);
	}

	public function validateNumeroSpedizione($attribute)
	{
		if (! $this->hasErrors()) {
            $modelSpedizione = Spedizioni::model()
                ->find('num_spedizione=:num', array('num' => $this->numeroSpedizione));

            if ($modelSpedizione === null) {
                $this->addError($attribute, 'Numero di errato. Non hai una spedizione con questo numero.');
            }

            return true;
		}
	}

    public function attributeLabels(){
        return array(
            "numeroSpedizione" => "Numero di Spedizione"
        );
    }
}
