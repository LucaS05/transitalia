<?php

class Cliente extends CModel{

    private function createUser($data)
    {
        if (isset($data)) {
            $userModel = new User();
            if (isset($data["username"])) {
                $userModel->username = $data["username"];
            }
            if (isset($data["password"])) {
                $userModel->password = $data["password"];
            }
            return $userModel;
        }
    }

    public function save($clienteData)
    {
        if (isset($clienteData)) {
            $arrayErrors = array();
            $modelMittente = new Mittente();
            $modelCliente = $this->createUser($clienteData);
            $transaction = Yii::app()->db->beginTransaction();
            try{
                if (!$modelCliente->save()) {
                    throw new Exception();
                }
                $ragSocList = $clienteData["listaRagSoc"];
                if (is_array($ragSocList)) {
                    foreach($ragSocList as $ragSoc) {
                        $modelMittente = Mittente::getFreeMittente($ragSoc);
                        if (isset($modelMittente)) {
                            $modelMittente->user = $modelCliente->id;
                            if (!$modelMittente->save()) {
                                throw new Exception();
                            }
                        } else {
                            array_push($arrayErrors, array("mittente" => "Ragione Sociale: " . $ragSoc .
                                " non disponibilie. È stata già utilizzata per un altro utente!"));
                            throw new Exception();
                        }
                    }
                }
                $transaction->commit();
                Yii::app()->authManager->assign("cliente", $modelCliente->id);
            } catch(Exception $e) {
                $transaction->rollback();
                if (isset($modelCliente) && isset($modelMittente)) {
                    $arrayErrors = array_merge($modelCliente->getErrors(), $modelMittente->getErrors());
                }
                return $arrayErrors;
            }
        }
    }

    public function setState($username, $state)
    {
        $arrayErrors = array();
        if (isset($username) && isset($state)) {
            $modelUser = User::getByUsername($username);
            if (isset($modelUser)) {
                $modelUser->stato = $state;
                $modelUser->update();
                return true;
            }
        } else {
            if(is_null($username)){
                array_push($arrayErrors, array("username" => "Nessun username specificato!"));
            }
            if(is_null($state)){
                array_push($arrayErrors, array("state" => "Stato non specificato!"));
            }
        }
    }

    public function editClienteAttribute($username, $attribute, $newValue)
    {
        $exUser = User::model()->find("username=:username", array("username" => $username));
        $res = false;
        if (isset($exUser)) {
            $exUser[$attribute] = $newValue;
            if ($exUser->validate()) {
                $exUser->update();
                $res = true;
            } else {
                $res = $exUser->getErrors();
            }
        }
        return $res;
    }

    public function addRagSoc($username, $ragSoc)
    {
        if (isset($username) && isset($ragSoc)) {
            $modelUtente = User::getByUsername($username);
            $modelMittente = Mittente::getFreeMittente($ragSoc);
            $modelMittente->user = $modelUtente->id;
            if ($modelMittente->validate()) {
                $modelMittente->update();
                return true;
            } else {
                return $modelMittente->getErrors();
            }
        }
    }

    public function removeRagSoc($ragSoc)
    {
        if (isset($ragSoc)) {
            $arrayErrors = array();
            $modelMittente = Mittente::getByRagSoc($ragSoc);
            if (isset($modelMittente)) {
                $modelMittente->user = null;
                if ($modelMittente->validate()) {
                    $modelMittente->update();
                    return true;
                } else {
                    return $modelMittente->getErrors();
                }
            } else {
                array_push($arrayErrors, array("rag_soc" => "Nessun mittente trovato con
                    ragione sociale: " . $ragSoc));
                return $arrayErrors;
            }
        }
        array_push($arrayErrors, array("rag_soc" => "Ragione sociale mancante"));
        return $arrayErrors;
    }

    public function existUsername($username)
    {
        if (isset($username)) {
            $modelUser = User::model()->find("username=:username", array(":username" => $username));
            if (isset($modelUser)) {
                return $modelUser;
            } else {
                return false;
            }
        }
    }

    /**
     * Returns the list of attribute names of the model.
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        // TODO: Implement attributeNames() method.
    }
}