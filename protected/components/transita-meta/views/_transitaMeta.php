<?php
    $themePath = Yii::app()->theme->baseUrl;
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Transitalia Express s.r.l. | Spedizioni Nazionali e Internazionali</title>

<meta name="description" content="Transitalia Express - 30 anni di esperienza in spedizioni nazionali e internazionali.">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo $themePath . "/assets/images/favicon.ico" ?>">

<!-- Open Graph data -->
<meta property="og:title" content="Transitalia Express s.r.l." />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.transitaliaexpress.it/" />
<meta property="og:image" content="http://example.com/image.jpg" />
<meta property="og:description" content="Transitalia Express - 30 anni di esperienza in spedizioni nazionali e internazionali." />
