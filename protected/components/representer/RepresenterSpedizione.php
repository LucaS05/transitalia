<?php

class RepresenterSpedizione extends CWidget
{
    public function render($view,$data=null,$return=false)
    {
        $modelSpedizione = $view;

        $modelSpedizione->data = DateTime::createFromFormat('Y-m-d', $modelSpedizione->data)
            ->format('d/m/Y');

        if(isset($modelSpedizione->data_consegna)){
            $modelSpedizione->data_consegna = DateTime::createFromFormat('Y-m-d', $modelSpedizione->data_consegna)
                ->format('d/m/Y');
        }

        $modelSpedizione->stato = $modelSpedizione->statoSp->nome;

        $spedizioneAttributes = $modelSpedizione->getAttributes();

        if(Yii::app()->user->checkAccess("cliente")){
            unset($spedizioneAttributes["tracking"]);
        }

        foreach ($modelSpedizione->cronologiaSp as $cronItem) {
            $cronItem->idstato = $cronItem->statoCrn->nome;
            $dataCrn = DateTime::createFromFormat('Y-m-d', $cronItem->data);
            $cronItem->data = $dataCrn->format('d/m/Y');
        }
        $spedizioneAttributes["cronologia"] = $modelSpedizione->cronologiaSp;

        return CJSON::encode($spedizioneAttributes);
    }
}