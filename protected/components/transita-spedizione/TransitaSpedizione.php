<?php

class TransitaSpedizione extends CWidget
{
    public function run() {
        if (Yii::app()->user->checkAccess('admin')) {
            $this->render('spedizione_admin');
        } else {
            $this->render('spedizione');
        }
    }
}