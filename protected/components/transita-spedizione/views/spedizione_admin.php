<div id="spedizione">
    <div class="row">
        <div class="toolbar col-md-12">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="toolbar-title col-md-8">
                        Spedizione nr. {{spedizione.num_spedizione}}
                    </h1>
                    <div class="col-md-4">
                        <div class="stato-spedizione">
                            <div class="stato-spedizione__label">Stato</div>
                            <div ng-if="isConsegnata" class="stato-spedizione__consegnata">{{spedizione.stato}}</div>
                            <div ng-if="!isConsegnata" class="stato-spedizione__stato">{{spedizione.stato}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">    
            <div id="spedizione-data" class="col-lg-9 col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <p class="tipo">Tracking</p>
                        <p class="data">{{spedizione.tracking}}</p>
                    </div>
                    <div class="col-md-4">
                        <p class="tipo">Numero Spedizione</p>
                        <p class="data">{{spedizione.num_spedizione}}</p>
                    </div>
                    <div class="col-md-4">
                        <p class="tipo">DDT</p>
                        <p class="data">{{spedizione.ddt}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <p class="tipo">Luogo Arrivo</p>
                        <p class="data">{{spedizione.luogo_arrivo}}</p>
                    </div>
                    <div class="col-md-4">
                        <p class="tipo">Destinatario</p>
                        <p class="data">{{spedizione.destinatario}}</p>
                    </div>
                    <div ng-if="isConsegnata" class="col-md-4">
                        <p class="tipo">Firmato Da</p>
                        <p class="data">{{spedizione.firmato_da}}</p>
                    </div>
                    <div ng-if="isPrenotata" class="col-md-4">
                        <p class="tipo">Prenotata Da</p>
                        <p class="data">{{spedizione.firmato_da}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <p class="tipo">Data Spedizione</p>
                        <p class="data">{{spedizione.data}}</p>
                    </div>
                    <div ng-if="isConsegnata" class="col-md-4">
                        <p class="tipo">Data Consegna</p>
                        <p class="data">{{spedizione.data_consegna}}</p>
                    </div>
                    <div class="col-md-4">
                        <p class="tipo">Colli</p>
                        <p class="data">{{spedizione.colli}}</p>
                    </div>
                </div>
            </div>
            <div ng-if="spedizione.cronologia.length !== 0">
                <div class="tracking col-lg-3 col-md-12">
                    <p class="tracking__titolo">Tracking Spedizione</p>
                    <table class="table tracking-table">
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Stato</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="crn in spedizione.cronologia">
                                <td data-th="Data">{{crn.data}}</td>
                                <td data-th="Stato">{{crn.idstato}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    body{
        background-color: #e9edf5;
    }
</style>