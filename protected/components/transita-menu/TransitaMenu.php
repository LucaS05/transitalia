<?php

class TransitaMenu extends CWidget
{
    public $isHome;

    public function run() {
        if ($this->isHome) {
            $this->render('_menuHome');
        } else {
            $this->render('_menu');
        }
    }
}