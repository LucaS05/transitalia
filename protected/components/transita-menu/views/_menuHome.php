<nav class="main-menu" role="navigation">
    <div class="menu-btn-home">Menu</div>
    <ul class="menu">
        <li>
            <?php echo CHtml::link(Yii::t("strings", "menu.azienda"), array('/site/page/view/azienda',
                'lang' => Yii::app()->language)) ?></li>
        <li>
            <?php echo CHtml::link(Yii::t("strings", "menu.servizi"), array('/site/page/view/servizi',
                'lang' => Yii::app()->language)) ?></li>
        <li>
            <?php echo CHtml::link(Yii::t("strings", "menu.contatti"), array('/site/page/view/contatti',
                'lang' => Yii::app()->language)) ?></li>
        <li>
            <?php echo CHtml::link(Yii::t("strings", "menu.tracking"), array('/login'),
                array('class' => 'btn btn-primary', 'role' => 'button')); ?>
        </li>
    </ul>
    <nav class="language-menu">
        <ul class="menu">
            <li><?php echo CHtml::link("it", "it"); ?></li>
            <li><?php echo CHtml::link("en", "en"); ?></li>
        </ul>
    </nav>
</nav>