<div class="menu-wrapper">
    <div class="menu-btn">Menu</div>
    <nav class="menu-main menu-inner">
        <div class="menu-close btn btn-text">
            <i class="fa fa-times"></i>
        </div>
        <ul class="menu">
            <li>
                <?php echo CHtml::link(Yii::t("strings", "menu.azienda"), array('/site/page/view/azienda',
                    'lang' => Yii::app()->language)) ?></li>
            <li>
                <?php echo CHtml::link(Yii::t("strings", "menu.servizi"), array('/site/page/view/servizi',
                    'lang' => Yii::app()->language)) ?></li>
            <li>
                <?php echo CHtml::link(Yii::t("strings", "menu.contatti"), array('/site/page/view/contatti',
                    'lang' => Yii::app()->language)) ?></li>
            <li>
                <?php echo CHtml::link(Yii::t("strings", "menu.tracking"), array('/login'),
                    array('class' => 'btn btn-primary', 'role' => 'button')); ?>
            </li>
        </ul>
    </nav>
</div>