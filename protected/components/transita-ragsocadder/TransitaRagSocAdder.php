<?php

class TransitaRagSocAdder extends CWidget
{
    public $isRightColumn;

    public function run() {
        $this->render('_adder', array('isRightColumn' => $this->isRightColumn));
    }
}