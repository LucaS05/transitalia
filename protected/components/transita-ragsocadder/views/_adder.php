<?php if($isRightColumn === true): ?>
<div class="ragsoc-list ragsoc-list_right">
<?php elseif($isRightColumn === false): ?>
<div class="ragsoc-list">
<?php endif; ?>
    <div class="ragsoc-list__list">
        <h3 ng-if="listaRagSoc.length == 0" style="color: #fff; text-align: center;">
            Nessuna ragione sociale aggiunta!
        </h3>
        <div class="error-message" ng-show="lastRagSoc && !hideme">
            Nessuna ragione sociale aggiunta! <a ng-click="hideme = true">Chiudi</a>
        </div>
        <div class="ragsoc-list__row" ng-repeat="ragsoc in listaRagSoc">
            <div class="row">
                <div class="col-md-8 ragsoc-list__item">{{ragsoc}}</div>
                <div class="col-md-4">
                    <div class="btn btn-block btn-danger" ng-click="removeRagSoc($index)">
                        Rimuovi <i class="fa fa-times"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="ragsoc-list__adder">
        <div class="row">
            <p class="trform__placeholder col-md-12">Inserisci la ragione sociale per il cliente</p>
            <div class="col-md-12">
                <angucomplete-alt
                    pause="100"
                    minlength="3"
                    placeholder="Esempio: Gemitex s.p.a."
                    ng-model="cliente.rag_soc"
                    selected-object="clienteSelected"
                    text-searching="Cerco ragione sociale..."
                    text-no-results="Nessuna ragione sociale trovata!"
                    remote-url="../api/mittenti?q="
                    remote-url-data-field="mittenti"
                    title-field="rag_soc"
                    input-class="form-control trform__input" />
            </div>
            <div class="col-md-12">
                <div ng-show="validationFailed && listaRagSoc.length == 0"
                     class="trform__errorlabel">Almeno una ragione sociale è obbligatoria</div>
            </div>
            <?php if($isRightColumn === true): ?>
            <div class="col-md-6 col-md-offset-3" style="margin-top: 10px">
            <?php elseif($isRightColumn === false): ?>
            <div class="col-md-4 col-md-offset-4" style="margin-top: 10px">
            <?php endif; ?>
                <input ng-click="addRagSoc()" class="btn btn-block trbtn-main" value="Aggiungi Ragione Sociale" />
            </div>
        </div>
    </div>
</div>