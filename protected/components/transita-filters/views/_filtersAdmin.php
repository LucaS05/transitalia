<div class='col-sm-5'>
    <div class='filter selfilter'>
        <div date-range-picker
             options='datePicker.opts'
             ng-model='datePicker.date'
             ng-if='!datePicker.date.startDate && !datePicker.date.endDate'>
            <div class="filter-label">Data</div>
            <div class="filter-wrap">Seleziona intervallo date</div>
        </div>
        <div ng-if="selStartDate && selEndDate">
            <div class="filter-active">
                <div class="filter-value-wrap">
                    <div class="filter-value">
                        <span class="filter-value__label">Data</span>
                        <span class="filter-value__value">
                                Dal <span class="selfilter__value">{{selStartDate}}</span>
                                al <span class="selfilter__value">{{selEndDate}}</span>
                            </span>
                    </div>
                    <div class="close-filter-btn" ng-click="resetSpedizioni()">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <text-filter filter="trackingFilter"
           filter-list="spedizioni"
           run="runFilter(filterType, filterValue)"
           reset="resetSpedizioni()" />
</div>