<div class="container">
    <h4 class="col-md-12">Cerca Spedizioni</h4>
    <div class="col-md-4">
        <text-filter filter="numSpedizioneFilter"
                 filter-list="spedizioni"
                 run="runFilter(filterType, filterValue)"
                 reset="resetSpedizioni()" />
    </div>
    <div class="col-md-3">
        <text-filter filter="ddtFilter"
                     run="runFilter(filterType, filterValue)"
                     reset="resetSpedizioni()" />
    </div>
    <div class="col-md-5">
        <text-filter filter="destFilter"
                     run="runFilter(filterType, filterValue)"
                     reset="resetSpedizioni()" />
    </div>
</div>