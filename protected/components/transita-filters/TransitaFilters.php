<?php

class TransitaFilters extends CWidget
{
    public function run() {
        if (Yii::app()->user->checkAccess('admin')) {
            $this->render('_filtersAdmin');
        } else if (Yii::app()->user->checkAccess('cliente')) {
            $this->render('_filtersCliente');
        }
    }
}