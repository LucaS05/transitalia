<?php

class ApiController extends Controller
{

    private function getJsonContetType()
    {
        return "application/json";
        
    }

	public function actionView()
	{
	    // Check if id was submitted via GET
	    if(!isset($_GET['id']))
	        $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
	 
	    switch($_GET['model'])
	    {
	        // Find respective model
	        case "mittente":
	            $model = Mittente::model()->findByPk($_GET['id']);
	            break;

            case 'cliente':
                $idStato = Yii::app()->request->getParam('id');
                if(isset($idStato) == false){
                    throw new CException('Parametro id per il cliente mancante');
                }
                $model = array();
                $modelUser = User::model()->findByPk($idStato);
                $mittenti = Mittente::model()->findAll('user=:user', array('user' => $modelUser->id));
                $model['username'] = $modelUser->username;
                $model['rag_soc'] = array();
                foreach ($mittenti as $mittente) {
                    array_push($model['rag_soc'], $mittente['rag_soc']);
                }
                break;
            case "spedizione":
                $idSpedizione = Yii::app()->request->getParam("id");
                if(isset($idSpedizione)){
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("t.id=:idspedizione");
                    $criteria->params = array(':idspedizione' => $idSpedizione);
                    if(Yii::app()->user->checkAccess("cliente")){
                        $criteria->join = "JOIN tbl_mittente ON t.mittente = tbl_mittente.id";
                        $criteria->addCondition("user=:user");
                        $criteria->params[":user"] = Yii::app()->user->id;
                    }
                    $modelSpedizione = Spedizioni::model()->find($criteria);
                    if(isset($modelSpedizione)){
                        $dataSpedizione = DateTime::createFromFormat('Y-m-d', $modelSpedizione->data);
                        $modelSpedizione->data = $dataSpedizione->format('d/m/Y');
                        if(isset($modelSpedizione->data_consegna)){
                            $dataConsegna = DateTime::createFromFormat('Y-m-d', $modelSpedizione->data_consegna);
                            $modelSpedizione->data_consegna = $dataConsegna->format('d/m/Y');
                        }
                        $modelSpedizione->stato = $modelSpedizione->statoSp->nome;
                        $spedizioneAttributes = $modelSpedizione->getAttributes();
                        if(Yii::app()->user->checkAccess("cliente")){
                            unset($spedizioneAttributes["tracking"]);
                            unset($spedizioneAttributes["num_spedizione"]);
                        }
                        foreach ($modelSpedizione->cronologiaSp as $cronItem) {
                            $cronItem->idstato = $cronItem->statoCrn->nome;
                            $dataCrn = DateTime::createFromFormat('Y-m-d', $cronItem->data);
                            $cronItem->data = $dataCrn->format('d/m/Y');
                        }
                        $spedizioneAttributes["cronologia"] = $modelSpedizione->cronologiaSp;
                        $model = $spedizioneAttributes;
                    }
                }
                break;
            case 'stato':
                $idStato = Yii::app()->request->getParam('id');
                if(isset($idStato) == false){
                    throw new CException('Parametro id per lo stato mancante');
                }
                $model = TrStatoSpedizione::model()->findByPk($idStato);
                break;
	        default:
	            $this->_sendResponse(501, sprintf(
	                'Mode <b>view</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	            Yii::app()->end();
	    }

	    if(is_null($model)){
	        $this->_sendResponse(404, "No Item found with id ".$_GET["id"]);
        }else{
	        $this->_sendResponse(200, CJSON::encode($model), $this->getJsonContetType());
        }
	}

	public function actionList()
	{
	    switch($_GET['model'])
	    {
	        case "spedizioni":
                $models = array();
                $criteria = new CDbCriteria();
                $criteria->order = "data DESC";
                $limit = Yii::app()->params["tablePageSize"];
                $pagina = Yii::app()->request->getParam("page");
                $ddt = Yii::app()->request->getParam("ddt");
                $destinatario = Yii::app()->request->getParam("dest");
                $tracking = Yii::app()->request->getParam("tracking");
                $dataFinale = Yii::app()->request->getParam("dataFinale");
                $dataIniziale = Yii::app()->request->getParam("dataIniziale");
                $numeroSpedizione = Yii::app()->request->getParam("numero_spedizione");
                $criteriaParams = array();

                if (! empty($numeroSpedizione)) {
                    $criteria->addCondition("t.num_spedizione = :num_spedizione");
                    $criteriaParams["num_spedizione"] = $numeroSpedizione;
                }

                if(isset($ddt)){
                    $criteria->addCondition("ddt=:ddt");
                    $criteriaParams["ddt"] = $ddt;
                }

                if(isset($destinatario)){
                    $destinatario = addcslashes($destinatario, "%_");
                    $criteria->addCondition("destinatario LIKE :dest");
                    $criteriaParams["dest"] = "%$destinatario%";
                }

                if(isset($tracking)){
                  $criteria->addCondition("tracking=:tracking");
                  $criteriaParams["tracking"] = $tracking;
                }

                $userUid = Yii::app()->user->getState('uid', null);

                if ($userUid !== null) {
                    $criteria->join = "JOIN tbl_mittente ON t.mittente = tbl_mittente.id";
                    $criteria->addCondition("tbl_mittente.uid = :uid");
                    $criteria->addCondition("t.num_spedizione = :num_spedizione");
                    $criteriaParams["uid"] = Yii::app()->user->uid;
                    $criteriaParams["num_spedizione"] = Yii::app()->user->numeroSpedizione;
                }

                if(isset($dataFinale) && isset($dataIniziale)){
                    $criteria->addCondition("data BETWEEN :dataIniziale AND :dataFinale");
                    $criteriaParams["dataIniziale"] = $dataIniziale;
                    $criteriaParams["dataFinale"] = $dataFinale;
                }

                if(Yii::app()->user->checkAccess("cliente")){
                    $criteria->join = "JOIN tbl_mittente ON t.mittente = tbl_mittente.id";
                    $criteria->addCondition("tbl_mittente.user = :user");
                    $criteriaParams["user"] = Yii::app()->user->id;
                }

                if(count($criteriaParams) > 0){
                    $criteria->params = $criteriaParams;
                }


                if(is_null($pagina)){
                    $pagina = 1;
                } else if($pagina > 1){
                    $criteria->offset = $limit * ($pagina - 1);
                }

                $criteria->limit = $limit;

                $spedizioni = Spedizioni::model()->findAll($criteria);
                $currPage = array("currPage" => $pagina);

                if (count($criteriaParams) > 0) {
                  $count = count($spedizioni);
                } else {
                  $count = Spedizioni::model()->count();
                }

                $size = array("count" => $count);
                $lastPage = array("lastPage" => ceil($count / $limit));

                array_push($models, $size);
                array_push($models, $lastPage);
                array_push($models, $currPage);

                $tempSpedizioni = array();
                foreach($spedizioni as $spedizione){
                    $attrs = $spedizione->attributes;
                    $dataSpedizione = DateTime::createFromFormat('Y-m-d', $attrs["data"]);
                    $attrs["data"] = $dataSpedizione->format('d/m/Y');
                    if(isset($attrs["data_consegna"])){
                        $dataConsegna = DateTime::createFromFormat('Y-m-d', $attrs["data_consegna"]);
                        $attrs["data_consegna"] = $dataConsegna->format('d/m/Y');
                    }
                    if(Yii::app()->user->checkAccess("cliente")){
                        unset($attrs["num_spedizione"]);
                        unset($attrs["tracking"]);
                    }
                    $idSpedizione = $spedizione->id;
                    $attrs["cronologia"] = Cronologia::model()->findAll(
                        array("condition"=>"idspedizione =  $idSpedizione"));
                    $attrs["stato"] = $spedizione->statoSp->nome;
                    array_push($tempSpedizioni, $attrs);
                }
                $tempModels = array("spedizioni" => $tempSpedizioni);
                array_push($models, $tempModels);
                array_push($models, array("limit" => $limit));
	            break;
            case "spedizione":
                $trk = Yii::app()->request->getParam("tracking");
                if(isset($trk)){
                    $models = array();
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("tracking=:tracking");
                    $criteria->params = array(':tracking' => $trk);
                    $spedizione = Spedizioni::model()->find($criteria);

                    $attrs = $spedizione->attributes;

                    $idSpedizione = $spedizione->id;
                    $attrs["cronologia"] = Cronologia::model()->findAll(array("condition"=>"idspedizione =  $idSpedizione"));
                    foreach($attrs["cronologia"] as $crn){
                        $crn->idstato = $crn->statoCrn->nome;
                    }
                    $attrs["stato"] = $spedizione->statoSp->nome;
                    array_push($models, $attrs);
                }
                break;
			case "clienti":
                if(Yii::app()->user->checkAccess("admin")){
                    $models = array();
                    $id = Yii::app()->user->id;
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("id>=:user_id");
                    $criteria->order = "username ASC";
                    $criteria->params = array(':user_id' => $id);
                    $users = User::model()->findAll($criteria);
                    foreach($users as $user){
                        if(Yii::app()->authManager->checkAccess('admin', $user->id) === false){
                            unset($user->password);
                            array_push($models, $user);
                        }
                    }
                }
	            break;
			case "spdDataRange":
				//Aggiungere controllo sul tipo di utente
	        	//Se non è admin, non deve effettuare questa operazione
	        	$models = array();
                $dataFinale = Yii::app()->request->getParam("dataFinale");
                $dataIniziale = Yii::app()->request->getParam("dataIniziale");
                if(isset($dataFinale) && isset($dataIniziale)){
                    $criteria = new CDbCriteria();
                    $criteria->addBetweenCondition("data", $dataIniziale, $dataIniziale);
                    $models = Spedizioni::model()->findAll($criteria);
                }
	            break;
	        case "spdclienteRange":
                $dataFinale = Yii::app()->request->getParam("dataFinale");
                $dataIniziale = Yii::app()->request->getParam("dataIniziale");
                if(isset($dataFinale) && isset($dataIniziale)){
                    $id = Yii::app()->user->mittente;
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("(data BETWEEN :datainizio AND :datafine) AND (mittente = :mittente)");
                    $criteria->order = "data ASC";
                    $criteria->params = array(":datainizio" => $dataIniziale,
                                              ":datafine" => $dataFinale,
                                              ":mittente" => $id);
                    $spedizioni = Spedizioni::model()->findAll($criteria);
                    $tempArrSpedizioni = array();
                    foreach($spedizioni as $spedizione){
                        $spdAttributes = $spedizione->getAttributes();
                        $spdAttributes["stato"] = $spedizione->statoSp->nome;
                        if(strlen($spdAttributes["num_spedizione"]) > 0){
                            array_push($tempArrSpedizioni, $spdAttributes);
                        }
                        unset($spdAttributes["num_spedizione"]);
                        unset($spdAttributes["tracking"]);
                    }
                    $models = $tempArrSpedizioni;
                }
                break;
            case "mittenti":
                $searchMit = $_GET["q"];
                if(isset($searchMit)){
                    $criteria = new CDbCriteria();
                    $searchMit = addcslashes($searchMit, "%_");
                    $criteria->addCondition("(user IS NULL) AND (rag_soc LIKE :ragsoc)");
                    $criteria->params = array(":ragsoc" => "$searchMit%");
                    $criteria->limit = 10;
                    $mittenti = Mittente::model()->findAll($criteria);
                    $models = array("mittenti" => $mittenti);
                }
                break;
            case "stati":
                $criteria = new CDbCriteria();
                $daDefinire = Yii::app()->request->getParam("dd");
                if ($daDefinire === "1") {
                    $criteria->addCondition("nome=:nome");
                    $criteria->params = array(":nome" => "Da Definire");
                }
                $models = TrStatoSpedizione::model()->findAll($criteria);
                break;
	        default:
	            // Model not implemented error
	            $this->_sendResponse(501, sprintf(
	                'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	            Yii::app()->end();
	    }
	    
	    // Did we get some results?
	    if(empty($models)) {
	        // No
	        $this->_sendResponse(200, 
	                // sprintf('No items where found for model <b>%s</b>', $_GET['model']) );
                    CJSON::encode($models) );
	    } else {
	        // Prepare response
	        $rows = array();
	        // foreach($models as $model)
	        //     $rows[] = $model;
	        // Send the response
	        $this->_sendResponse(200, CJSON::encode($models), $this->getJsonContetType());
	    }
	}

	private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
	{
	    // set the status
	    $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
	    header($status_header);
	    // and the content type
	    header('Content-type: ' . $content_type);
	 
	    // pages with body are easy
	    if($body != '')
	    {
	        // send the body
	        echo $body;
	    }
	    // we need to create the body if none is passed
	    else
	    {
	        // create some body messages
	        $message = '';
	 
	        // this is purely optional, but makes the pages a little nicer to read
	        // for your users.  Since you won't likely send a lot of different status codes,
	        // this also shouldn't be too ponderous to maintain
	        switch($status)
	        {
	            case 401:
	                $message = 'You must be authorized to view this page.';
	                break;
	            case 404:
	                $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
	                break;
	            case 500:
	                $message = 'The server encountered an error processing your request.';
	                break;
	            case 501:
	                $message = 'The requested method is not implemented.';
	                break;
	        }
	 
	        // servers don't always have a signature turned on 
	        // (this is an apache directive "ServerSignature On")
	        $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
	 
	        // this should be templated in a real-world solution
	        $body = '
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
	</head>
	<body>
	    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
	    <p>' . $message . '</p>
	    <hr />
	    <address>' . $signature . '</address>
	</body>
	</html>';
	 
	        echo $body;
	    }
	    Yii::app()->end();
	}

	private function _getStatusCodeMessage($status)
	{
	    // these could be stored in a .ini file and loaded
	    // via parse_ini_file()... however, this will suffice
	    // for an example
	    $codes = Array(
	        200 => 'OK',
	        400 => 'Bad Request',
	        401 => 'Unauthorized',
	        402 => 'Payment Required',
	        403 => 'Forbidden',
	        404 => 'Not Found',
	        500 => 'Internal Server Error',
	        501 => 'Not Implemented',
	    );
	    return (isset($codes[$status])) ? $codes[$status] : '';
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('list','view'),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
}
