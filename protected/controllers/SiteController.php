<?php

class SiteController extends Controller
{

    public $layout = "//layouts/main";

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = '//layouts/main-login';

		$trackingFormModel = new LoginForm();
        $transitaSearchForm = new TransitaSearchForm();

		if(isset($_POST['LoginForm'])) {
		    $this->loginUser($trackingFormModel, 'LoginForm', 'transitatracking/#/');
		}

        if (isset($_POST['TransitaSearchForm'])) {
            $transitaSearchForm->attributes = $_POST['TransitaSearchForm'];
            if ($transitaSearchForm->validate()) {
                Yii::app()->session->add('numeroSpedizione', $transitaSearchForm->numeroSpedizione);

                $this->redirect('transitasearch/');
            }
        }

		$this->render('login',
            array(
                'trackingModel' => $trackingFormModel,
                'genericModel' => $transitaSearchForm
            )
        );
	}

    private function loginUser($formModel, $modelName, $redirectUrl)
    {
        if(isset($formModel)) {
            $formModel->attributes = $_POST[$modelName];
            if ($formModel->validate() && $formModel->login()) {
                $this->redirect($this->redirect($redirectUrl));
            }
        }
    }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionShowLog()
	{
		echo "Logged Messages:<br><br>";
		CVarDumper::dump(Yii::getLogger()->getLogs());
	}

	public function actionArchive()
    {
        $commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
        $runner = new CConsoleCommandRunner();
        $runner->addCommands($commandPath);
        $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
        $runner->addCommands($commandPath);
        $args = array('yiic', 'archive');
        ob_start();
        $runner->run($args);
        echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
    }
}