<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
            'db'=>array(
                'connectionString' => 'mysql:host=172.18.0.2;dbname=transitaliatest',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => 'secret',
                'charset' => 'utf8',
                'enableProfiling' => true,
                'enableParamLogging' => true,
            ),
		),
	)
);
