<?php

return array(
    'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name' => 'Transitalia',
    'sourceLanguage' => '00',
    'language' => 'en',
    'theme' => 'trtheme',
    'preload' => array(
        'log'
    ),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.helpers.*',
        'application.modules.transitatracking.models.*',
        'application.modules.transitatracking.components.*'.
        'application.modules.transitasearch.models.*',
        'application.modules.transitasearch.components.*'
    ),
    'modules' => array(
        'transitatracking',
        'transitasearch',
    ),
    'components' => array(
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'tbl_auth_item',
            'itemChildTable' => 'tbl_auth_item_child',
            'assignmentTable' => 'tbl_auth_assignment',
        ),
        'user' => array(
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => 'false',
            'matchValue' => true,
            'rules' => array(
                array(
                    'api/list',
                    'pattern' => 'api/<model:\w+>',
                    'verb' => 'GET'
                ),
                array(
                    'api/list',
                    'pattern' => 'api/<model:\w+>/<datainizio>/<datafine>',
                    'verb' => 'GET'
                ),
                array(
                    'api/list',
                    'pattern' => 'api/<model:spdddt>/<ddt>/',
                    'verb' => 'GET'
                ),
                array(
                    'api/list',
                    'pattern' => 'api/<model:spddest>/<dest>/',
                    'verb' => 'GET'
                ),
                array(
                    'api/list',
                    'pattern' => 'api/<model:spdcliente>/',
                    'verb' => 'GET'
                ),
                array(
                    'api/list',
                    'pattern' => 'api/<model:mittenti>/?q=<mittente:\w+>',
                    'verb' => 'GET'
                ),
                array(
                    'api/list',
                    'pattern' => 'api/<model:spedizioni>/<tracking>/',
                    'verb' => 'GET'
                ),
                array(
                    'api/view',
                    'pattern' => 'api/<model:\w+>/<id:\d+>',
                    'verb' => 'GET'
                ),
                '/login' => 'site/login',
                '/logout' => 'site/logout',
                '/archive-$2y$10$vQPaoWKydePMHuGAO/Iss.NyvTl/hQazgb6pxqWNP33wSV/S1AwgO' => 'site/archive',
                '<lang:en>/' => '/',
                '<lang:it>/' => '/',
                '<lang:en>/company' => 'site/page/view/azienda',
                '<lang:it>/azienda' => 'site/page/view/azienda',
                '<lang:en>/services' => 'site/page/view/servizi',
                '<lang:it>/servizi' => 'site/page/view/servizi',
                '<lang:en>/contacts' => 'site/page/view/contatti',
                '<lang:it>/contatti' => 'site/page/view/contatti',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'db'=>array(
            'connectionString' => 'mysql:host=89.46.111.53;dbname=Sql810815_1',
            'emulatePrepare' => true,
            'username' => 'Sql810815',
            'password' => '1xlg3w0w87',
            'charset' => 'utf8',
            'enableProfiling' => true,
            'enableParamLogging' => true,
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'sentry'=>array(
            'class'=>'ext.yii-sentry.components.RSentryClient',
            'dsn'=>'https://7a831b867b4148839ef566f4e6e4354e:5830956bdaa640738e4cf6abc4053c40@sentry.io/100899',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class'=>'ext.yii-sentry.components.RSentryLogRoute',
                    'levels'=>'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning'
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'categories' => 'system.db.*',
                    'logFile' => 'db.log'
                )
            )

        ),
    ),
    'params' => array(
        'adminEmail' => 'lucasuriano@live.it',
        'trackingLink' => 'login',
        'tablePageSize' => 10
    ),
);