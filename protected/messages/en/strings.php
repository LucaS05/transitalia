<?php
    return array(
        'menu.azienda' => 'Company',
        'menu.servizi' => 'Services',
        'menu.contatti' => 'Contacts',
        'menu.tracking' => 'Online Tracking',
        'azienda.titolo' => 'Our Company',
        'azienda.sottotitolo' => '30 years in the business for you',
        'azienda.pagina.titolo' => '30 years in the business for you',
        'azienda.pagina.sottotitolo' => '30 years in the business for you',
        'azienda.pagina.contenuto' => "We founded our company in 1983. 
                                       Our goal at the time, was to spread our distribution and shipment services all 
                                       over the Bari province.
                                       Today we use the most advanced and professional systems for ensuring 
                                       top quality services.<br /><br />
                                       Thanks to our business leaders and partners Pallet and Parcel and UPS Italy, 
                                       we can deliver your shipments worldwide in 24 or 48 hours.<br /><br />
                                       <a href='contacts'>Contact us</a> about discussing the best <a href='services'>services</a>
                                       and rates tailored for your needs.
                                       Our customer service office will help you out, following your shipment until 
                                       its deliver.",
        'servizi.titolo' => 'Our services',
        'servizi.sottotitolo' => 'Everything needed for you shipment',
        'servizi.pagina.titolo' => 'For all your needs a solution',
        'servizi.pagina.sottotitolo' => 'Transitalia Express offers a wide range of services tailored for your needs.',
        'servizi.pagina.contenuto' => 'We offer different services for you to chose the best timing and costs options.
                                       <br />
                                       We are also able to ship worldwide with planned delivery times and returns thanks
                                       to our technologies, so to speed up and simplify your business.',
        'servizi.standard.titolo' => 'STANDARD',
        'servizi.standard' => '24 hour delivery everywhere in Italy (48-72 hour for Sicily and Sardinia).
                               When you need to balance between speed and costs.
                               We take care of your EU shipments, telling you the exact delivery times, 
                               taking into account customs clearance where needed. This way we help you and your 
                               business planning ahead.',
        'servizi.express_saver.titolo' => 'EXPRESS SAVER',
        'servizi.express_saver' => 'Next day delivery guaranteed in all business centers',
        'servizi.express.titolo' => 'EXPRESS',
        'servizi.express' => 'Next morning delivery guaranteed between 10:30 AM and 12:00 AM in the 70% of EU addresses',
        'servizi.express_plus.titolo' => 'EXPRESS PLUS',
        'servizi.express_plus' => 'Next working day delivery until 9:00 in the morning in most business center in Italy
                                   and Europe',
        'servizi.grandiconsegne.titolo' => 'BIG DELIVERY',
        'servizi.grandiconsegne' => 'While planning the shipment beforehand, we guarantee a 24 hour delivery of your big
                                     shipment. Big Delivery makes sure that your business is safe',
        'servizi.opzionali.contrassegno.titolo' => 'CASH ON DELIVERY',
        'servizi.opzionali.contrassegno' => "We're going to cash out your shipment for you to speed up payments, lowering
                                             risks with new clients transactions and improve cash flow.",
        'servizi.opzionali.signature.titolo' => 'ELECTRONIC SIGNATURE',
        'servizi.opzionali.signature' => 'This service will let you have and see you\'re recipient signature as delivery 
                                          proof in 50% of the time.
                                          In the remaining 50% of cases we will guarantee to get you the signed parcel
                                          label',
        'servizi.opzionali.ritiro.titolo' => 'GOODS COLLECTION',
        'servizi.opzionali.ritiro' => 'With a special written permission, is possible to collect also big quantity of goods
                                       in most Europe business centers',
        'contatti.titolo' => 'Contacts',
        'contatti.sottotitolo' => 'How to reach us',
        'contatti.telefono' => 'Phone',
        'contatti.ufficioamministrativo' => 'Customer Service',
        'contatti.ufficiooperativo' => 'Operations',
        'contatti.ufficiocommerciale' => 'Business',
        'contatti.ufficiocontabile' => 'Accounting',
    );
?>
