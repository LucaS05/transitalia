<?php
    return array(
        "menu.azienda" => "Azienda",
        "menu.servizi" => "Servizi",
        "menu.contatti" => "Contatti",
        "menu.tracking" => "Tracking Online",
        "azienda.titolo" => "La Nostra Azienda",
        "azienda.sottotitolo" => "L’esperienza di 30 anni al vostro servizio",
        "azienda.pagina.titolo" => "L’esperienza di 30 anni al vostro servizio",
        "azienda.pagina.sottotitolo" => "L’esperienza di 30 anni al vostro servizio",
        "azienda.pagina.contenuto" => "La nostra azienda nasce nel 1983, anno in cui si prefigge di unire 
                                            in maniera capillare tutta la provincia di Bari attraverso la distribuzione 
                                            giornaliera di corrispondenza e pacchi. Attualmente, i nostri sistemi 
                                            operativi sono tra i più tecnologici e professionali del mondo.
                                            Grazie anche alla preziosa collaborazione di aziende leader nel settore dei 
                                            trasporti, quali Pallet and Parcel e UPS Italia (dei quali siamo anche
                                            distributori) possiamo consegnare la vostra corrispondenza e i 
                                            vostri pacchi in tutto il mondo, con tempistiche che, a seconda dei rispettivi
                                            C.A.P. vanno dalle  24 ore (J+1) alle 48 ore (J+2).<br /><br />
                                            <a href='contatti'>Contattateci</a>, un nostro agente vi assisterà valutando con voi tariffe 
                                            e <a href='servizi'>servizi</a> i più adatti alle vostre specifiche esigenze. 
                                            Il nostro ufficio assistenza clienti sarà a vostra disposizione per
                                             seguire le vostre spedizioni fino ad avvenuta consegna.",
        "servizi.titolo" => "I nostri servizi",
        "servizi.sottotitolo" => "Tutto ciò che serve per la tua spedizione",
        "servizi.pagina.titolo" => "Per ogni tua esigenza una soluzione",
        "servizi.pagina.sottotitolo" => "Transitalia Express offre un'ampia gamma di servizi in base ad ogni tua singola spedizione.",
        "servizi.pagina.contenuto" => "La nostra gamma completa di servizi consente di scegliere tra diverse opzioni 
                                           sia in termini di velocità di consegna che di costi.<br />
                                           E' inoltre in grado, di consegnare in qualsiasi parte del mondo con orari 
                                           di consegna garantiti e tempi di resa programmati, sopportati da soluzioni 
                                           tecnologiche, che rendono più semplice il vostro lavoro.",
        "servizi.standard.titolo" => "STANDARD",
        "servizi.standard" => "Servizio di consegna affidabile in 24 ore in tutta Italia (48-72 ore in Sicilia-Sardegna) Quando occorre bilanciare l'esigenza della velocità con gli aspetti economici. Per il resto dell'Europa vi diremo esattamente quanti giorni occorrono perché la vostra spedizione arrivi a destinazione considerando anche i tempi di sdoganamento, dove necessario, in modo da aiutare voi e la Vostra azienda a pianificare tutto in anticipo e nel modo più conveniente",
        "servizi.express_saver.titolo" => "EXPRESS SAVER",
        "servizi.express_saver" => "Consegna garantita il giorno lavorativo successivo praticamente in tutti i centri d'affari.",
        "servizi.express.titolo" => "EXPRESS",
        "servizi.express" => "Consegna garantita il mattino successivo entro le ore 10:30 nel 70% degli indirizzi commerciali dell'Unione Europea o le 12:00 al più tardi.",
        "servizi.express_plus.titolo" => "EXPRESS PLUS",
        "servizi.express_plus" => "Consegna garantita entro le ore 09:00 del mattino del giorno lavorativo successivo nella maggior parte dei centri d'affari in Italia e in Europa.",
        "servizi.grandiconsegne.titolo" => "GRANDI CONSEGNE",
        "servizi.grandiconsegne" => "Programmando anticipatamente la spedizione, diamo garanzia di consegna in 24 ore dal momento del ritiro, anche di grandi quantitativi di merce, cosi' da poter sincronizzare il Vostro lavoro e quello del ricevente.",
        "servizi.opzionali.contrassegno.titolo" => "SERVIZIO CONTRASSEGNO",
        "servizi.opzionali.contrassegno" => "Con il servizio contrassegno, ci occuperemo noi di incassare il pagamento per la vostra spedizione al momento della consegna, garantendo pagamenti più rapidi e riducendo i rischi legati a transazioni con nuovi clienti, oltre a migliorare il flusso di cassa.",
        "servizi.opzionali.signature.titolo" => "ELECTRONIC SIGNATURE",
        "servizi.opzionali.signature" => "E' un servizio che vi permette di poter ricevere e visualizzare la firma olografica del ricevente come prova di avvenuta consegna nel 50% dei casi. Nella restante parte comunque potrete ricevere copia del documento di trasporto con relativo segnacollo timbrato e firmato.",
        "servizi.opzionali.ritiro.titolo" => "SERVIZIO RITIRO MERCI",
        "servizi.opzionali.ritiro" => "E' possibile effettuare, attraverso l'invio di una richiesta scritta, il ritiro di qualsiasi tipo di spedizione anche di ingenti quantità, nella maggior parte dei centri economici d'Europa.",
        "contatti.titolo" => "Contatti",
        "contatti.sottotitolo" => "Tutte le info per raggiungerci e contattarci",
        'contatti.telefono' => 'Telefono',
        'contatti.ufficioamministrativo' => 'Ufficio Amministrativo',
        'contatti.ufficiooperativo' => 'Ufficio Operativo',
        'contatti.ufficiocommerciale' => 'Ufficio Commerciale',
        'contatti.ufficiocontabile' => 'Ufficio Contabile e Fatturazione',
    );
?>